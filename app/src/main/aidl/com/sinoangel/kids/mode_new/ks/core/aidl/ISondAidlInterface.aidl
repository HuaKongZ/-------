// ISondAidlInterface.aidl
package com.sinoangel.kids.mode_new.ks.core.aidl;

// Declare any non-default types here with import statements

interface ISondAidlInterface {
   void playSound(int id);
}
