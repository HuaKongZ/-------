package com.sinoangel.kids.mode_new.ks.core.bean;

import android.content.Intent;
import android.content.pm.PackageInfo;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.db.annotation.Id;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.MD5Util;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/8/3 0003.
 */
public class CoreDataBean {

    /**
     * flag : 1
     * error : []
     * data : [{"appId":"3743","isNew":"0","appName":"Dinosaur get well soon 1","appDesc":"","packageName":"Dinosaur get well soon 1.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpjJwBZ3.png","versionName":"","versionCode":"04:22","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/Dinosaur get well soon 1.mp3","images":[],"picShowType":2},{"appId":"3744","isNew":"0","appName":"Dinosaur get well soon 2","appDesc":"","packageName":"Dinosaur get well soon 2.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpUvNxPa.png","versionName":"","versionCode":"04:13","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/Dinosaur get well soon 2.mp3","images":[],"picShowType":2},{"appId":"3745","isNew":"0","appName":"how do dinosaurs eat their food 1","appDesc":"","packageName":"how do dinosaurs eat their food 1.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/php2y19Ul.png","versionName":"","versionCode":"05:47","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/how do dinosaurs eat their food 1.mp3","images":[],"picShowType":2},{"appId":"3746","isNew":"0","appName":"how do dinosaurs eat their food 2","appDesc":"","packageName":"how do dinosaurs eat their food 2.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpcFS3yk.png","versionName":"","versionCode":"05:38","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/how do dinosaurs eat their food 2.mp3","images":[],"picShowType":2},{"appId":"3747","isNew":"0","appName":"How Do Dinosaurs Go to School 1","appDesc":"","packageName":"How Do Dinosaurs Go to School 1.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpg33UPb.png","versionName":"","versionCode":"03:36","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Go to School 1.mp3","images":[],"picShowType":2},{"appId":"3748","isNew":"0","appName":"How Do Dinosaurs Go to School 2","appDesc":"","packageName":"How Do Dinosaurs Go to School 2.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/php43AFeC.png","versionName":"","versionCode":"03:34","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Go to School 2.mp3","images":[],"picShowType":2},{"appId":"3749","isNew":"0","appName":"How Do Dinosaurs Learn Colours and Numbers ","appDesc":"","packageName":"How Do Dinosaurs Learn Colours and Numbers .mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpUFgjqs.png","versionName":"","versionCode":"02:28","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Learn Colours and Numbers .mp3","images":[],"picShowType":2},{"appId":"3750","isNew":"0","appName":"How Do Dinosaurs Say Goodnight 1","appDesc":"","packageName":"How Do Dinosaurs Say Goodnight 1.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpGGlkiJ.png","versionName":"","versionCode":"04:06","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Say Goodnight 1.mp3","images":[],"picShowType":2},{"appId":"3751","isNew":"0","appName":"How Do Dinosaurs Say Goodnight 2","appDesc":"","packageName":"How Do Dinosaurs Say Goodnight 2.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpWd5fbn.png","versionName":"","versionCode":"03:57","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Say Goodnight 2.mp3","images":[],"picShowType":2},{"appId":"3752","isNew":"0","appName":"How Do Dinosaurs Say I Love You 1","appDesc":"","packageName":"How Do Dinosaurs Say I Love You 1.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpscXTfo.png","versionName":"","versionCode":"03:14","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Say I Love You 1.mp3","images":[],"picShowType":2},{"appId":"3753","isNew":"0","appName":"How Do Dinosaurs Say I Love You 2","appDesc":"","packageName":"How Do Dinosaurs Say I Love You 2.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpskJmdO.png","versionName":"","versionCode":"03:06","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Say I Love You 2.mp3","images":[],"picShowType":2},{"appId":"3754","isNew":"0","appName":"How Do Dinosaurs Say I Love You 3","appDesc":"","packageName":"How Do Dinosaurs Say I Love You 3.mp3","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/php4BEpzP.png","versionName":"","versionCode":"01:03","sourceId":"1","downUrl":"http://cn.api.store.sinoangel.cn/audios/konglong/How Do Dinosaurs Say I Love You 3.mp3","images":[],"picShowType":2}]
     */

    private int flag;
    private List<?> error;
    /**
     * appId : 3743
     * isNew : 0
     * appName : Dinosaur get well soon 1
     * appDesc :
     * packageName : Dinosaur get well soon 1.mp3
     * icon : http://cn.img.store.sinoangel.cn/icons/2016-07-07/icon/phpjJwBZ3.png
     * versionName :
     * versionCode : 04:22
     * sourceId : 1
     * downUrl : http://cn.api.store.sinoangel.cn/audios/konglong/Dinosaur get well soon 1.mp3
     * images : []
     * picShowType : 2
     */

    private List<DataBean> data;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public List<?> getError() {
        return error;
    }

    public void setError(List<?> error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {

        @Id
        private String appId;
        private String isNew;
        private String appName;
        private String appDesc;
        private String packageName;
        private String icon;
        private String versionName;
        private String versionCode;
        private String sourceId;
        private String downUrl;
        private String images;
        private String picShowType;
        private String duration;
        private String albumAges;
        private String cateZh;
        private String cateEn;

        //外加

        private DownData dd;

        public String cardID;//属于哪个卡片
        public String categoryID;//属于哪个大类


        public DownData getDd() {
            if (dd == null) {
                dd = new DownData();
                dd.setAppId(appId);
//                AppUtils.outputLog("创建" + appId + dd.getDownSize());
            }
            return dd;
        }

        public void setDd(DownData dd) {
            this.dd = dd;
        }


        public String getCardID() {
            return StaticObj.getCurrCata().getAppId();
        }

        public String getCategoryID() {
            return StaticObj.getCategory_id();
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public void setCardID(String cardID) {
            this.cardID = cardID;
        }


        public String getAlbumAges() {
            return albumAges;
        }

        public void setAlbumAges(String albumAges) {
            this.albumAges = albumAges;
        }

        public String getCateZh() {
            return cateZh;
        }

        public void setCateZh(String cateZh) {
            this.cateZh = cateZh;
        }

        public String getCateEn() {
            return cateEn;
        }

        public void setCateEn(String cateEn) {
            this.cateEn = cateEn;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getIsNew() {
            return isNew;
        }

        public void setIsNew(String isNew) {
            this.isNew = isNew;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getAppDesc() {
            return appDesc;
        }

        public void setAppDesc(String appDesc) {
            this.appDesc = appDesc;
        }

        public String getPackageName() {
            return packageName;
        }

        public String getMD5PackageName() {
            return MD5Util.MD5(packageName);
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public String getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(String versionCode) {
            this.versionCode = versionCode;
        }

        public String getSourceId() {
            return sourceId;
        }

        public void setSourceId(String sourceId) {
            this.sourceId = sourceId;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }

        public String getPicShowType() {
            return picShowType;
        }

        public void setPicShowType(String picShowType) {
            this.picShowType = picShowType;
        }

        public String getImages() {
            return images;
        }

        public List<ImagesBean> getImagesList() {
            try {
                return JSON.parseArray(images, ImagesBean.class);
            } catch (Exception e) {
                return null;
            }
        }

        public void setImages(List<ImagesBean> images) {
            try {
                if (images != null) {
                    this.images = JSON.toJSONString(images);
                }
            } catch (Exception e) {
                int i = 0;
            }
        }

        /**
         * 判断文件是否在存在本地
         *
         * @return
         */
        public boolean isFileExist() {
            File file = new File(DownManagerUtil.getInstance().getSavePath() + appId + "/" + getMD5PackageName());
            return file.exists();
        }

        /**
         * 方法说明：获取文件路径
         *
         * @return
         */
        public String getSavePath() {
            return DownManagerUtil.getInstance().getSavePath() + appId + "/" + getMD5PackageName();
        }

        public String getUnZipPath() {
            return DownManagerUtil.getInstance().getSavePath() + appId + "/unzip/";
        }

        /**
         * 判断应用是否在安装在本地
         *
         * @return
         */
        public boolean isExistDevice() {
            PackageInfo packageInfo = null;
            try {
                packageInfo = MyApplication.getInstance().getPackageManager()
                        .getPackageInfo(packageName, 0);
            } catch (Exception e) {
                packageInfo = null;
            }
            return packageInfo != null;
        }

        private Intent intent;

        public Intent getIntent() {
            if (intent != null)
                return intent;
            intent = getAppIntent(packageName);
            if (intent != null) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            }
            return intent;
        }

        /**
         * 获取此包对应的意图
         *
         * @param packageName
         * @return
         */
        protected Intent getAppIntent(String packageName) {
            try {
                return MyApplication.getInstance().getPackageManager()
                        .getLaunchIntentForPackage(packageName);
            } catch (Exception e) {
            }
            return null;
        }

    }

    public static class ImagesBean implements Serializable {
        private int app_id;
        private String url;

        public int getApp_id() {
            return app_id;
        }

        public void setApp_id(int app_id) {
            this.app_id = app_id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class DownData implements Serializable {

        @Id
        private String appId;

        private int downSize;

        private int downState;

        private String silent;//无用

        private int posation;

        private int currPage;//看到第几页

        public String cardID;//属于哪个卡片
        public String categoryID;//属于哪个大类


        private boolean iseed;//是否看过

        public boolean iseed() {
            return iseed;
        }

        public void setIseed(boolean iseed) {
            this.iseed = iseed;
        }

        public String getCardID() {
            if ("0".equals(StaticObj.getCurrCata().getAppId())) {
                if (cardID == null) {
                    int i = 0;
                }
                return cardID;
            } else
                return StaticObj.getCurrCata().getAppId();
        }

        public String getCategoryID() {
            return StaticObj.getCategory_id();
        }


        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public void setCardID(String cardID) {
            this.cardID = cardID;
        }


        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public int getDownSize() {
            return downSize;
        }

        public void setDownSize(int downSize) {
            this.downSize = downSize;
        }

        public int getDownState() {
            return downState;
        }

        public void setDownState(int downState) {
            this.downState = downState;
        }

        public String getSilent() {
            return silent;
        }

        public void setSilent(String silent) {
            this.silent = silent;
        }

        public int getPosation() {
            return posation;
        }

        public void setPosation(int posation) {
            this.posation = posation;
        }

        public int getCurrPage() {
            return currPage;
        }

        public void setCurrPage(int currPage) {
            this.currPage = currPage;
        }
    }

}
