package com.sinoangel.kids.mode_new.ks.core;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.core.bean.HostWordBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.sinoangel.kids.mode_new.ks.widget.DynamicTagFlowLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommitCommentActivity extends BaseActivity implements View.OnClickListener {
    private EditText et;
    private ImageView iv_send, iv_cantel;
    private CoreDataBean.DataBean dat;
    private TextView tv_textcount;
    private DynamicTagFlowLayout dtfl;
    private List<HostWordBean.DataBean> lhdb;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            List<String> ls = new ArrayList<>();
            for (HostWordBean.DataBean wdb : lhdb) {
                ls.add(wdb.getKey());
            }
            dtfl.setTags(lhdb, et);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_commit_comment);
        iv_send = (ImageView) findViewById(R.id.iv_send);
        iv_cantel = (ImageView) findViewById(R.id.iv_cantel);
        tv_textcount = (TextView) findViewById(R.id.tv_textcount);
        dtfl = (DynamicTagFlowLayout) findViewById(R.id.dtfl);
        et = (EditText) findViewById(R.id.et_context);

        iv_send.setOnClickListener(this);
        iv_cantel.setOnClickListener(this);
        dat = (CoreDataBean.DataBean) getIntent().getExtras().get("data");
        String url = API.getHotkeyword();
        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {

            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    HostWordBean hb = JSON.parseObject(json, HostWordBean.class);
                    lhdb = hb.getData();
                    handler.sendEmptyMessage(0);
                } catch (Exception e) {
                }
            }
        });

        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tv_textcount.setText(s.length() + "/150");
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_send:

                if (et.getText().toString().trim().length() < 1) {
                    AppUtils.showToast(getString(R.string.bunengshao));
                    return;
                }


                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                Map<String, String> mss = new HashMap<>();
                if (StaticObj.getUid() != null)
                    mss.put("userId", StaticObj.getUid().getUserId() + "");
                mss.put("deviceId", SPUtils.getPhoneId());
                mss.put("singleId", dat.getAppId());
                mss.put("seriesId", StaticObj.getCurrCata().getAppId());
                mss.put("carrierId", StaticObj.getCategory_id());
                mss.put("content", et.getText().toString().trim());
                mss.put("starLevel", "5");
//                String url = API.userComment(mss);

                HttpUtil.getUtils().getJsonStringByPost(API.NET_USER_COMMENT, mss, new HttpUtil.OnNetResponseListener() {
                    @Override
                    public void onNetFail() {

                    }

                    @Override
                    public void onNetSucceed(String json) {
                        setResult(100);
                        finish();
                    }
                });

                break;
            case R.id.iv_cantel:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
        }
    }
}
