package com.sinoangel.kids.mode_new.ks.core.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/8/18 0018.
 */
public class HostWordBean {

    /**
     * flag : 1
     * error : []
     * data : [{"id":"7","albumId":"12","albumType":"1","key":"好看","word":"#好看#"},{"id":"8","albumId":"12","albumType":"1","key":"有趣","word":"#有趣#"},{"id":"9","albumId":"12","albumType":"1","key":"很喜欢","word":"#很喜欢#"}]
     */

    private int flag;
    private List<?> error;
    /**
     * id : 7
     * albumId : 12
     * albumType : 1
     * key : 好看
     * word : #好看#
     */

    private List<DataBean> data;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public List<?> getError() {
        return error;
    }

    public void setError(List<?> error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String id;
        private String albumId;
        private String albumType;
        private String key;
        private String word;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAlbumId() {
            return albumId;
        }

        public void setAlbumId(String albumId) {
            this.albumId = albumId;
        }

        public String getAlbumType() {
            return albumType;
        }

        public void setAlbumType(String albumType) {
            this.albumType = albumType;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }
    }
}
