package com.sinoangel.kids.mode_new.ks.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import android.util.Log;

/**
 * 功能说明：解压ZIP工具
 * 编写日期: 2016-2-23
 * 作者: 闻铭
 */
public class ZipUnpackUtils {
    /**
     * 方法说明：解压ZIP的方法
     *
     * @param targetDir 解压缩的目标目录
     */
    public static void Unzip(String zipFile, String targetDir) {
        int BUFFER = 4096; // 这里缓冲区我们使用4KB，
        String strEntry; // 保存每个zip的条目名称

        try {
            BufferedOutputStream dest = null; // 缓冲输出流
            FileInputStream fis = new FileInputStream(zipFile);
            ZipInputStream zis = new ZipInputStream(
                    new BufferedInputStream(fis));
            ZipEntry entry; // 每个zip条目的实例

            while ((entry = zis.getNextEntry()) != null) {

                try {
                    Log.e("Unzip: ", "=" + entry);
                    int count;
                    byte data[] = new byte[BUFFER];
                    strEntry = entry.getName();

                    File entryFile = new File(targetDir + "/" + strEntry);
                    File entryDir = new File(entryFile.getParent());
                    if (!entryDir.exists()) {
                        entryDir.mkdirs();
                    }

                    FileOutputStream fos = new FileOutputStream(entryFile);
                    dest = new BufferedOutputStream(fos, BUFFER);
                    while ((count = zis.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, count);
                    }
                    dest.flush();
                    dest.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            zis.close();
        } catch (Exception cwj) {
            cwj.printStackTrace();
        }
    }

    /**
     * 读取文本文件中的内容
     *
     * @param strFilePath
     * @return
     */
    public static String ReadTxtFile(String strFilePath) {
        String fileName = strFilePath;// 文件路径
        String res = "";
        try {
            FileInputStream fin = new FileInputStream(fileName);
            int length = fin.available();
            byte[] buffer = new byte[length];
            fin.read(buffer);
           // res = EncodingUtils.getString(buffer, "UTF-8");// //依Y.txt的编码类型选择合适的编码，如果不调整会乱码
            res = new String(buffer);
            fin.close();// 关闭资源
//            int a = Integer.parseInt(res.substring(3, 5));
//            int b = Integer.parseInt(res.substring(8, 10));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

}
