package com.sinoangel.kids.mode_new.ks.core.broadcast;

import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;

import java.io.File;

/**
 * Created by Administrator on 2015/11/26 0026.
 */
public class DownBroadcastReceiver extends BroadcastReceiver {

    private DownloadManager downloadManager;

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
            Toast.makeText(context, "下载完成.即将安装", Toast.LENGTH_LONG).show();

            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);                                                                                      //TODO 判断这个id与之前的id是否相等，如果相等说明是之前的那个要下载的文件
            Query query = new Query();
            query.setFilterById(id);
            downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            Cursor cursor = downloadManager.query(query);

            int columnCount = cursor.getColumnCount();
            while (cursor.moveToNext()) {
                for (int j = 0; j < columnCount; j++) {
                    String columnName = cursor.getColumnName(j);
                    String string = cursor.getString(j);
                    if (columnName.equals("local_filename")) {
                        Intent intentdown = new Intent();
                        intentdown.setAction(Intent.ACTION_VIEW);
                        File file = new File(string);
                        if (file == null) {
                            return;
                        }
                        intentdown.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                        intentdown.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intentdown);
                    }
                }
            }
            cursor.close();
        }

    }

}
