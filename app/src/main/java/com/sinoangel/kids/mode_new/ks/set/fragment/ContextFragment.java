package com.sinoangel.kids.mode_new.ks.set.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.db.sqlite.Selector;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.set.adapter.ContextAdapter;
import com.sinoangel.kids.mode_new.ks.set.adapter.ContextGameAdapter;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.squareup.okhttp.Call;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LongXi on 2016/8/22.
 */
public class ContextFragment extends Fragment {
    private List<Cate.DataBean> CataList;
    private Call call;
    private List<CoreDataBean.DataBean> dataList;
    private ImageView iv_down;
    private RecyclerView rv_list;
    private ContextAdapter ca;
    private ContextGameAdapter cga;
    private AppUninstallReceiver receiver;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    try {
                        ca = new ContextAdapter(getActivity(), getActivity().getWindow(), CataList);
                        rv_list.setAdapter(ca);
                    } catch (Exception e) {

                    }
                    break;
                case 1:
                    try {
                        if (dataList == null || dataList.size() == 0)
                            iv_down.setVisibility(View.VISIBLE);
                        else
                            iv_down.setVisibility(View.GONE);
                        cga = new ContextGameAdapter(getActivity(), getActivity().getWindow(), dataList);
                        rv_list.setAdapter(cga);
                    } catch (Exception e) {

                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_context, null);
        rv_list = (RecyclerView) view.findViewById(R.id.rv_list);
        RadioGroup rg_btn = (RadioGroup) view.findViewById(R.id.rg_btn);
        iv_down = (ImageView) view.findViewById(R.id.iv_down);
        GridLayoutManager flm = new GridLayoutManager(getActivity(), 4);
        rv_list.setLayoutManager(flm);

        rg_btn.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                switch (i) {
                    case R.id.rb_1:
                        StaticObj.setCategory_id(Constant.CATEGORY_INITIATE);
                        getNativeData();
                        HttpUtil.getUtils().statistics("", "1028-1", "0", "0", "0", 0);
                        break;
                    case R.id.rb_2:
                        iv_down.setVisibility(View.GONE);
                        StaticObj.setCategory_id(Constant.CATEGORY_CARTOON);
                        getNetData();
                        HttpUtil.getUtils().statistics("", "1028-2", "0", "0", "0", 0);
                        break;
                    case R.id.rb_3:
                        iv_down.setVisibility(View.GONE);
                        StaticObj.setCategory_id(Constant.CATEGORY_PICTURE);
                        getNetData();
                        HttpUtil.getUtils().statistics("", "1028-3", "0", "0", "0", 0);
                        break;
                    case R.id.rb_4:
                        iv_down.setVisibility(View.GONE);
                        StaticObj.setCategory_id(Constant.CATEGORY_SONG);
                        getNetData();
                        HttpUtil.getUtils().statistics("", "1028-4", "0", "0", "0", 0);
                        break;
                }


            }
        });

        rg_btn.check(R.id.rb_1);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ca != null) {
                    ca.refish();
                } else if (cga != null) {
                    cga.refish();
                }
            }
        });

        //注册应用程序广播接收者
        receiver = new AppUninstallReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addDataScheme("package");
        getActivity().registerReceiver(receiver, filter);

        return view;
    }

    /**
     * 方法说明：从服务器获取数据来显示滑动控件
     */
    private void getNetData() {
        if (call != null)
            call.cancel();

        final String url = API.generalCate();
        call = HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
            }

            @Override
            public void onNetSucceed(String json) {

                try {
                    Cate cate = JSON.parseObject(json, Cate.class);
                    CataList = cate.getData();
                    handler.sendEmptyMessage(0);
                } catch (Exception e) {
                }
            }
        });

    }

    private void getNativeData() {
        try {
            Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
            sel_dd.expr(" categoryID =" + Constant.CATEGORY_INITIATE + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
            List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

            Selector sel_db = Selector.from(CoreDataBean.DataBean.class);
            sel_db.expr(" categoryID =" + Constant.CATEGORY_INITIATE);
            List<CoreDataBean.DataBean> ldb = MyApplication.getInstance().getDbUtisl().findAll(sel_db);

            dataList = new ArrayList<>();
            if (ldd != null)
                for (CoreDataBean.DownData dd : ldd) {
                    if (ldb != null)
                        for (CoreDataBean.DataBean db : ldb) {
                            if (dd.getAppId().equals(db.getAppId())) {
                                db.setDd(dd);
                                dataList.add(db);
                                break;
                            }
                        }
                }
            handler.sendEmptyMessage(1);
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(receiver);
    }


    public class AppUninstallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_PACKAGE_REMOVED.equals(intent.getAction())) {
                CoreDataBean.DataBean data = StaticObj.dataIng;
                File file = new File(DownManagerUtil.getInstance().savePath + data.getAppId());
                DownManagerUtil.deleteAllFile(file);
                dataList.remove(data);
                try {
                    MyApplication.getInstance().getDbUtisl().delete(data.getDd());
                    MyApplication.getInstance().getDbUtisl().delete(data);
                } catch (Exception e) {

                }

                cga.notifyDataSetChanged();

                StaticObj.dataIng = null;
            }
        }
    }
}
