package com.sinoangel.kids.mode_new.ks.set.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.util.List;

/**
 * 关联类：讲故事的适配器 适配器说明：用于展示启蒙应用中的icon ,黑色遮盖物，下载按钮 编写日期: 2016-2-2 作者: 闻铭
 * </pre>
 */
public class ContextGameAdapter extends RecyclerView.Adapter<ContextGameAdapter.ViewHolder> implements View.OnLongClickListener, View.OnClickListener {

    private List<CoreDataBean.DataBean> dataList;
    private Context mContext;
    private boolean isEdit;

    public ContextGameAdapter(Context mContext, Window win, List<CoreDataBean.DataBean> dataList) {
        super();
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_item, null);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.iv.setOnLongClickListener(this);
        view.setOnClickListener(this);
        viewHolder.iv.setOnClickListener(this);
        viewHolder.iv_delete.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if (isEdit) {
            viewHolder.iv_delete.setVisibility(View.VISIBLE);
        } else {
            viewHolder.iv_delete.setVisibility(View.GONE);
        }
        viewHolder.iv_delete.setTag(dataList.get(position));
        ImageUtils.showImgUrl(dataList.get(position).getIcon(), viewHolder.iv);


    }

    @Override
    public int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public boolean onLongClick(final View view) {
        isEdit = true;
        notifyDataSetChanged();

        return true;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.iv_delete:
                StaticObj.dataIng = (CoreDataBean.DataBean) view.getTag();

                AppUtils.uninstallApplication(mContext, StaticObj.dataIng.getPackageName());

                break;
            default:
                if (isEdit) {
                    isEdit = false;
                    notifyDataSetChanged();
                }
                break;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv;
        public ImageView iv_delete;

        public ViewHolder(View itemView) {
            super(itemView);
            iv = (ImageView) itemView.findViewById(R.id.iv_core);
            iv_delete = (ImageView) itemView.findViewById(R.id.iv_delete);
        }
    }

    public void refish() {
        isEdit = false;
        notifyDataSetChanged();
    }
}

