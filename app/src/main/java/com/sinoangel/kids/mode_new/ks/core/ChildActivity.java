package com.sinoangel.kids.mode_new.ks.core;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.db.sqlite.Selector;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.function.book.PaintingActivity;
import com.sinoangel.kids.mode_new.ks.function.cartoon.CartoonActivity;
import com.sinoangel.kids.mode_new.ks.function.game.GameActivity;
import com.sinoangel.kids.mode_new.ks.function.song.SongActivity;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.lang.ref.WeakReference;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * 类说明：选择类别页 功能说明:滑动控件展示4大分类中的小类别，点击某一个大类下的小类别可以进行进一步跳转 编写日期: 2016-1-26 作者: 闻铭
 * </pre>
 */
public class ChildActivity extends BaseActivity implements OnClickListener {
    // 变量说明：back键
    private ImageView back;
    //    private View rl_lay;
    //滑动控件的适配器
    private PagerAdapter adapter;
    /* 全局的二级页面滑动分类集合 */
    private List<Cate.DataBean> CataList;
    //变量说明：progressBar
    private GifImageView bar;
    private ViewPager vp_list;
    private View v_left, v_right, v_cen;
    private int postion;

    private boolean fistStart;

    private MyHandler handler = new MyHandler(this);

    private static class MyHandler extends Handler {

        WeakReference<ChildActivity> mActivityReference;

        MyHandler(ChildActivity activity) {
            mActivityReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final ChildActivity activity = mActivityReference.get();
            switch (msg.what) {
                case 0:
                    activity.vp_list.setAdapter(activity.adapter);
//                    activity.adapter.notifyDataSetChanged();
                    activity.vp_list.setCurrentItem(activity.CataList.size() * 1000);
//                    activity.adapter.notifyDataSetChanged();
                    activity.bar.setVisibility(View.GONE);
                    break;
                case 1:
                    AppUtils.showToast(activity.getString(R.string.net_warnnetwork));
                    break;
            }
        }
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);
        init();
        if (HttpUtil.isNetworkAvailable()) {
            getNetData();
        } else {
            getNativeData();
        }

    }


    /**
     * 方法说明：初始化所有按钮
     */
    private void init() {
        back = (ImageView) findViewById(R.id.child_back);
        bar = (GifImageView) findViewById(R.id.child_progressBar);
        vp_list = (ViewPager) findViewById(R.id.vp_list);
        v_left = findViewById(R.id.v_left);
        v_right = findViewById(R.id.v_right);
        v_cen = findViewById(R.id.v_cen);

        v_left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                vp_list.dispatchTouchEvent(event);
                return false;
            }
        });

        v_cen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                vp_list.dispatchTouchEvent(event);
                return false;
            }
        });

        v_right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                vp_list.dispatchTouchEvent(event);
                return false;
            }
        });

        switch (StaticObj.getCategory_id()) {
            case Constant.CATEGORY_CARTOON:
                bar.setImageResource(R.mipmap.lloading2);
                break;
            case Constant.CATEGORY_INITIATE:
                bar.setImageResource(R.mipmap.lloading4);
                break;
            case Constant.CATEGORY_PICTURE:
                bar.setImageResource(R.mipmap.lloading1);
                break;
            case Constant.CATEGORY_SONG:
                bar.setImageResource(R.mipmap.lloading3);
                break;
        }

        back.setOnClickListener(this);

        adapter = new PagerAdapter() {

            @Override
            public int getCount() {
                return CataList == null ? 0 : Integer.MAX_VALUE;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                final int pos = position % CataList.size();
                final ViewHolder vh = new ViewHolder();

                if ("0".equals(CataList.get(pos).getAppId())) {
                    vh.imageView.setImageResource(R.mipmap.mycatebg);
                } else {
                    if (!"-1".equals(CataList.get(pos).getAppId()))
                        if ("1".equals(CataList.get(pos).getAppLang()))
                            vh.iv_lang.setImageResource(R.mipmap.chainese);
                        else if ("2".equals(CataList.get(pos).getAppLang())) {
                            vh.iv_lang.setImageResource(R.mipmap.english);
                        }
                    ImageUtils.showImgUrl(CataList.get(pos).getIcon(), vh.imageView);
                }
                container.addView(vh.rl_box);
                return vh.rl_box;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

        };

        vp_list.setOffscreenPageLimit(6);

        vp_list.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                            @Override
                                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                            }

                                            @Override
                                            public void onPageSelected(int position) {
                                                if (fistStart) {
                                                    MusicUtils.getMusicUtils().playSound(R.raw.objective_bar_filling);
                                                } else {
                                                    fistStart = true;
                                                }
                                                postion = position;
                                            }

                                            @Override
                                            public void onPageScrollStateChanged(int state) {

                                            }
                                        }

        );
        vp_list.setPageTransformer(true, new ViewPager.PageTransformer() {
                    final float MIN_SCALE1 = 0.85f;
                    final float MIN_SCALE2 = 0.7f;
                    final float MIN_SCALE3 = 0.6f;

                    final float MIN_ALPHA1 = 0.8f;
                    final float MIN_ALPHA2 = 0.6f;
                    final float MIN_ALPHA3 = 0.4f;

                    @Override
                    public void transformPage(View view, float position) {

                        int hei = view.getHeight();
                        float scaleFactor;
                        float tranYFactor;
                        float apFactor;
                        if (position <= -2) {
                            float cha = MIN_SCALE2 - MIN_SCALE3;
                            float baifenbi = (-position) - (int) (-position);
                            scaleFactor = MIN_SCALE2 - cha * baifenbi;

                            float apcha = MIN_ALPHA2 - MIN_ALPHA3;
                            apFactor = MIN_ALPHA2 - apcha * baifenbi;

                            tranYFactor = (hei - scaleFactor * hei) / 4;
                        } else if (position < -1) {
                            float cha = MIN_SCALE1 - MIN_SCALE2;
                            float baifenbi = (-position) - (int) (-position);
                            scaleFactor = MIN_SCALE1 - cha * baifenbi;

                            float apcha = MIN_ALPHA1 - MIN_ALPHA2;
                            apFactor = MIN_ALPHA1 - apcha * baifenbi;

                            tranYFactor = (hei - scaleFactor * hei) / 4;
                        } else if (position >= -1f && position <= 1f) {//[-1,1]
                            float cha = 1 - MIN_SCALE1;
                            scaleFactor = 1 - cha * Math.abs(position);

                            float apcha = 1 - MIN_ALPHA1;
                            apFactor = 1 - apcha * Math.abs(position);

                            tranYFactor = (hei - scaleFactor * hei) / 4;
                        } else if (position > 1 && position < 2) {
                            float cha = MIN_SCALE1 - MIN_SCALE2;
                            float baifenbi = position - (int) position;
                            scaleFactor = MIN_SCALE1 - cha * baifenbi;

                            float apcha = MIN_ALPHA1 - MIN_ALPHA2;
                            apFactor = MIN_ALPHA1 - apcha * baifenbi;

                            tranYFactor = (hei - scaleFactor * hei) / 4;
                        } else {
                            float cha = MIN_SCALE2 - MIN_SCALE3;
                            float baifenbi = position - (int) position;
                            scaleFactor = MIN_SCALE2 - cha * baifenbi;

                            float apcha = MIN_ALPHA2 - MIN_ALPHA3;
                            apFactor = MIN_ALPHA2 - apcha * baifenbi;

                            tranYFactor = (hei - scaleFactor * hei) / 4;
                        }
                        view.setScaleX(scaleFactor);
                        view.setScaleY(scaleFactor);

                        view.setTranslationY(tranYFactor);
                        view.setAlpha(apFactor);

                    }
                }

        );
        v_cen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = postion % CataList.size();

                Intent intent = null;
                //启蒙应用
                if (StaticObj.getCategory_id().equals(Constant.CATEGORY_INITIATE)) {
                    intent = new Intent(ChildActivity.this,
                            GameActivity.class);
                    //MP4
                } else if (StaticObj.getCategory_id().equals(Constant.CATEGORY_CARTOON)) {
                    intent = new Intent(ChildActivity.this,
                            CartoonActivity.class);
                    //绘本
                } else if (StaticObj.getCategory_id().equals(Constant.CATEGORY_PICTURE)) {
                    intent = new Intent(ChildActivity.this,
                            PaintingActivity.class);
                    //儿歌
                } else if (StaticObj.getCategory_id().equals(Constant.CATEGORY_SONG)) {
                    intent = new Intent(ChildActivity.this,
                            SongActivity.class);
                }

                StaticObj.setCurrCata(CataList.get(pos));
                startActivity(intent);
            }
        });

    }

    private void getNativeData() {
        try {
            Selector sele = Selector.from(Cate.DataBean.class);
            sele.where("categoryId", "=", StaticObj.getCategory_id());
            CataList = MyApplication.getInstance().getDbUtisl().findAll(sele);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CataList != null && CataList.size() > 0) {
            handler.sendEmptyMessage(0);
        } else {
            handler.sendEmptyMessage(1);
        }
    }

    /**
     * 方法说明：从服务器获取数据来显示滑动控件
     */
    private void getNetData() {

        final String url = API.generalCate();

        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                handler.sendEmptyMessage(1);
            }

            @Override
            public void onNetSucceed(String json) {

                try {
                    Cate cate = JSON.parseObject(json, Cate.class);
                    CataList = cate.getData();

                    if (!Constant.CATEGORY_CARTOON.equals(StaticObj.getCategory_id())) {
                        Cate.DataBean cat = new Cate.DataBean();
                        cat.setAppName(getString(R.string.mydown));
                        cat.setAppId("0");
                        cat.setAppDesc("");
                        CataList.add(0, cat);
                    }

                    MyApplication.getInstance().saveCardData(CataList);

                    handler.sendEmptyMessage(0);
                } catch (Exception e) {
                    handler.sendEmptyMessage(1);
                }
            }
        });

    }

    /**
     * 方法说明：所有按钮的点击事件
     */
    public void onClick(View V) {
        switch (V.getId()) {
            case R.id.child_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.v_left:
                vp_list.setCurrentItem(vp_list.getCurrentItem() - 1);
                break;
            case R.id.v_right:
                vp_list.setCurrentItem(vp_list.getCurrentItem() + 1);
                break;
        }
    }

    class ViewHolder {
        public ImageView imageView;
        public ImageView iv_lang;
        private RelativeLayout rl_box;

        public ViewHolder() {
            rl_box = (RelativeLayout) getLayoutInflater().inflate(R.layout.item_second_card, null);
            imageView = (ImageView) rl_box.findViewById(R.id.iv_img);
            iv_lang = (ImageView) rl_box.findViewById(R.id.iv_lang);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        fistStart = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (HttpUtil.isNetworkAvailable()) {
            getNetData();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(this);
    }
}
