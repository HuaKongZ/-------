package com.sinoangel.kids.mode_new.ks.core.bean;

import android.graphics.Bitmap;

import com.lidroid.xutils.db.annotation.Id;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.util.List;

public class Cate {

    /**
     * flag : 1
     * error : []
     * data : [{"appId":"45","appName":"益智","appDesc":"-","packageName":"-","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-06/icon/php3f7uAx.png","versionName":"-","versionCode":"-","downUrl":"-","images":"-","picShowType":2},{"appId":"57","appName":"艺术","appDesc":"-","packageName":"-","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-06/icon/phpHuzszv.png","versionName":"-","versionCode":"-","downUrl":"-","images":"-","picShowType":2},{"appId":"59","appName":"认知","appDesc":"-","packageName":"-","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-06/icon/phpljmtul.png","versionName":"-","versionCode":"-","downUrl":"-","images":"-","picShowType":2},{"appId":"68","appName":"探险","appDesc":"-","packageName":"-","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-06/icon/php48Thz7.png","versionName":"-","versionCode":"-","downUrl":"-","images":"-","picShowType":2},{"appId":"84","appName":"策略","appDesc":"-","packageName":"-","icon":"http://cn.img.store.sinoangel.cn/icons/2016-07-06/icon/phpqV3cRe.png","versionName":"-","versionCode":"-","downUrl":"-","images":"-","picShowType":2}]
     */

    private int flag;
    private List<?> error;
    /**
     * appId : 45
     * appName : 益智
     * appDesc : -
     * packageName : -
     * icon : http://cn.img.store.sinoangel.cn/icons/2016-07-06/icon/php3f7uAx.png
     * versionName : -
     * versionCode : -
     * downUrl : -
     * images : -
     * picShowType : 2
     */

    private List<DataBean> data;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public List<?> getError() {
        return error;
    }

    public void setError(List<?> error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        @Id
        private String appId;
        private String CateImage;
        private Bitmap CateBitmap;
        private String appName;
        private String appDesc;
        private String packageName;
        private String icon;
        private String versionName;
        private String versionCode;
        private String downUrl;
        private String images;
        private int picShowType;
        private String appLang;
        private String categoryId;
        private String albumAges;

        private String categoryNames;
        private String duration;


        private int isLocal;

        public int getIsLocal() {
            return isLocal;
        }

        public void setIsLocal(int isLocal) {
            this.isLocal = isLocal;
        }

        public String getCategoryNames() {
            return categoryNames;
        }

        public void setCategoryNames(String categoryNames) {
            this.categoryNames = categoryNames;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getAlbumAges() {
            return albumAges;
        }

        public void setAlbumAges(String albumAges) {
            this.albumAges = albumAges;
        }

        public String getCategory_id() {
            return categoryId;
        }

        public String getCategoryId() {
            return StaticObj.getCategory_id();

        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getAppLang() {
            return appLang;
        }

        public void setAppLang(String appLang) {
            this.appLang = appLang;
        }

        public String getCateImage() {
            return CateImage;
        }

        public void setCateImage(String cateImage) {
            CateImage = cateImage;
        }

        public Bitmap getCateBitmap() {
            return CateBitmap;
        }

        public void setCateBitmap(Bitmap cateBitmap) {
            CateBitmap = cateBitmap;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getAppDesc() {
            return appDesc;
        }

        public void setAppDesc(String appDesc) {
            this.appDesc = appDesc;
        }

        public String getPackageName() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public String getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(String versionCode) {
            this.versionCode = versionCode;
        }

        public String getDownUrl() {
            return downUrl;
        }

        public void setDownUrl(String downUrl) {
            this.downUrl = downUrl;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public int getPicShowType() {
            return picShowType;
        }

        public void setPicShowType(int picShowType) {
            this.picShowType = picShowType;
        }
    }

}
