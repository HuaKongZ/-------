package com.sinoangel.kids.mode_new.ks.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.R;

/**
 * Created by Z on 2016/11/23.
 */

public class ReflectImageView extends ImageView {

    public ReflectImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ReflectImageView(Context context) {
        this(context, null, 0);
    }

    public ReflectImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onDraw(Canvas canvas) {
//
        Drawable drawable = getDrawable();
        if (null != drawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Bitmap b = createReflectedImages(zoomImage(bitmap));
            canvas.drawBitmap(b, 0, 0, new Paint());
        } else {
            super.onDraw(canvas);
        }

    }

    final float Refl = getResources().getDimension(R.dimen.sw600_120dp);//倒影高度

    private Bitmap createReflectedImages(Bitmap bmp) {

        final int gap = (int) getResources().getDimension(R.dimen.sw600_10dp);
        final int width = bmp.getWidth();
        final int height = bmp.getHeight();

        Matrix matrix = new Matrix();
        matrix.preScale(1, -1);         // 图片矩阵变换（从低部向顶部的倒影）


        Bitmap reflectionImage = Bitmap.createBitmap(bmp, 0, height - (int) Refl, width, (int) Refl, matrix, false);   // 截取原图下半部分
        Bitmap bitmapWithReflection = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);          // 创建倒影图片（高度为原图3/2）

        Canvas canvas = new Canvas(bitmapWithReflection);   // 绘制倒影图（原图 + 间距 + 倒影）
        canvas.drawBitmap(bmp, 0, 0, null);       // 绘制原图
        canvas.drawBitmap(reflectionImage, 0, height + gap, new Paint());    // 绘制倒影图

        final Paint paint = new Paint();
        final LinearGradient shader = new LinearGradient(0, height + gap, 0, getHeight(), 0x70ffffff, 0x00ffffff, Shader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0, height, width, getHeight(), paint);

        return bitmapWithReflection;
    }

    public Bitmap zoomImage(Bitmap bgimage) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = getWidth() / width;
        float scaleHeight = (getHeight() - Refl) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }
}
