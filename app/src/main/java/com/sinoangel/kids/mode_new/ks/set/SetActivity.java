package com.sinoangel.kids.mode_new.ks.set;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.MainActivity;
import com.sinoangel.kids.mode_new.ks.core.base.BaseFragmentActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.set.fragment.AboutFragment;
import com.sinoangel.kids.mode_new.ks.set.fragment.ContextFragment;
import com.sinoangel.kids.mode_new.ks.set.fragment.HelpFragment;
import com.sinoangel.kids.mode_new.ks.set.fragment.SystemFragment;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;

import java.util.List;


public class SetActivity extends BaseFragmentActivity {

    private RadioGroup rg_btn;
    private FragmentManager fm;
    private FragmentTransaction ft;
    private Fragment af, sf, cf, hf;
    private ImageView iv_back;
    private int ny_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_set);

        sf = new SystemFragment();
        af = new AboutFragment();
        cf = new ContextFragment();
        hf = new HelpFragment();
        init();

        ((RadioButton) rg_btn.getChildAt(0)).setChecked(true);

        if (SPUtils.getIsExists(Constant.NEWUSE_NY3)) {
            Intent intent = new Intent(SetActivity.this, GuideActivity.class);
            intent.putExtra("index", 0);
            intent.putExtra("flage", Constant.NEWUSE_NY3);
            startActivity(intent);
        }

    }

    private void init() {
        rg_btn = (RadioGroup) findViewById(R.id.rg_btn);
        iv_back = (ImageView) findViewById(R.id.iv_back);

        fm = getSupportFragmentManager();//布局管理器
        rg_btn.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                ft = fm.beginTransaction();
                List<Fragment> lf = fm.getFragments();
                switch (checkedId) {
                    case R.id.rb_sys:
                        if (lf == null || !lf.contains(sf)) {
                            ft.add(R.id.fl_box, sf);
                        }
                        ft.hide(cf);
                        ft.hide(af);
                        ft.hide(hf);
                        ft.show(sf);
                        ft.commit();
                        HttpUtil.getUtils().statistics("", "1020", "1", "0", "0", 0);
                        break;
                    case R.id.rb_con:
                        if (lf == null || !lf.contains(cf)) {
                            ft.add(R.id.fl_box, cf);
                        }
                        ft.hide(af);
                        ft.hide(sf);
                        ft.hide(hf);
                        ft.show(cf);
                        ft.commit();
                        HttpUtil.getUtils().statistics("", "1021", "1", "0", "0", 0);
                        break;
                    case R.id.rb_about:
                        if (lf == null || !lf.contains(af)) {
                            ft.add(R.id.fl_box, af);
                        }
                        ft.hide(cf);
                        ft.hide(sf);
                        ft.hide(hf);
                        ft.show(af);
                        ft.commit();
                        HttpUtil.getUtils().statistics("", "1022", "1", "0", "0", 0);
                        break;
                    case R.id.rb_help:
                        if (lf == null || !lf.contains(hf)) {
                            ft.add(R.id.fl_box, hf);
                        }
                        ft.hide(cf);
                        ft.hide(sf);
                        ft.hide(af);
                        ft.show(hf);
                        ft.commit();
                        HttpUtil.getUtils().statistics("", "1023", "1", "0", "0", 0);
                        break;
                }

            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        MusicUtils.getMusicUtils().playOrPauseBgsc(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        MusicUtils.getMusicUtils().playOrPauseBgsc(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Constant.LOGIN_SUSSCE) {
            ((RadioButton) rg_btn.getChildAt(1)).setChecked(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
