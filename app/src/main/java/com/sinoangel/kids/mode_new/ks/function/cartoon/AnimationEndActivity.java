package com.sinoangel.kids.mode_new.ks.function.cartoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.set.H5StoreActivity;
import com.sinoangel.kids.mode_new.ks.set.LoginActivity;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.sinoangel.kids.mode_new.ks.widget.YKPlayerActivity;

public class AnimationEndActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_more, iv_content;
    private String vid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation_end);
        vid = getIntent().getStringExtra(Constant.DATA);


        iv_content = (ImageView) findViewById(R.id.iv_content);
        iv_more = (ImageView) findViewById(R.id.iv_more);


        if (getIntent().getBooleanExtra(Constant.COMFLAGE, false)) {
            iv_more.setImageResource(R.mipmap.ev_booking);
        }

        iv_more.setOnClickListener(this);
        iv_content.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_content:
                Intent i = new Intent(AnimationEndActivity.this, YKPlayerActivity.class);
                i.putExtra("vid", vid);
                startActivity(i);
                StaticObj.YKAgle = Constant.YK_FLAGE_MORE;
                finish();
                break;
            case R.id.iv_more:
                Intent userIntent = null;
                if (StaticObj.getUid() != null) {
                    userIntent = new Intent(this, H5StoreActivity.class);
                } else {
                    userIntent = new Intent(this, LoginActivity.class);
                }
                startActivityForResult(userIntent, 0);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constant.LOGIN_SUSSCE:
                Intent userIntent = new Intent(this, H5StoreActivity.class);
                startActivity(userIntent);
                break;
        }
    }
}
