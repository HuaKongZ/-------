package com.sinoangel.kids.mode_new.ks.set.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.set.LoginActivity;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/8/17 0017.
 */
public class SystemFragment extends Fragment implements View.OnClickListener {

    private CheckBox cb_music_of;
    private ImageView iv_logonout;
    private RadioGroup rg_age, rg_time;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_system, null);

        cb_music_of = (CheckBox) view.findViewById(R.id.cb_music_of);
        iv_logonout = (ImageView) view.findViewById(R.id.iv_logonout);

        rg_age = (RadioGroup) view.findViewById(R.id.rg_age);
        rg_time = (RadioGroup) view.findViewById(R.id.rg_time);

        iv_logonout.setOnClickListener(this);
        cb_music_of.setOnClickListener(this);


        if (SPUtils.getMusicFlage()) {
            cb_music_of.setChecked(true);
        }

        if (StaticObj.getUid() != null) {
            iv_logonout.setImageResource(R.mipmap.btn_logout);
        }

        int age = SPUtils.getAge();
        switch (age) {
            case 1:
                rg_age.check(R.id.rb_age1);
                break;
            case 2:
                rg_age.check(R.id.rb_age2);
                break;
            case 3:
                rg_age.check(R.id.rb_age3);
                break;
            case 4:
                rg_age.check(R.id.rb_age4);
                break;
        }

        int time = SPUtils.getTime();
        switch (time) {
            case 0:
                rg_time.check(R.id.rb_time_none);
                break;
            case 15:
                rg_time.check(R.id.rb_time_15);
                break;
            case 30:
                rg_time.check(R.id.rb_time_30);
                break;
            case 45:
                rg_time.check(R.id.rb_time_45);
                break;
            case 60:
                rg_time.check(R.id.rb_time_60);
                break;
        }


        rg_time.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_time_none:
                        SPUtils.putTime(0);
                        HttpUtil.getUtils().statistics("", "1026-0", "0", "0", "0", 0);
                        break;
                    case R.id.rb_time_15:
                        SPUtils.putTime(15);
                        HttpUtil.getUtils().statistics("", "1026-1", "0", "0", "0", 0);
                        break;
                    case R.id.rb_time_30:
                        SPUtils.putTime(30);
                        HttpUtil.getUtils().statistics("30", "1026-2", "0", "0", "0", 0);
                        break;
                    case R.id.rb_time_45:
                        SPUtils.putTime(45);
                        HttpUtil.getUtils().statistics("45", "1026-3", "0", "0", "0", 0);
                        break;
                    case R.id.rb_time_60:
                        SPUtils.putTime(60);
                        HttpUtil.getUtils().statistics("60", "1026-4", "0", "0", "0", 0);
                        break;
                }
                MyApplication.getInstance().startLockScreen();
            }
        });

        rg_age.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_age1:
                        SPUtils.putAge(1);
                        HttpUtil.getUtils().statistics("", "1025-1", "0", "0", "0", 0);
                        break;
                    case R.id.rb_age2:
                        SPUtils.putAge(2);
                        HttpUtil.getUtils().statistics("", "1025-2", "0", "0", "0", 0);
                        break;
                    case R.id.rb_age3:
                        SPUtils.putAge(3);
                        HttpUtil.getUtils().statistics("", "1025-3", "0", "0", "0", 0);
                        break;
                    case R.id.rb_age4:
                        SPUtils.putAge(4);
                        HttpUtil.getUtils().statistics("", "1025-4", "0", "0", "0", 0);
                        break;
                }
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cb_music_of:
                if (SPUtils.getMusicFlage()) {
                    SPUtils.putMusicFlage(false);
                    MusicUtils.getMusicUtils().nOFBGMusicService(false);
                    MusicUtils.getMusicUtils().nOFSoundService(false);
                    HttpUtil.getUtils().statistics("", "1024-0", "0", "0", "0", 0);
                } else {
                    SPUtils.putMusicFlage(true);
                    MusicUtils.getMusicUtils().nOFBGMusicService(true);
                    MusicUtils.getMusicUtils().nOFSoundService(true);
                    HttpUtil.getUtils().statistics("", "1024-1", "0", "0", "0", 0);
                }
                break;
            case R.id.iv_logonout:

                if (StaticObj.getUid() == null) {
                    startActivityForResult(new Intent(getActivity(), LoginActivity.class), 0);
                } else {

                    DialogUtils.YesOrNoDialog(getActivity(), R.mipmap.text_exit, new DialogUtils.YeOrOnListener() {
                        @Override
                        public void onOk() {

//                            HttpUtil.getUtils().getJsonString(API.outLogin(), new HttpUtil.OnNetResponseListener() {
//                                @Override
//                                public void onNetFail() {
//                                    AppUtils.outputLog("outLogin-fail");
//                                }
//
//                                @Override
//                                public void onNetSucceed(String json) {
//                                    AppUtils.outputLog("outLogin-succes");
//                                }
//                            });

                            StaticObj.setUid(null);
                            iv_logonout.setImageResource(R.mipmap.btn_login);
                            HttpUtil.getUtils().statistics("", "1027", "0", "0", "0", 0);
                        }

                        @Override
                        public void onNo() {

                        }
                    });
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constant.LOGIN_SUSSCE:
                iv_logonout.setImageResource(R.mipmap.btn_logout);
                break;
        }
    }
}
