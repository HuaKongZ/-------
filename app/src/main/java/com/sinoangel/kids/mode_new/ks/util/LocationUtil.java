package com.sinoangel.kids.mode_new.ks.util;

import android.location.LocationManager;

/**
 * Created by Administrator on 2016/9/2 0002.
 */
public class LocationUtil {


    private LocationManager lm;
    private static LocationUtil lu;

    public static LocationUtil getInit() {
        if (lu == null)
            lu = new LocationUtil();
        return lu;
    }

    public static void getLocation() {
        String url = "http://www.google.com.hk/";
        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                SPUtils.putLocation("1");
            }

            @Override
            public void onNetSucceed(String json) {
                SPUtils.putLocation("2");
            }
        });
    }
}
