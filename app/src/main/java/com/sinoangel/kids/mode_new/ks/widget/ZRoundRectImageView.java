package com.sinoangel.kids.mode_new.ks.widget;

/**
 * Created by Lx on 2016/3/22.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.R;

/**
 * 自定义的圆角矩形ImageView，可以直接当组件在布局中使用。
 *
 * @author caizhiming
 */
public class ZRoundRectImageView extends ImageView {

    private Paint paint;

    public ZRoundRectImageView(Context context) {
        this(context, null);
    }

    public ZRoundRectImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ZRoundRectImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        paint = new Paint();
    }

    /**
     * 绘制圆角矩形图片
     *
     * @author caizhiming
     */
    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (null != drawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Bitmap b = getRoundBitmap(bitmap);
            paint.reset();
            canvas.drawBitmap(b, 0, 0, paint);
        } else {
            super.onDraw(canvas);
        }

//        NinePatchDrawable drawable = (NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.bk_zez);
//
//        Bitmap bmp = Bitmap.createBitmap(
//                getWidth(),
//                getHeight(),
//                Bitmap.Config.RGB_565);
//        Canvas canv = new Canvas(bmp);
//        drawable.setBounds(0, 0, 100,
//                100);
//        drawable.draw(canv);
//        canvas.drawBitmap(bmp, 0, 0, null);
//        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));

//        super.onDraw(canvas);
    }

    /**
     * 获取圆角矩形图片方法
     *
     * @param bitmap
     * @return Bitmap
     * @author caizhiming
     */
    private Bitmap getRoundBitmap(Bitmap bitmap) {
        paint.setAntiAlias(true);//设置抗锯齿
        Bitmap output = Bitmap.createBitmap(getWidth(),
                getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        NinePatchDrawable drawable = (NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.bk_zez);

        Bitmap bmp = Bitmap.createBitmap(
                getWidth(),
                getHeight(),
                Config.ARGB_4444);
        Canvas canv = new Canvas(bmp);
        drawable.setBounds(0, 0, getWidth(),
                getHeight());
        drawable.draw(canv);
        canvas.drawBitmap(bmp, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));

        Bitmap bg = zoomImage(bitmap, getWidth(), getHeight());
        canvas.drawBitmap(bg, 0, 0, paint);

        bmp.recycle();
        bg.recycle();
        System.gc();

        return output;

    }

    public static Bitmap zoomImage(Bitmap bgimage, double newWidth,
                                   double newHeight) {
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }

}
