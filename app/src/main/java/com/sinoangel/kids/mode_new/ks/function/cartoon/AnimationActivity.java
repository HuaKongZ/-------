package com.sinoangel.kids.mode_new.ks.function.cartoon;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.ZanActivity;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
//import com.umeng.socialize.ShareAction;
//import com.umeng.socialize.bean.SHARE_MEDIA;

import java.io.File;
import java.util.Date;

/**
 * 类说明：动画屋的播放界面 功能说明：拿到点击的文件并且传入mp4播放器进行播放 编写日期: 2016-2-2 作者: 闻铭 </pre>
 */
public class AnimationActivity extends BaseActivity implements
        OnClickListener, OnSeekBarChangeListener {
    // 页面播放控件
    private SurfaceView surfaceView;
    // 播放器控件
    private MediaPlayer mediaPlayer;
    // 进度条控件
    private SeekBar seekbar;
    // 返回按钮
    private ImageView amback;
    // 整个布局
    private RelativeLayout relativeLayout;
    // 开始按钮
    private Button play;
    // 左边的时间显示
    protected TextView time_left;
    // 右边的时间显示
    protected TextView time_right;
    // 总时长
    protected long allTime;
    // 用于控制全局的显示
    private boolean surfaceViewplay = false;
    // 用于是否显示其他按钮
    private boolean display = false;

    /* 全局的文件管理变量 */
    protected File file;
    private CoreDataBean.DataBean dat;

    private long userTime;//使用时长

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 保持屏幕常亮
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_animotion);
        dat = StaticObj.dataIng;
        file = new File(dat.getSavePath());

        init();

        //获取电话通讯服务
        TelephonyManager tpm = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        //创建一个监听对象，监听电话状态改变事件
        tpm.listen(new MyPhoneStateListener(),
                PhoneStateListener.LISTEN_CALL_STATE);

        userTime = new Date().getTime();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        MusicUtils.getMusicUtils().playOrPauseBgsc(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
        mediaPlayer = null;
        HttpUtil.getUtils().statistics(dat.getAppId(),StaticObj.getCategory_id(), "0", "1", "0", (new Date().getTime() - userTime));
    }

    @Override
    protected void onPause() {
        super.onPause();
        //屏幕失去焦点，暂停播放
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    /**
     * 方法说明：初始化所有控件
     */
    private void init() {
        mediaPlayer = new MediaPlayer();// 创建一个播放器对象
        surfaceView = (SurfaceView) findViewById(R.id.animation_surfaceView);
        seekbar = (SeekBar) findViewById(R.id.animation_seekbar);
        amback = (ImageView) findViewById(R.id.animation_back);
        relativeLayout = (RelativeLayout) findViewById(R.id.animation_relativeLayout);
        play = (Button) findViewById(R.id.animation_button);
        time_left = (TextView) findViewById(R.id.vedio_time_curr);
        time_right = (TextView) findViewById(R.id.vedio_time_sum);


        play.setOnClickListener(this);
        amback.setOnClickListener(this);
        seekbar.setEnabled(false);
        seekbar.setOnSeekBarChangeListener(this);
        surfaceView.setOnClickListener(this);
        surfaceView.getHolder().setKeepScreenOn(true); // 保持屏幕高亮
        surfaceView.getHolder().addCallback(new Callback() {
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mediaPlayer.setDisplay(holder);

                //显示界面的时候立即播放
                immePlay();
                Listening();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                       int height) {

            }
        });


    }

    /**
     * 方法说明：按钮的点击执行
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.animation_button:
                // 如果不在播放
                if (display == false) {
                    // 隐藏back 隐藏播放状态栏
                    relativeLayout.setVisibility(View.GONE);
                    amback.setVisibility(View.GONE);
                    // 播放的方法（需要得到播放的路径）
                    play(file);
                    // 赋值修改播放状态
                    display = true;
                    // 将播放按钮修改为暂停按钮
                    play.setBackgroundResource(R.mipmap.pause_pre);
                } else {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
//					position = mediaPlayer.getCurrentPosition();
                        //Toast.makeText(getApplication(), "当前位置："+position, Toast.LENGTH_SHORT).show();
                        play.setBackgroundResource(R.mipmap.pause_pre);
                        // 赋值修改播放状态
                        display = true;
                    } else {
                        mediaPlayer.start();
                        play.setBackgroundResource(R.mipmap.play_pre);
                        // 赋值修改播放状态
                        display = true;
                    }
                }
                break;

            // 点击播放按钮的事件
            case R.id.animation_surfaceView:
                // true的时候隐藏所有控件只显示surfaceView
                if (surfaceViewplay == true) {
                    relativeLayout.setVisibility(View.GONE);
                    amback.setVisibility(View.GONE);
                    surfaceViewplay = false;
                    // false的时候显示所有控件包括surfaceView
                } else {
                    relativeLayout.setVisibility(View.VISIBLE);
                    amback.setVisibility(View.VISIBLE);
                    surfaceViewplay = true;
                }
                break;

            case R.id.animation_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;

        }

    }

    //为了实现刚进入界面的时候就播放视频
    public void immePlay() {
        // 隐藏back 隐藏播放状态栏
        relativeLayout.setVisibility(View.GONE);
        amback.setVisibility(View.GONE);
        // 播放的方法（需要得到播放的路径）
        play(file);
        // 赋值修改播放状态
        display = true;
    }

    /**
     * 方法说明：点击播放按钮的开始播放 参数说明：需要传递一个播放路径的文件名，类似（"sdcard/sinoangel/Video/1.mp4"）
     */
    private void play(File file) {
        // 文件不等于空

        if (file.exists()) {
            try {
                // 先重置到0位子
                mediaPlayer.reset();
                // 给予播放的文件
                mediaPlayer.setDataSource(file.getAbsolutePath());
                // 设置将视频画面输出到SurfaceView
                mediaPlayer.setDisplay(surfaceView.getHolder());
                // 准备
                mediaPlayer.prepare();
                mediaPlayer.start();
                // 获得被点击文件的总播放时长
                allTime = mediaPlayer.getDuration();
                // 将总时长复制到右边text上
                time_right.setText(toTime(allTime));
            } catch (Exception e) {
            }
        } else {
        }
    }

    /**
     * 方法说明：seekBar的拖拽事件
     */
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        if (fromUser && mediaPlayer != null) {
            mediaPlayer.seekTo(progress);
        }
        time_left.setText(toTime(progress));
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    /**
     * 方法说明：用线程来控制seekBar，让它每100就进行一次更新，不能大于300否知滑动会出现卡顿现象
     */
    protected static Handler handler = new Handler();
    protected Runnable updateThread = new Runnable() {
        public void run() {
            // 获得歌曲现在播放位置并设置成播放进度条的值
            if (mediaPlayer != null) {
                seekbar.setProgress(mediaPlayer.getCurrentPosition());
                // 每次延迟500毫秒再启动线程
                handler.postDelayed(updateThread, 100);
            }
        }
    };

    /**
     * 方法说明：监听播放的动作
     */
    private void Listening() {
        // 监听音乐播放中
        mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                seekbar.setMax(mediaPlayer.getDuration());
                handler.post(updateThread);
                seekbar.setEnabled(true);

            }
        });

        // 播放完毕完毕后
        mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {

                startActivity(new Intent(AnimationActivity.this, ZanActivity.class).putExtra(Constant.ZAN_VH, false).putExtra(Constant.DATA, dat));

                finish();
            }
        });
    }

    class MyPhoneStateListener extends PhoneStateListener {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE: //空闲
                    Log.i("phone", "电话空闲");
                    //onStart();
                    break;
                case TelephonyManager.CALL_STATE_RINGING: //来电
                    Log.i("phone", "有来电");
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        //onStop();
                        mediaPlayer.pause();
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK: //摘机（正在通话中）
                    Log.i("phone", "通话中");
                    break;
            }
        }
    }

    /**
     * 时间转化处理
     */
    public String toTime(long time) {
        time /= 1000;
        long minute = time / 60;
        long second = time % 60;
        minute %= 60;
        return String.format(" %02d:%02d ", minute, second);
    }

    @Override
    protected void onStart() {
        super.onStart();
        MusicUtils.getMusicUtils().playOrPauseBgsc(false);
    }

}
