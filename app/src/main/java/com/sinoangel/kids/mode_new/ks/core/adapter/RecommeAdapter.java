package com.sinoangel.kids.mode_new.ks.core.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.function.book.PaintingActivity;
import com.sinoangel.kids.mode_new.ks.function.cartoon.CartoonActivity;
import com.sinoangel.kids.mode_new.ks.function.song.SongActivity;
import com.sinoangel.kids.mode_new.ks.set.H5StoreActivity;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.util.List;

/**
 * Created by Administrator on 2016/8/4 0004.
 */
public class RecommeAdapter extends RecyclerView.Adapter<RecommeAdapter.IviewHolder> implements View.OnClickListener {

    private List<Cate.DataBean> lcdb;
    private Context mContext;

    public RecommeAdapter(Context context, List<Cate.DataBean> lcdb) {
        mContext = context;
        this.lcdb = lcdb;
        if (!"0".equals(StaticObj.getCurrCata().getAppId()) && StaticObj.isEnd)
            this.lcdb.add(0, null);
    }

    @Override
    public IviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RelativeLayout rl = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, null);

        return new IviewHolder(rl);
    }

    @Override
    public void onBindViewHolder(IviewHolder holder, int position) {
        holder.iv.setTag(lcdb.get(position));
        holder.iv.setOnClickListener(this);
        String id = null;
        if (lcdb.get(position) == null) {
            ImageUtils.showImgUrl(StaticObj.getCurrCata().getIcon(), holder.iv);
            holder.iv_next.setVisibility(View.VISIBLE);
            id = StaticObj.getCategory_id();
        } else {
            ImageUtils.showImgUrl(lcdb.get(position).getIcon(), holder.iv);
            id = lcdb.get(position).getCategory_id();
        }
        switch (id) {
            case Constant.CATEGORY_CARTOON:
                holder.iv_type.setImageResource(R.mipmap.ev_icon_carton);
                break;
            case Constant.CATEGORY_PICTURE:
                holder.iv_type.setImageResource(R.mipmap.ev_icon_book);
                break;
            case Constant.CATEGORY_INITIATE:
                holder.iv_type.setImageResource(R.mipmap.ev_icon_game);
                break;
            case Constant.CATEGORY_SONG:
                holder.iv_type.setImageResource(R.mipmap.ev_icon_song);
                break;
        }


    }

    @Override
    public int getItemCount() {
        if (lcdb == null) {
            return 0;
        } else if (lcdb.size() < 7) {
            return lcdb.size();
        } else {
            return 6;
        }
    }

    @Override
    public void onClick(View v) {
        Cate.DataBean cb = (Cate.DataBean) v.getTag();
        MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
        if (cb == null) {//下一集
            Intent intent = null;
            switch (StaticObj.getCategory_id()) {
                case Constant.CATEGORY_CARTOON:
                    intent = new Intent(mContext, CartoonActivity.class);
                    break;
                case Constant.CATEGORY_PICTURE:
                    intent = new Intent(mContext, PaintingActivity.class);
                    break;
            }
            intent.putExtra(Constant.DO, Constant.DO_NEXT);
            mContext.startActivity(intent);
        } else {
            Intent intent = null;

            if (cb.getIsLocal() == 1) {
                switch (cb.getCategory_id()) {
                    case Constant.CATEGORY_CARTOON:
                        intent = new Intent(mContext, CartoonActivity.class);
                        break;
                    case Constant.CATEGORY_SONG:
                        intent = new Intent(mContext, SongActivity.class);
                        break;
                    case Constant.CATEGORY_PICTURE:
                        intent = new Intent(mContext, PaintingActivity.class);
                        break;
                }
                StaticObj.setCategory_id(cb.getCategory_id());
                StaticObj.setCurrCata(cb);
//                intent.putExtra(Constant.DO, Constant.DO_REFSIH);
            } else {
                intent = new Intent(mContext, H5StoreActivity.class);
                String zss = "";
                switch (cb.getCategory_id()) {
                    case Constant.CATEGORY_CARTOON:
                        zss = API.NET_STOREDEILPAGE_VEDIO + cb.getAppId();
                        break;
                    case Constant.CATEGORY_SONG:
                        zss = API.NET_STOREDEILPAGE_MUSIC + cb.getAppId();
                        break;
                    case Constant.CATEGORY_PICTURE:
                        zss = API.NET_STOREDEILPAGE_PIC + cb.getAppId();
                        break;
                    case Constant.CATEGORY_INITIATE:
                        zss = API.NET_STOREDEILPAGE_GAME + cb.getAppId();
                        break;
                }
                if (StaticObj.getUid() != null)
                    intent.putExtra(Constant.WEB_URL, zss);
                else
                    intent.putExtra(Constant.WEB_URL, zss + "?access_token=0000000000000000");
            }
            mContext.startActivity(intent);
        }
    }

    public class IviewHolder extends RecyclerView.ViewHolder {
        public ImageView iv, iv_type;
        private ImageView iv_next;
        public RelativeLayout rl;

        public IviewHolder(View itemView) {
            super(itemView);
            rl = (RelativeLayout) itemView;
            iv = (ImageView) rl.findViewById(R.id.iv_core);
            iv_type = (ImageView) rl.findViewById(R.id.iv_type);
            iv_next = (ImageView) rl.findViewById(R.id.iv_next);

            iv_type.setVisibility(View.VISIBLE);
        }

    }
}
