package com.sinoangel.kids.mode_new.ks.function.cartoon;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.util.Date;

import pl.droidsonroids.gif.GifImageView;

public class WEBViewActivity extends BaseActivity {

    private WebView id_webView;
    private GifImageView gib_bar;
    private ImageView back;
    private CoreDataBean.DataBean dat;

    private long userTime;//使用时长

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);


        id_webView = (WebView) findViewById(R.id.id_webView);
        WebSettings settings = id_webView.getSettings();
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        gib_bar = (GifImageView) findViewById(R.id.gib_bar);
        back = (ImageView) findViewById(R.id.iv_back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dat = (CoreDataBean.DataBean) getIntent().getExtras().get("data");
        if (dat == null) {
            goWebView(getIntent().getStringExtra("url"));
        } else {
            goWebView(dat.getDownUrl());
        }
//        goWebView("XNzkwOTQ2ODcy");

        userTime = new Date().getTime();
    }

    @Override
    protected void onStart() {
        super.onStart();
        MusicUtils.getMusicUtils().playOrPauseBgsc(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void goWebView(String url) {
        if ("".equals(url)) {
            return;
        }


        id_webView.setVisibility(View.VISIBLE);
        ////切记要添加下一行代码，因为Android中的WebView默认是不响应JavaScript控件的，如果不加下面这一行，
        // 就会出现一个很奇怪的问题:加载的网页链接可以点击并跳转，但是按钮点了却没反应。因为很多网页的控件是通
        // 过JS编写的，所以要添加下面一行代码以便响应JS控件。
        id_webView.getSettings().setJavaScriptEnabled(true);
        id_webView.loadUrl(url);
        id_webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                String str = url;
                if (url != null) {
                    if (url.indexOf("https://www.youtube.com/watch?v=") >= 0) {
                        str = url.replace("https://www.youtube.com/watch?v=", "https://www.youtube.com/embed/");
                    }
                    view.loadUrl(str);
                }

                return true;
            }

            //网页加载开始时调用，显示加载提示旋转进度条
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // TODO Auto-generated method stub
                super.onPageStarted(view, url, favicon);
                gib_bar.setVisibility(View.VISIBLE);
            }

            //网页加载完成时调用，隐藏加载提示旋转进度条
            @Override
            public void onPageFinished(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onPageFinished(view, url);
                gib_bar.setVisibility(View.GONE);
            }

            //网页加载失败时调用，隐藏加载提示旋转进度条
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                gib_bar.setVisibility(View.GONE);
                AppUtils.showToast(getString(R.string.net_limitation));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        id_webView.destroy();
        if (dat != null)
            HttpUtil.getUtils().statistics(dat.getAppId(), StaticObj.getCategory_id(), "1", "1", "0", (new Date().getTime() - userTime));
        System.exit(0);
    }

    @Override
    protected void onPause() {
        id_webView.pauseTimers();//当应用程序被切换到后台我们使用了webview， 这个方法不仅仅针对当前的webview而是全局的全应用程序的webview，它会暂停所有webview的layout，parsing，javascripttimer。降低CPU功耗。
        super.onPause();
    }

    @Override
    protected void onPostResume() {
        id_webView.resumeTimers();
        super.onPostResume();
    }

}
