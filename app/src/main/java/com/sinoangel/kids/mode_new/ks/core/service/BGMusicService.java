package com.sinoangel.kids.mode_new.ks.core.service;

import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.aidl.IMusicAidlInterface;


public class BGMusicService extends Service {

    private MediaPlayer mp;//背景音乐

    /**
     * 方法说明：开始播放音乐
     */
    private void startMusic() {
        mp.start();
        // 音乐播放完毕的事件处理
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                // 循环播放
                try {
                    mp.start();
                } catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        // 播放音乐时发生错误的事件处理
        mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub
                // 释放资源
                try {
                    mp.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
    }

    @Override
    public void onCreate() {
        initMusic();
        super.onCreate();
    }

    /**
     * 方法说明：初始化音乐资源
     */
    private void initMusic() {
        try {
            mp = MediaPlayer.create(this, R.raw.ambient_birds);
            // 在MediaPlayer取得播放资源与stop()之后要准备PlayBack的状态前一定要使用MediaPlayer.prepeare()
            mp.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        System.exit(0);
    }

    /**
     * 方法说明：服务停止时停止播放音乐并释放资源
     */
    private void stopMusic() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
//
    }

    @Override
    public IBinder onBind(Intent intent) {
        startMusic();
        return new MyBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopMusic();
        return super.onUnbind(intent);
    }

    public class MyBinder extends IMusicAidlInterface.Stub {
        @Override
        public void palyOrPause(boolean flage) {
            try {
                if (flage) {
                    if (!mp.isPlaying())
                        mp.start();
                } else {
                    if (mp.isPlaying())
                        mp.pause();
                }
            } catch (Exception e) {

            }
        }
    }
}
