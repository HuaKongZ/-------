package com.sinoangel.kids.mode_new.ks.set;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;

import java.util.Timer;
import java.util.TimerTask;

public class LockScreenActivity extends BaseActivity {

    private ImageView iv_unlock;
    private TextView tv1;
    private long now;

    Timer timer = new Timer();

    private Handler timeHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            long fen = now / 1000 / 60 % 60;
            long mm = now / 1000 % 60;
            String time = fen + ":" + (mm < 10 ? "0" : "") + mm;
            tv1.setText(time);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen);

        iv_unlock = (ImageView) findViewById(R.id.iv_unlock);
        tv1 = (TextView) findViewById(R.id.tv1);
        now = SPUtils.getTime() * 60 * 1000;

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                now -= 1000;
                if (now >= 0) {
                    timeHandler.sendEmptyMessage(0);
                } else {
                    finish();
                }
            }
        }, 0, 1000);
        timeHandler.sendEmptyMessage(0);

        iv_unlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogUtils().showULDialog(LockScreenActivity.this, new DialogUtils.UnLockFinish() {
                    @Override
                    public void onULFinish() {
                        MyApplication.getInstance().startLockScreen();
                        timer.cancel();
                        finish();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
