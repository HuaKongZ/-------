package com.sinoangel.kids.mode_new.ks.util;

import android.os.Handler;
import android.os.Message;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.ZanBean;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2016/7/27 0027.
 */
public class API {
    public static final String API = "http://cn.api.sinoangel.cn/";
    public static final String API2 = "http://api2.sinoangel.cn/";//商店
    public static final String NET_GENERAL_CATE = API + "general/cate";//大类
    public static final String NET_GENERAL_DETAIL = API + "general/detail";//小类
    public static final String NET_USER_LOGIN = API + "StoreUsers/login";//登录
    public static final String NET_USER_REGISTER = API + "StoreUsers/register";//注册
    public static final String NET_USER_OTHERLOGIN = API + "OAuth/login";//第三方登录
//    public static final String NET_USER_RESET = API + "StoreUsers/reset";//忘记密码
    public static final String NET_USER_COMMENT = API + "UsersComment";//提交用户评论
    public static final String NET_USER_COMMENT_LIST = API + "UsersComment/list";//用户评论列表
    public static final String NET_USER_POINTLIKECHAT = API + "PointLikeChat";//点赞
    public static final String NET_USER_ISPOINTLIKECHAT = API + "PointLikeChat/isPoint";//获取点赞
    public static final String NET_PRODUCTAGESEGMENT = API + "ProductAgeSegment";//获取年龄段
    public static final String NET_PRODUCTRECOMMEND = API + "ProductRecommend";//获取绘本推荐
    public static final String NET_STATISTICS = API + "statistics/collect";//统计
    public static final String NET_LOGINIMG = API + "HomePush";//登录页广告
    public static final String NET_EVERDAYSHARE = API + "HomePush/dayRecommend";//每日推荐
    public static final String NET_SHARE = API + "share";//抛出的分享连接
    public static final String NET_HOTKEYWORD = API + "UsersComment/hotkeyword";//热点词
    public static final String NET_BLACKLIST = API + "BlackList/Addlist";//加入黑名单
    public static final String NET_REMOVEBLACKLIST = API + "BlackList/RemoveFromList";//移除黑名单
    public static final String NET_CMS = API + "cms";//内容管理
    public static final String NET_UPDATEUSERLANG = API + "OAuth/UpdateUserLang";//修改商店语言
    public static final String NET_GENERAL_GETINFO = API + "general/GetInfo";//商店应用下载地址

    public static final String NET_USER_STORE = API2 + "store";//商店
    public static final String NET_DAYRECOMMEND = API2 + "dayrecommend";//每日推荐订阅地址

    public static final String NET_STOREDEILPAGE_PIC = API2 + "p_detail/picbook/";//绘本商店详情页
    public static final String NET_STOREDEILPAGE_VEDIO = API2 + "p_detail/video/";//视频商店详情页
    public static final String NET_STOREDEILPAGE_MUSIC = API2 + "p_detail/audio/";//音乐商店详情页
    public static final String NET_STOREDEILPAGE_GAME = API2 + "p_detail/app/";//游戏商店详情页
    public static final String NET_FINDPWDBYEMAIL = API2 + "api/StoreUsers/reset";//密码找回

    public static final String NET_WEIXIN = "https://api.weixin.qq.com/sns/userinfo";//获取微信信息
    public static final String NET_QQ = "https://openmobile.qq.com/user/get_simple_userinfo";//获取微信信息

    public static final String NET_LOCOD = "http://api.map.baidu.com/geocoder/v2/?ak=rGIEcHDH2Op56GCx8vGvA3k6&callback=renderReverse&output=json&pois=1&location=";//获取微信信息
    public static final String NET_IP = "http://api.map.baidu.com/location/ip?ak=rGIEcHDH2Op56GCx8vGvA3k6&coor=bd09ll&ip=";

    public static String getURL(String api, Map<String, String> hmss) {
        Set<String> ss = hmss.keySet();
        StringBuffer sb = new StringBuffer("?");
        for (String str : ss) {
            sb.append(str + "=" + hmss.get(str) + "&");
        }
        sb.deleteCharAt(sb.length() - 1);
        String url = api + sb.toString();
        AppUtils.outputLog(url);
        return url;
    }

    /**
     * 大类
     */
    public static String generalCate() {
        Map<String, String> mss = new HashMap<>();
        mss.put("category_id", StaticObj.getCategory_id());
        mss.put("age", MyApplication.DayTime() + "");
        mss.put("sex", "1");
        mss.put("lang_id", MyApplication.systemLanguageType() + "");
        mss.put("channel_id", MyApplication.getAppKey());
        mss.put("tokenId", SPUtils.getPhoneId());
        mss.put("location", SPUtils.getLocation());
        if (StaticObj.getUid() != null)
            mss.put("userid", StaticObj.getUid().getUserId() + "");
        return getURL(NET_GENERAL_CATE, mss);
    }

    /**
     * 小类
     */
    public static String generalDetail() {
        Map<String, String> mss = new HashMap<>();
        mss.put("category_id", StaticObj.getCategory_id());
        mss.put("age", MyApplication.DayTime() + "");
        mss.put("sex", "1");
        mss.put("lang_id", MyApplication.systemLanguageType() + "");
        mss.put("channel_id", MyApplication.getAppKey());
        mss.put("tokenId", SPUtils.getPhoneId());
        mss.put("album_id", StaticObj.getCurrCata().getAppId());
        mss.put("location", SPUtils.getLocation());
        if (StaticObj.getUid() != null)
            mss.put("userid", StaticObj.getUid().getUserId() + "");
        return getURL(NET_GENERAL_DETAIL, mss);
    }

    /**
     * 用户登录
     */
    public static String userLogin(String api, Map<String, String> mss) {
        return getURL(api, mss);
    }

    /**
     * 用户注册
     */
    public static String userRegister(Map<String, String> mss) {
        return getURL(NET_USER_REGISTER, mss);
    }

    /**
     * 登录商店
     */
    public static String comeStore(String token) {
        Map<String, String> mss = new HashMap<>();
        mss.put("access_token", token);
        mss.put("cAge", MyApplication.DayTime()+"");
        return getURL(NET_USER_STORE, mss);
    }

    /**
     * 用户评论列表
     */
    public static String getNetUserCommentList(Map<String, String> mss) {
        return getURL(NET_USER_COMMENT_LIST, mss);
    }

    /**
     * 获取年龄段
     */
//    public static String getAgeByCard(Map<String, String> mss) {
//        return getURL(NET_PRODUCTAGESEGMENT, mss);
//    }


    /**
     * 统计
     */
    public static String statistics(Map<String, String> mss) {
        return getURL(NET_STATISTICS, mss);
    }

    /**
     * 抛出的分享地址
     */
    public static String getShare(String id) {
        Map<String, String> mss = new HashMap<>();
        mss.put("singleId", id);
        mss.put("catId", StaticObj.getCategory_id());
        mss.put("seriesId", StaticObj.getCurrCata().getAppId());
        return getURL(NET_SHARE, mss);
    }


    /**
     * 登录页广告
     */
    public static String LoginImg() {
        Map<String, String> mss = new HashMap<>();
        mss.put("location", SPUtils.getLocation());
        mss.put("lang", MyApplication.systemLanguageType() + "");
        return getURL(NET_LOGINIMG, mss);
    }

    /**
     * 微信
     */
    public static String WeiXin(Map<String, String> mss) {
        return getURL(NET_WEIXIN, mss);
    }

    /**
     * QQ
     */
    public static String QQ(Map<String, String> mss) {
        return getURL(NET_QQ, mss);
    }


    /**
     * 获取关键词
     */
    public static String getHotkeyword() {
        Map<String, String> mss = new HashMap<>();
        mss.put("albumId", StaticObj.getCurrCata().getAppId());
        mss.put("carrierId", StaticObj.getCategory_id());
        mss.put("lang_id", MyApplication.systemLanguageType() + "");
        return getURL(NET_HOTKEYWORD, mss);
    }


    /**
     * 获取推荐
     */
    public static String getProductreCommend(String id) {
        Map<String, String> mss = new HashMap<>();
        if (StaticObj.getUid() != null)
            mss.put("userid", StaticObj.getUid().getUserId() + "");
        mss.put("channelId", MyApplication.getInstance().getAppKey());
        mss.put("catId", StaticObj.getCategory_id());
        mss.put("age", MyApplication.DayTime() + "");
        mss.put("singleId", id);
        mss.put("seriesId", StaticObj.getCurrCata().getAppId());

        return getURL(NET_PRODUCTRECOMMEND, mss);
    }


    /**
     * 每日推荐订阅
     */
    public static String getDAYRECOMMEND(String keyword) {
        Map<String, String> mss = new HashMap<>();
        String key = null;
        try {
            key = URLEncoder.encode(keyword, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        mss.put("keyword", key);
        if (StaticObj.getUid() != null)
            mss.put("access_token", StaticObj.getUid().getId());
        else
            mss.put("access_token", "0000000000000000");
        return getURL(NET_DAYRECOMMEND, mss);
    }


    /**
     * 加入黑名单
     */
    public static String getAddBlacklist(String cardID) {
        Map<String, String> mss = new HashMap<>();
        mss.put("carrierid", StaticObj.getCategory_id());
        mss.put("albumId", cardID);
        if (StaticObj.getUid() != null)
            mss.put("userid", StaticObj.getUid().getUserId() + "");
        return getURL(NET_BLACKLIST, mss);
    }

    /**
     * 移除黑名单
     */
    public static String getRemoveBlacklist(String cardID) {
        Map<String, String> mss = new HashMap<>();
        mss.put("carrierid", StaticObj.getCategory_id());
        mss.put("albumId", cardID);
        if (StaticObj.getUid() != null)
            mss.put("userid", StaticObj.getUid().getUserId() + "");
        return getURL(NET_REMOVEBLACKLIST, mss);
    }

//
//    /**
//     * 获取内容管理列表
//     */
//    public static String getContexgtlist() {
//        Map<String, String> mss = new HashMap<>();
//        mss.put("category_id", StaticObj.getCategory_id());
//        mss.put("age", MyApplication.DayTime() + "");
//        mss.put("sex", "1");
//        mss.put("lang_id", MyApplication.systemLanguageType() + "");
//        mss.put("channel_id", MyApplication.getAppKey());
//        mss.put("tokenId", SPUtils.getPhoneId());
//        if (StaticObj.getUid() != null)
//            mss.put("userid", StaticObj.getUid().getUserId() + "");
//        return getURL(NET_GENERAL_DETAIL, mss);
//    }

//    public static String getLocation(String lat, String lon) {
//        return NET_LOCOD + lat + "," + lon;
//    }
//
//    public static String getLocationByIP(String ip) {
//        return NET_IP + ip;
//    }

    /**
     * 获取内容管理列表
     */
    public static String getUpdateuserLang() {
        Map<String, String> mss = new HashMap<>();
        mss.put("lang", MyApplication.systemLanguageType() + "");
        if (StaticObj.getUid() != null)
            mss.put("userid", StaticObj.getUid().getUserId() + "");
        return getURL(NET_UPDATEUSERLANG, mss);
    }

    /**
     * 获取每日推荐
     */
    public static String getEverdayShare() {
        Map<String, String> mss = new HashMap<>();
        mss.put("location", SPUtils.getLocation());
        mss.put("age", MyApplication.DayTime() + "");
//        mss.put("location", "2");
        return getURL(NET_EVERDAYSHARE, mss);
    }

    public static String getAppDownPath(String id) {
        Map<String, String> mss = new HashMap<>();
        mss.put("category_id", Constant.CATEGORY_INITIATE);
        mss.put("album_id", id);
        mss.put("isablum", "0");
        return getURL(NET_GENERAL_GETINFO, mss);
    }

//    public static String outLogin() {
//        Map<String, String> mss = new HashMap<>();
//        mss.put("access_token", StaticObj.getUid().getId());
//        return getURL(NET_OUTLOGIN, mss);
//    }
}
