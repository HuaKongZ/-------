package com.sinoangel.kids.mode_new.ks.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能说明：APK状态管理器
 *
 * @author linf 用来保存APK的状态是下载还是未下载，还是下载中
 */
public class DownManagerUtil {

    /* 判断SD卡是否存在存在就返回SD卡路径 */
    public final String SD_CARD = Environment.getExternalStorageState()
            .equals(Environment.MEDIA_MOUNTED) ? Environment
            .getExternalStorageDirectory().toString() : "";
    // App
    public final String _APPS = "/sinoangel/app/";
    // 未下载
    public static final int DOWNLOAD_NO = 1;
    // 正在下载
    public static final int DOWNLOAD_ING = 2;
    // 暂停
    public static final int DOWNLOAD_PUASE = 3;
    // 安装完成（本地已存在）
    public static final int DOWNLOAD_FINISH = 4;
    // 等待下载
    public static final int DOWNLOAD_WAIT = 5;
    // 下载出错
    public static final int DOWNLOAD_FAIL = 6;

    public static final String DOWNLOAD_ACTION = "download_action";
    // 包名
    public static final String DOWNLOAD_OBJ = "download_obj";

    // 变量说明：网络请求工具
    HttpUtils hu;

    // 变量说明：路径
    public String savePath;
    // 变量说明：上下文
    private Context context;

    private DownManagerUtil(Context context) {
        this.context = context;
        hu = new HttpUtils();
        getSavePath();
    }

    private static DownManagerUtil appManagerUtil;

    public static DownManagerUtil getInstance() {
        if (appManagerUtil == null) {
            appManagerUtil = new DownManagerUtil(MyApplication.getInstance());
        }
        return appManagerUtil;
    }

    /**
     * 方法说明：得到路径
     */
    public String getSavePath() {
        if (context.getExternalFilesDir("") == null) {
            savePath = SD_CARD + _APPS;
        } else {
            savePath = context.getExternalFilesDir("").getAbsolutePath() + _APPS;
        }
        AppUtils.outputLog(savePath);
        return savePath;
    }

    public void stopDownload(CoreDataBean.DataBean data) {
        data.getDd().setDownState(DOWNLOAD_NO);
        StaticObj.ld.remove(data);
        if (mf.get(data.getAppId()) != null)
            mf.get(data.getAppId()).cancel();
    }

    public String file;


    /**
     * 方法说明：
     *
     * @param data
     */
    public void sendReceiver(CoreDataBean.DataBean data) {
        Intent intent = new Intent(DOWNLOAD_ACTION);
        intent.putExtra(DOWNLOAD_OBJ, data);
        context.sendBroadcast(intent);
    }

    /**
     * 判断某个应用是否安装了
     *
     * @param context
     * @param packageName
     * @return
     */
    public static boolean isAvilible(Context context, String packageName) {
        final PackageManager packageManager = context.getPackageManager();
        // 获取所有已安装程序的包信息
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        for (int i = 0; i < pinfo.size(); i++) {
            if (pinfo.get(i).packageName.equalsIgnoreCase(packageName))
                return true;
        }
        return false;
    }

    /**
     * 删除文件下所有文件
     */

    public static boolean deleteAllFile(File file) {
        if (file.isDirectory()) {
            for (File fil : file.listFiles()) {
                if (fil.isDirectory()) {
                    deleteAllFile(fil);
                } else {
                    if (!fil.delete()) {
                        return false;
                    }
                }
            }
        } else {
            if (!file.delete()) {
                return false;
            }
        }
        return true;
    }

    private Map<String, HttpHandler<File>> mf = new HashMap<>();

    public void downFile(final CoreDataBean.DataBean data) {

        if (!HttpUtil.isNetworkAvailable()) {
            AppUtils.showToast(MyApplication.getInstance().getString(R.string.net_warnnetwork));
            return;
        }

        // 拿到要下载的文件名字
        if (data.getDd().getDownState() == DOWNLOAD_ING) {  //正在下载中，不去重复下载
            return;
        }
        StaticObj.ld.add(data);

        data.getDd().setDownState(DOWNLOAD_WAIT);
        sendReceiver(data);//更新状态
        try {
            MyApplication.getInstance().getDbUtisl().saveOrUpdate(data.getDd());
        } catch (DbException e) {
            e.printStackTrace();
        }

        String url = "";
        try {
            url = URLEncoder.encode(data.getDownUrl(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String cc = url.replaceAll("%3A", ":").replaceAll("%2F", "/").replace("+", "%20");
        AppUtils.outputLog(cc);

        final String filePath = savePath + data.getAppId() + "/" + data.getMD5PackageName();

        HttpHandler<File> hhf = hu.download(cc, filePath + ".tmp", true, false, new RequestCallBack<File>() {
            @Override
            public void onSuccess(ResponseInfo<File> responseInfo) {
                data.getDd().setDownState(DOWNLOAD_FINISH);
                sendReceiver(data);
                StaticObj.ld.remove(data);

                try {
                    MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState", "downSize");
                } catch (DbException e) {
                    e.printStackTrace();
                }

                File file = new File(filePath + ".tmp");
                file.renameTo(new File(filePath));

                PackageUtils.install(MyApplication.getInstance(), filePath);
            }

            @Override
            public void onFailure(HttpException he, String s) {
                AppUtils.showToast(context.getString(R.string.net_down_fail));
                data.getDd().setDownState(DOWNLOAD_FAIL);
                try {
                    MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState");
                } catch (DbException e) {
                    e.printStackTrace();
                }
                sendReceiver(data);
                StaticObj.ld.remove(data);

                AppUtils.outputLog(he.getMessage());

            }

            @Override
            public void onLoading(long total, long current, boolean isUploading) {
                super.onLoading(total, current, isUploading);
                double lodingSize = (double) current / total * 100;
                data.getDd().setDownSize((int) lodingSize);
                sendReceiver(data);

            }

            @Override
            public void onStart() {
                super.onStart();
                data.getDd().setDownState(DOWNLOAD_ING);
                try {
                    MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState", "downSize");
                } catch (DbException e) {
                    e.printStackTrace();
                }
                sendReceiver(data);

            }
        });
        mf.put(data.getAppId(), hhf);
    }


}
