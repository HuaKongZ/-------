package com.sinoangel.kids.mode_new.ks.util;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.widget.BelowDialog;

/**
 * Created by Administrator on 2016/8/23 0023.
 */
public class DialogUtils {
    private int coun;
    private static Dialog progressDialog;
    private int a;
    private int b;
    private String daansz;

    public void upNum() {
        a = 10 + (int) (Math.random() * 9);
        b = 2 + (int) (Math.random() * 7);
        daansz = a * b + "";
    }

    public void showULDialog(Context context, final UnLockFinish ulf) {
        upNum();

        final TranslateAnimation animation = new TranslateAnimation(0, -5, 0, 0);
        animation.setInterpolator(new OvershootInterpolator());
        animation.setDuration(50);
        animation.setRepeatCount(5);
        animation.setRepeatMode(Animation.REVERSE);

        final BelowDialog bd = new BelowDialog(context);
        bd.setContentView(R.layout.dialog_unlock);
        final View layout = bd.findViewById(R.id.ll_box);
        ImageView ivb0, ivb1, ivb2, ivb3, ivb4, ivb5, ivb6, ivb7, ivb8, ivb9;
        final TextView tv_prodom = (TextView) bd.findViewById(R.id.tv_prodom);
        final TextView tv_daan = (TextView) bd.findViewById(R.id.tv_daan);
        tv_prodom.setText(a + "  x  " + b + "  = ");
        ivb0 = (ImageView) bd.findViewById(R.id.ivb0);
        ivb1 = (ImageView) bd.findViewById(R.id.ivb1);
        ivb2 = (ImageView) bd.findViewById(R.id.ivb2);
        ivb3 = (ImageView) bd.findViewById(R.id.ivb3);
        ivb4 = (ImageView) bd.findViewById(R.id.ivb4);
        ivb5 = (ImageView) bd.findViewById(R.id.ivb5);
        ivb6 = (ImageView) bd.findViewById(R.id.ivb6);
        ivb7 = (ImageView) bd.findViewById(R.id.ivb7);
        ivb8 = (ImageView) bd.findViewById(R.id.ivb8);
        ivb9 = (ImageView) bd.findViewById(R.id.ivb9);

        View.OnClickListener voc = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.ivb0:
                        coun = 0;
                        break;
                    case R.id.ivb1:
                        coun = 1;
                        break;
                    case R.id.ivb2:
                        coun = 2;
                        break;
                    case R.id.ivb3:
                        coun = 3;
                        break;
                    case R.id.ivb4:
                        coun = 4;
                        break;
                    case R.id.ivb5:
                        coun = 5;
                        break;
                    case R.id.ivb6:
                        coun = 6;
                        break;
                    case R.id.ivb7:
                        coun = 7;
                        break;
                    case R.id.ivb8:
                        coun = 8;
                        break;
                    case R.id.ivb9:
                        coun = 9;
                        break;
                }
                tv_daan.setText(tv_daan.getText().toString() + coun);

            }
        };

        tv_daan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                if (str.length() == 0)
                    return;
                int dex = str.length() - 1;
                if (str.charAt(dex) != daansz.charAt(dex)) {
                    layout.startAnimation(animation);
                    upNum();
                    tv_prodom.setText(a + "  x  " + b + "  = ");
                    tv_daan.setText("");
                } else if (str.equals(a * b + "")) {
                    ulf.onULFinish();
                    bd.dismiss();
                }
            }
        });

        ivb0.setOnClickListener(voc);
        ivb1.setOnClickListener(voc);
        ivb2.setOnClickListener(voc);
        ivb3.setOnClickListener(voc);
        ivb4.setOnClickListener(voc);
        ivb5.setOnClickListener(voc);
        ivb6.setOnClickListener(voc);
        ivb7.setOnClickListener(voc);
        ivb8.setOnClickListener(voc);
        ivb9.setOnClickListener(voc);
        bd.show();
    }

    /**
     * 显示正在加载的进度条
     */
    public static void showProgressDialog(Context context, String str) {
        if (null != progressDialog && progressDialog.isShowing()) {
        } else {
            progressDialog = new Dialog(context, R.style.customDialog);
            progressDialog.setContentView(R.layout.dialog_progress_layout);
            progressDialog.setCancelable(true);
            TextView tv = (TextView) progressDialog.findViewById(R.id.tv_str);
            tv.setText(str);

            WindowManager.LayoutParams lp = progressDialog.getWindow().getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            progressDialog.getWindow().setAttributes(lp);
            progressDialog.show();
        }
    }

    /**
     * 隐藏正在加载的进度条
     */
    public static void dismissProgressDialog() {
        try {
            if (null != progressDialog && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
        }
    }


    public static void YesOrNoDialog(Context context, int imgid, final YeOrOnListener yl) {
        final BelowDialog bd = new BelowDialog(context);
        bd.setContentView(R.layout.dialog_yes_no);
        ImageView iv = (ImageView) bd.findViewById(R.id.iv_title);
        iv.setImageResource(imgid);
        ImageView iv_ok = (ImageView) bd.findViewById(R.id.iv_ok);
        ImageView iv_no = (ImageView) bd.findViewById(R.id.iv_no);

        iv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yl.onOk();
                bd.dismiss();
            }
        });
        iv_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yl.onNo();
                bd.dismiss();
            }
        });
        bd.show();

    }


    public static void showDesDialog(Context context, Window win, int id, final BtnStartListener dsl) {
        final BelowDialog dialog = new BelowDialog(context, win);
        if ("0".equals(StaticObj.getCurrCata().getAppId())) {
            dialog.setContentView(R.layout.dialog_prompt);
            ImageView iv = (ImageView) dialog.findViewById(R.id.iv_text);
            iv.setImageResource(R.mipmap.text_my_down);
        } else if ("-1".equals(StaticObj.getCurrCata().getAppId())) {
            dialog.setContentView(R.layout.dialog_prompt);
            ImageView iv = (ImageView) dialog.findViewById(R.id.iv_text);
            if (Constant.CATEGORY_CARTOON.equals(StaticObj.getCategory_id()))
                iv.setImageResource(R.mipmap.text_dianying);
            else
                iv.setImageResource(R.mipmap.text_danxingben);
        } else {
            dialog.setContentView(R.layout.dialog_book_info);
            Button btn_new = (Button) dialog.findViewById(R.id.btn_new);
            ImageView iv = (ImageView) dialog.findViewById(R.id.iv_img);
            TextView tv_age = (TextView) dialog.findViewById(R.id.tv_age);
            TextView tv_time = (TextView) dialog.findViewById(R.id.tv_time);
            TextView tv_context = (TextView) dialog.findViewById(R.id.tv_context);
            TextView tv_stye = (TextView) dialog.findViewById(R.id.tv_stye);
            ImageUtils.showImgUrl(StaticObj.getCurrCata().getIcon(), iv);

            tv_age.setText(context.getString(R.string.age) + StaticObj.getCurrCata().getAlbumAges());
            tv_time.setText(context.getString(R.string.shijian) + StaticObj.getCurrCata().getDuration() + context.getString(R.string.min));
            tv_stye.setText(context.getString(R.string.fenlei) + StaticObj.getCurrCata().getCategoryNames());
            tv_context.setText(context.getString(R.string.jianjie) + StaticObj.getCurrCata().getAppDesc());


            btn_new.setBackgroundResource(id);

            btn_new.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                    dsl.onDoSome();
                    dialog.dismiss();
                }
            });
        }
        try {
            dialog.show();
        } catch (Exception e) {

        }
    }

//    public static void showShareDialog(final Activity context, final CoreDataBean.DataBean dat) {
//        final BelowDialog dialog = new BelowDialog(context);
//        dialog.setContentView(R.layout.dialog_share);
//        View fb = dialog.findViewById(R.id.ll_facebook);
//        View twwer = dialog.findViewById(R.id.ll_twwer);
//        View guge = dialog.findViewById(R.id.ll_geoogle);
//
//        View.OnClickListener doc = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (v.getId()) {
//                    case R.id.ll_facebook:
//                        String name = "0".equals(StaticObj.getCurrCata().getAppId()) ? "" : "---" + StaticObj.getCurrCata().getAppName();
//                        ShareLinkContent content = new ShareLinkContent.Builder()
//                                .setContentUrl(Uri.parse(dat.getIcon()))
//                                .setContentTitle(context.getString(R.string.app_name) + name)
//                                .setContentDescription(StaticObj.getCurrCata().getAppDesc())
//                                .build();
//                        ShareApi.share(content, new FacebookCallback<Sharer.Result>() {
//                            @Override
//                            public void onSuccess(Sharer.Result result) {
//                                AppUtils.showToast("cc");
//                            }
//
//                            @Override
//                            public void onCancel() {
//                                AppUtils.showToast("can");
//                            }
//
//                            @Override
//                            public void onError(FacebookException error) {
//                                AppUtils.outputLog(error.getMessage());
//                                AppUtils.showToast("er");
//                            }
//                        });
//                        break;
//                    case R.id.ll_twwer:
//                        break;
//                    case R.id.ll_geoogle:
//                        break;
//
//                }
//                dialog.dismiss();
//            }
//        };
//        fb.setOnClickListener(doc);
//        twwer.setOnClickListener(doc);
//        guge.setOnClickListener(doc);
//        dialog.show();
//    }

    public interface BtnStartListener {
        void onDoSome();
    }

    public interface UnLockFinish {
        void onULFinish();
    }

    public interface YeOrOnListener {
        void onOk();

        void onNo();
    }
}
