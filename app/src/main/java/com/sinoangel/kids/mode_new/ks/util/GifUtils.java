package com.sinoangel.kids.mode_new.ks.util;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;

import java.io.File;

/**
 * Created by Administrator on 2016/8/4 0004.
 */
public class GifUtils {
    public static GifUtils gifUtils;
    private HttpUtils hu;

    private GifUtils() {
        hu = new HttpUtils();
    }

    public static GifUtils getGIFUtils() {
        if (gifUtils == null) {
            gifUtils = new GifUtils();
        }
        return gifUtils;
    }

    public void DownGif(String url, final String name, final GifListener gl) {
        final File savePath = new File(DownManagerUtil.getInstance().getSavePath() + "adv/");
        if (!savePath.exists()) {
            savePath.mkdirs();
        }
        final String filePath = savePath.getAbsolutePath() + "/" + name;
        hu.download(url, savePath.getAbsolutePath() + "/" + name, false, false, new RequestCallBack<File>() {
            @Override
            public void onSuccess(ResponseInfo<File> responseInfo) {
                gl.onGifFinish(filePath);
            }

            @Override
            public void onFailure(HttpException e, String s) {

            }
        });
//        DownloadRequest downloadRequest = NoHttp.createDownloadRequest(url, RequestMethod.GET, savePath.getAbsolutePath(), name, false, true);
//        downloadQueue.add(0, downloadRequest, new DownloadListener() {
//            @Override
//            public void onDownloadError(int what, Exception exception) {
//
//            }
//
//            @Override
//            public void onStart(int what, boolean isResume, long rangeSize, Headers responseHeaders, long allCount) {
//
//            }
//
//            @Override
//            public void onProgress(int what, int progress, long fileCount) {
//
//            }
//
//            @Override
//            public void onFinish(int what, String filePath) {
//                gl.onGifFinish(filePath);
//            }
//
//            @Override
//            public void onCancel(int what) {
//
//            }
//        });
    }

    public interface GifListener {
        void onGifFinish(String filePath);
    }
}
