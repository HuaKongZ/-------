package com.sinoangel.kids.mode_new.ks.function.book;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.function.book.adapter.PaintingAdapter;
import com.sinoangel.kids.mode_new.ks.function.book.adapter.PaintingGVAdapter;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.squareup.okhttp.Call;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * 类说明：绘本屋类
 * 功能说明：根据跳转时候确定的数值来判断是动画屋还是绘本屋，然后根据url的字符串获取数据 从而得到绘本屋和动画屋的icon下载地址
 * 编写日期: 2016-2-2
 * 作者: 闻铭 </pre>
 */
public class PaintingActivity extends BaseActivity implements OnClickListener {
    // 变量说明：返回按钮
    private ImageView back, iv_info;
    private TextView tv_title;
    private RecyclerView rv_back, rv_list;
    private PaintingGVAdapter pGVAdpter; // 适配器
    private PaintingAdapter pAdapter;
    private List<CoreDataBean.DataBean> baseAppInfos;
    private GifImageView bar;
    private Call call;

    private long userTime;//使用时长

    private MyHandler animHandler = new MyHandler(this);

    private String idflage = "";


    private static class MyHandler extends Handler {
        WeakReference<PaintingActivity> wrpa;

        public MyHandler(PaintingActivity pa) {
            super();
            wrpa = new WeakReference<>(pa);
        }

        @Override
        public void handleMessage(Message msg) {
            final PaintingActivity pa = wrpa.get();
            switch (msg.what) {
                case 0:
                    // 展示集合
                    pa.pGVAdpter.setData(pa.baseAppInfos);
                    pa.pAdapter.setData(pa.baseAppInfos);

                    if (SPUtils.getIsExists(StaticObj.getCategory_id() + StaticObj.getCurrCata().getAppId())) {
                        SPUtils.putIsExists(StaticObj.getCategory_id() + StaticObj.getCurrCata().getAppId());
                        DialogUtils.showDesDialog(pa, pa.getWindow(), R.mipmap.btn_read, new DialogUtils.BtnStartListener() {
                            @Override
                            public void onDoSome() {
                                pa.pGVAdpter.fastItem();
                            }
                        });
                    }
                    pa.iv_info.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    AppUtils.showToast(pa.getString(R.string.net_warnnetwork));
                    break;
            }
            pa.bar.setVisibility(View.GONE);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painting);
        init();

        if ("0".equals(StaticObj.getCurrCata().getAppId())) {
            getNativeData(false);
        } else if (!HttpUtil.isNetworkAvailable()) {
            getNativeData(true);
        } else if (HttpUtil.isNetworkAvailable()) {
            getNetData();
            idflage = StaticObj.getCurrCata().getAppId();
        }

        tv_title.setText(StaticObj.getCurrCata().getAppName());

        userTime = new Date().getTime();

    }

    @Override
    protected void onStart() {
        super.onStart();

        // 注册广播接收机制 : 更新应用列表状态
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(DownManagerUtil.DOWNLOAD_ACTION); // progress
        this.registerReceiver(AppReceiver, mIntentFilter);

        if (!"0".equals(StaticObj.getCurrCata().getAppId()))
            if (!idflage.equals(StaticObj.getCurrCata().getAppId())) {
                if (HttpUtil.isNetworkAvailable()) {
                    bar.setVisibility(View.VISIBLE);
                    baseAppInfos.clear();
                    iv_info.setVisibility(View.GONE);
                    pGVAdpter.notifyDataSetChanged();
                    tv_title.setText(StaticObj.getCurrCata().getAppName());
                    getNetData();
                    idflage = StaticObj.getCurrCata().getAppId();
                }
            }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(AppReceiver);
        if (call != null)
            call.cancel();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * 方法说明：初始化所有控件
     */
    private void init() {
        back = (ImageView) findViewById(R.id.painting_back);
        rv_back = (RecyclerView) findViewById(R.id.rv_back);
        rv_list = (RecyclerView) findViewById(R.id.rv_list);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_info = (ImageView) findViewById(R.id.iv_info);
        bar = (GifImageView) findViewById(R.id.gib_bar);

        back.setOnClickListener(this);
        iv_info.setOnClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        // 设置布局管理器
        rv_back.setLayoutManager(layoutManager);

        rv_list.setLayoutManager(new GridLayoutManager(this, 4));


        rv_list.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                rv_back.scrollBy(0, dy);
            }
        });

        pGVAdpter = new PaintingGVAdapter();
        pAdapter = new PaintingAdapter();
        rv_back.setAdapter(pAdapter);
        rv_list.setAdapter(pGVAdpter);

    }

    private void getNativeData(boolean flage) {
        try {

            if (flage) {
                Selector selec = Selector.from(CoreDataBean.DataBean.class);
                selec.expr("cardID = '" + StaticObj.getCurrCata().getAppId() + "' AND categoryID = '" + Constant.CATEGORY_PICTURE + "'");
                baseAppInfos = MyApplication.getInstance().getDbUtisl().findAll(selec);

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_PICTURE + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (baseAppInfos != null)
                            for (CoreDataBean.DataBean db : baseAppInfos) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    break;
                                }
                            }
                    }

            } else {

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr(" categoryID =" + Constant.CATEGORY_PICTURE + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                Selector sel_db = Selector.from(CoreDataBean.DataBean.class);
                sel_db.expr(" categoryID =" + Constant.CATEGORY_PICTURE);
                List<CoreDataBean.DataBean> ldb = MyApplication.getInstance().getDbUtisl().findAll(sel_db);

                baseAppInfos = new ArrayList<>();
                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (ldb != null)
                            for (CoreDataBean.DataBean db : ldb) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    baseAppInfos.add(db);
                                    break;
                                }
                            }
                    }
            }

            if (baseAppInfos.size() > 0)
                animHandler.sendEmptyMessage(0);
            else
                animHandler.sendEmptyMessage(1);
        } catch (Exception e) {
            animHandler.sendEmptyMessage(1);
        }
    }

    private void getNetData() {

        String url = API.generalDetail();
        call = HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                animHandler.sendEmptyMessage(1);

            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    CoreDataBean dat = JSON.parseObject(json, CoreDataBean.class);
                    baseAppInfos = dat.getData();

                    MyApplication.getInstance().saveItem(dat.getData());

                    Selector selec = Selector.from(CoreDataBean.DownData.class);
                    selec.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_PICTURE + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");


                    List<CoreDataBean.DownData> lcd = MyApplication.getInstance().getDbUtisl().findAll(selec);
                    if (lcd != null)
                        for (CoreDataBean.DownData dd : lcd) {
                            if (baseAppInfos != null)
                                for (CoreDataBean.DataBean cdb : baseAppInfos) {
                                    if (dd.getAppId().equals(cdb.getAppId())) {
                                        cdb.setDd(dd);
                                        break;
                                    }
                                }
                        }
                    if (baseAppInfos.size() > 0)
                        animHandler.sendEmptyMessageDelayed(0, 1500);
                    else
                        animHandler.sendEmptyMessage(1);
                } catch (DbException de) {
                    try {
                        MyApplication.getInstance().getDbUtisl().dropTable(CoreDataBean.DataBean.class);
                        getNetData();
                    } catch (DbException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    animHandler.sendEmptyMessage(1);
                    e.printStackTrace();
                }

            }
        });


    }

    /**
     * 方法说明：下载的服务
     */
    private BroadcastReceiver AppReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(),
                    DownManagerUtil.DOWNLOAD_ACTION)) {
                if (pGVAdpter != null) {
                    CoreDataBean.DataBean data = (CoreDataBean.DataBean) intent.getSerializableExtra(DownManagerUtil.DOWNLOAD_OBJ);
                    pGVAdpter.notifyItemChanged(data.getDd().getPosation());
                }
            }
        }
    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent == null)
            return;
        if (!TextUtils.isEmpty(intent.getStringExtra(Constant.DO)))
            switch (intent.getStringExtra(Constant.DO)) {
                case Constant.DO_NEXT:
                    pGVAdpter.nextItem();
                    break;
            }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_anmi:
                MusicUtils.getMusicUtils().playSound(R.raw.supergem_chainreaction_5);
                break;
            case R.id.painting_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_info:
                MusicUtils.getMusicUtils().playSound(R.raw.supergem_chainreaction_5);
                DialogUtils.showDesDialog(PaintingActivity.this, getWindow(), R.mipmap.btn_read, new DialogUtils.BtnStartListener() {
                    @Override
                    public void onDoSome() {
                        pGVAdpter.fastItem();
                    }
                });
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        animHandler.removeCallbacksAndMessages(this);
        HttpUtil.getUtils().statistics(StaticObj.getCurrCata().getAppId(), StaticObj.getCategory_id(), "1", "1", "0", (new Date().getTime() - userTime));
    }
}
