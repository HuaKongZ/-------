package com.sinoangel.kids.mode_new.ks.util;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.set.LockScreenActivity;

/**
 * Created by Z on 2016/12/1.
 */

public class LockSrceenUtils {

    private static LockSrceenUtils lsu;

    private LockSrceenUtils() {
    }

    public static LockSrceenUtils getLsu() {
        if (lsu == null)
            lsu = new LockSrceenUtils();
        return lsu;
    }


    public void startLockScreen() {
        long time = SPUtils.getTime();
        lockSrceenHandler.removeMessages(0);
        if (time != 0) {
            lockSrceenHandler.sendEmptyMessageDelayed(0, time * 60 * 1000);
            StaticObj.isCountDown = true;
        }
    }

    public void stopLockScreen() {
        lockSrceenHandler.removeMessages(0);
        StaticObj.isCountDown = false;
    }

    private Handler lockSrceenHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MyApplication.getInstance().startActivity(new Intent(MyApplication.getInstance(), LockScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    };
}
