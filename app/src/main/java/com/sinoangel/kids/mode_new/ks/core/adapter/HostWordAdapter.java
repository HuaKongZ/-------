package com.sinoangel.kids.mode_new.ks.core.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.bean.HostWordBean;

import java.util.List;

/**
 * Created by Administrator on 2016/8/18 0018.
 */
public class HostWordAdapter extends RecyclerView.Adapter<HostWordAdapter.MYViewHolder> implements View.OnClickListener {

    private EditText et;

    private List<HostWordBean.DataBean> lhdb;

    private int[] colorB = {R.drawable.bk_color_1, R.drawable.bk_color_2, R.drawable.bk_color_3, R.drawable.bk_color_4, R.drawable.bk_color_5, R.drawable.bk_color_6, R.drawable.bk_color_7};

    public HostWordAdapter(EditText et) {
        this.et = et;
    }

    @Override
    public MYViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout rl = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tv_hostword, null);
        MYViewHolder mh = new MYViewHolder(rl);
        return mh;
    }

    @Override
    public int getItemCount() {
        return lhdb == null ? 0 : lhdb.size();
    }

    @Override
    public void onBindViewHolder(MYViewHolder holder, int position) {
        holder.tv.setText(lhdb.get(position).getKey());
        int i = position % colorB.length;
        holder.tv.setBackgroundResource(colorB[i]);

        holder.tv.setTag(lhdb.get(position).getWord());
        holder.tv.setOnClickListener(this);

    }

    public void setData(List<HostWordBean.DataBean> lhdb) {
        this.lhdb = lhdb;
    }

    @Override
    public void onClick(View v) {
        String str = (String) v.getTag();
        et.setText(et.getText().toString() + str);
    }

    public class MYViewHolder extends RecyclerView.ViewHolder {
        public TextView tv;

        public MYViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }

}
