package com.sinoangel.kids.mode_new.ks.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.bean.HostWordBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 流式标签(动态的，根据传入的数据动态添加标签)
 */
public class DynamicTagFlowLayout extends ViewGroup {

//    private List<String> mTags = new ArrayList<String>();

    private int[] colorB = {R.drawable.bk_color_1, R.drawable.bk_color_2, R.drawable.bk_color_3, R.drawable.bk_color_4, R.drawable.bk_color_5, R.drawable.bk_color_6, R.drawable.bk_color_7};

    public DynamicTagFlowLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DynamicTagFlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DynamicTagFlowLayout(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        //当前ViewGroup的总高度
        int totalHeight = 0;
        //所有行中的最大宽度
        int maxLineWidth = 0;

        //当前行的最大高度
        int lineMaxHeight = 0;
        //当前行的总宽度
        int currentLineWidth = 0;

        //每个childView所占用的宽度
        int childViewWidthSpace = 0;
        //每个childView所占用的高度
        int childViewHeightSpace = 0;

        int count = getChildCount();
//        LayoutParams layoutParams;

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);

            if (child.getVisibility() != View.GONE) {//只有当这个View能够显示的时候才去测量
                //测量每个子View，以获取子View的宽和高
                measureChild(child, widthMeasureSpec, heightMeasureSpec);

//                layoutParams = (LayoutParams) child.getLayoutParams();

                childViewWidthSpace = child.getMeasuredWidth();
                childViewHeightSpace = child.getMeasuredHeight();

                if (currentLineWidth + childViewWidthSpace > widthSize) {//表示如果当前行再加上现在这个子View，就会超出总的规定宽度，需要另起一行
                    totalHeight += lineMaxHeight;
                    if (maxLineWidth < currentLineWidth) {//如果行的最长宽度发生了变化，更新保存的最长宽度
                        maxLineWidth = currentLineWidth;
                    }
                    currentLineWidth = childViewWidthSpace;//另起一行后，需要重置当前行宽
                    lineMaxHeight = childViewHeightSpace;
                } else {//表示当前行可以继续添加子元素
                    currentLineWidth += childViewWidthSpace;
                    if (lineMaxHeight < childViewHeightSpace) {
                        lineMaxHeight = childViewHeightSpace;
                    }
                }
            }
        }

        setMeasuredDimension(widthMode == MeasureSpec.EXACTLY ? widthSize : maxLineWidth, heightMode == MeasureSpec.EXACTLY ? heightSize : totalHeight);

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        //当前是第几行
        int currentLine = 1;
        //存放每一行的最大高度
        List<Integer> lineMaxHeightList = new ArrayList<Integer>();

        //每个childView所占用的宽度
        int childViewWidthSpace = 0;
        //每个childView所占用的高度
        int childViewHeightSpace = 0;

        //当前行的最大高度
        int lineMaxHeight = 0;
        //当前行的总宽度
        int currentLineWidth = 0;

        int count = getChildCount();
//        LayoutParams layoutParams;

        for (int i = 0; i < count; i++) {
            int cl = 0, ct = 0, cr = 0, cb = 0;
            View child = getChildAt(i);
            if (child.getVisibility() != View.GONE) {//只有当这个View能够显示的时候才去测量

//                layoutParams = (LayoutParams) child.getLayoutParams();
                childViewWidthSpace = child.getMeasuredWidth();
                childViewHeightSpace = child.getMeasuredHeight();

//                System.out.println("getWidth()---->" + getWidth());

                if (currentLineWidth + childViewWidthSpace > getWidth()) {//表示如果当前行再加上现在这个子View，就会超出总的规定宽度，需要另起一行
                    lineMaxHeightList.add(lineMaxHeight);//此时先将这一行的最大高度加入到集合中
                    //新的一行，重置一些参数
                    currentLine++;
                    currentLineWidth = childViewWidthSpace;
                    lineMaxHeight = childViewHeightSpace;

//                    cl = layoutParams.leftMargin;
                    if (currentLine > 1) {
                        for (int j = 0; j < currentLine - 1; j++) {
                            ct += lineMaxHeightList.get(j);
                        }
//                        ct += layoutParams.topMargin;
                    } else {
//                        ct = layoutParams.topMargin;
                    }
                } else {//表示当前行可以继续添加子元素
                    cl = currentLineWidth;
                    if (currentLine > 1) {
                        for (int j = 0; j < currentLine - 1; j++) {
                            ct += lineMaxHeightList.get(j);
                        }
//                        ct += layoutParams.topMargin;
                    } else {
//                        ct = layoutParams.topMargin;
                    }
                    currentLineWidth += childViewWidthSpace;
                    if (lineMaxHeight < childViewHeightSpace) {
                        lineMaxHeight = childViewHeightSpace;
                    }
                }

                cr = cl + child.getMeasuredWidth();
                cb = ct + child.getMeasuredHeight();

                child.layout(cl, ct, cr, cb);

            }
        }
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }

    public void setTags(List<HostWordBean.DataBean> lhdb, final EditText et) {
        if (lhdb != null) {
//            mTags.clear();
//            mTags.addAll(tags);
            for (int i = 0; i < lhdb.size(); i++) {

                RelativeLayout rl = (RelativeLayout) LayoutInflater.from(getContext()).inflate(R.layout.item_tv_hostword, null);

                TextView tv = (TextView) rl.findViewById(R.id.tv_text);

                tv.setText(lhdb.get(i).getKey());

                int index = i % colorB.length;
                tv.setBackgroundResource(colorB[index]);

                final String str = lhdb.get(i).getWord();
                tv.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        et.setText(et.getText().toString() + str);
                    }
                });

                addView(rl);
            }
            requestLayout();
        }
    }

//    private OnTagItemClickListener listener;

//    public interface OnTagItemClickListener {
//        public void onClick(View v);
//    }

//    public void setOnTagItemClickListener(OnTagItemClickListener l) {
//        listener = l;
//    }

}

