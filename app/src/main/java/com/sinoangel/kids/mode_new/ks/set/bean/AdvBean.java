package com.sinoangel.kids.mode_new.ks.set.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/8/2 0002.
 */
public class AdvBean {


    /**
     * flag : 1
     * error : []
     * data : [{"icon":"http://cn.img.store.sinoangel.cn/icons/learn.gif","resourceType":2,"url":"XMTI4NzYzNjUxNg","position":"0"},{"icon":"http://cn.img.store.sinoangel.cn/icons/jiemu.gif","resourceType":2,"url":"XMTQ3NjAwODc3Ng","position":"4"},{"icon":"http://cn.img.store.sinoangel.cn/icons/song.gif","resourceType":2,"url":"XMTU2Njk4MzQ1Mg","position":"5"},{"icon":"http://cn.img.store.sinoangel.cn/icons/cartoon.gif","resourceType":2,"url":"XMTY3ODMwNzQyMA","position":"10"}]
     */

    private int flag;
    private List<?> error;
    /**
     * icon : http://cn.img.store.sinoangel.cn/icons/learn.gif
     * resourceType : 2
     * url : XMTI4NzYzNjUxNg
     * position : 0
     */

    private List<DataBean> data;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public List<?> getError() {
        return error;
    }

    public void setError(List<?> error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String icon;
        private String resourceType;
        private String url;
        private int position;

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getResourceType() {
            return resourceType;
        }

        public void setResourceType(String resourceType) {
            this.resourceType = resourceType;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
}
