package com.sinoangel.kids.mode_new.ks.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;

/**
 * Created by Administrator on 2015/10/10.
 */
public class BelowDialog extends Dialog {

    //自定义大小的
    public BelowDialog(Context context) {
        super(context, R.style.customDialog);
        //设置点击边缘关闭对话框
        setCanceledOnTouchOutside(true);
        //位置居中
        final Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
        window.setWindowAnimations(R.style.Addressanim);

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
            }
        });

    }

    //自定义大小的
    public BelowDialog(Context context, final Window win) {
        super(context, R.style.customDialog);
        //设置点击边缘关闭对话框
        setCanceledOnTouchOutside(true);
        //位置居中
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.CENTER;
        window.setAttributes(params);
        window.setWindowAnimations(R.style.Addressanim);

        setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                backgroundAlpha(1f, win);
            }
        });
        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                backgroundAlpha(0.6f, win);
            }
        });
    }

    public void backgroundAlpha(float bgAlpha, Window win) {
        if (win != null) {
            WindowManager.LayoutParams lp = win.getAttributes();
            lp.alpha = bgAlpha; //0.0-1.0
            win.setAttributes(lp);
        }
    }
}
