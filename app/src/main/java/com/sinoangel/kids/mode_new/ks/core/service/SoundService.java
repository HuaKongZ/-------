package com.sinoangel.kids.mode_new.ks.core.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;

import com.sinoangel.kids.mode_new.ks.core.aidl.ISondAidlInterface;

import java.io.IOException;


public class SoundService extends Service {

    private MediaPlayer mp;//音效

    /**
     * 方法说明：开始播放音乐
     */
    private void startMusic() {
        mp.start();
        // 音乐播放完毕的事件处理
        // 播放音乐时发生错误的事件处理
        mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            public boolean onError(MediaPlayer mp, int what, int extra) {
                // TODO Auto-generated method stub
                // 释放资源
                try {
                    mp.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return false;
            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * 方法说明：初始化音乐资源
     */
    private void initMusic(int id) {
        try {

            // 将音乐保存在res/raw/angelmusic.mp3
            mp = MediaPlayer.create(this, id);
            // 在MediaPlayer取得播放资源与stop()之后要准备PlayBack的状态前一定要使用MediaPlayer.prepeare()
            mp.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        System.exit(0);
    }

    /**
     * 方法说明：服务停止时停止播放音乐并释放资源
     */
    private void stopMusic() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new SoundContrl();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopMusic();
        return super.onUnbind(intent);
    }

    public class SoundContrl extends ISondAidlInterface.Stub{

        @Override
        public void playSound(int id) {
            initMusic(id);
            startMusic();
        }

    }


}
