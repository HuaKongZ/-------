package com.sinoangel.kids.mode_new.ks.core;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CommentsBean;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommentsActivity extends BaseActivity implements View.OnClickListener {
    private CoreDataBean.DataBean dat;
    private List<CommentsBean.DataBean> lcb;
    private ListView lv;
    private ImageView iv_back, iv_commit;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:
                    lv.setAdapter(new BaseAdapter() {
                        @Override
                        public int getCount() {
                            return lcb == null ? 0 : lcb.size();
                        }

                        @Override
                        public Object getItem(int position) {
                            return lcb.get(position);
                        }

                        @Override
                        public long getItemId(int position) {
                            return position;
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = getLayoutInflater().inflate(R.layout.item_comments, null);
                            ImageView iv = (ImageView) view.findViewById(R.id.iv_img);
                            TextView tv_name = (TextView) view.findViewById(R.id.tv_name);
                            TextView tv_time = (TextView) view.findViewById(R.id.tv_time);
                            TextView tv_context = (TextView) view.findViewById(R.id.tv_context);
                            CommentsBean.DataBean cdb = lcb.get(position);
                            ImageUtils.showImgUrl(cdb.getUserIcon(), iv);
                            tv_name.setText(cdb.getUserName());
                            String time = cdb.getCreateTime().substring(0, cdb.getCreateTime().lastIndexOf(":"));
                            tv_time.setText(time);
                            tv_context.setText(cdb.getContent());
                            return view;
                        }
                    });
                    break;
            }
            DialogUtils.dismissProgressDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        dat = (CoreDataBean.DataBean) getIntent().getExtras().get("data");

        lv = (ListView) findViewById(R.id.lv_list);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_commit = (ImageView) findViewById(R.id.iv_commit);

        iv_back.setOnClickListener(this);
        iv_commit.setOnClickListener(this);
        getNetDada();
    }

    private void getNetDada() {
        DialogUtils.showProgressDialog(this, "");
        Map<String, String> mss = new HashMap<>();
        mss.put("singleId", dat.getAppId());
        mss.put("seriesId", StaticObj.getCurrCata().getAppId());
        mss.put("carrierId", StaticObj.getCategory_id());
        HttpUtil.getUtils().getJsonString(API.getNetUserCommentList(mss), new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                handler.sendEmptyMessage(1);
            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    CommentsBean cb = JSON.parseObject(json, CommentsBean.class);
                    if (cb.getFlag() == 1) {
                        lcb = cb.getData();
                        handler.sendEmptyMessage(0);
                    } else {
                        handler.sendEmptyMessage(1);
                    }
                } catch (Exception e) {
                    handler.sendEmptyMessage(1);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_commit:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                Intent intent = new Intent(CommentsActivity.this, CommitCommentActivity.class);
                intent.putExtra("data", dat);
                CommentsActivity.this.startActivityForResult(intent, 10);
                break;
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == 100)
//            getNetDada();
//    }


    @Override
    protected void onStart() {
        super.onStart();
        getNetDada();
    }
}
