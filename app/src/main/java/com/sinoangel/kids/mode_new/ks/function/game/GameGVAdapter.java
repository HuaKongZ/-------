package com.sinoangel.kids.mode_new.ks.function.game;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.PackageUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.io.File;
import java.util.List;

/**
 * Created by Administrator on 2016/7/6.
 */
public class GameGVAdapter extends RecyclerView.Adapter<GameGVAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {
    private GameActivity mContext;
    private List<CoreDataBean.DataBean> baseAppInfoList;

    public GameGVAdapter(GameActivity mContext, List<CoreDataBean.DataBean> baseAppInfoList) {
        super();
        this.mContext = mContext;
        this.baseAppInfoList = baseAppInfoList;

        //注册应用程序广播接收者
        AppinstallReceiver receiver = new AppinstallReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addDataScheme("package");
        mContext.registerReceiver(receiver, filter);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_app, null);
//        int hei = parent.getContext().getResources().getDisplayMetrics().heightPixels;
//        view.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, hei / 4));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // 设置tag，作为唯一标示
        CoreDataBean.DataBean data = baseAppInfoList.get(position);

        ImageUtils.showImgUrl(data.getIcon(), holder.imageapp);

        if (data.isExistDevice()) {//判断应用是否已经安装
            holder.imagebtn.setVisibility(View.INVISIBLE);
            holder.imagecover.setVisibility(View.INVISIBLE);

            try {
                CoreDataBean.DownData dd = data.getDd();
                dd.setDownState(DownManagerUtil.DOWNLOAD_FINISH);
                MyApplication.getInstance().getDbUtisl().saveOrUpdate(dd);
            } catch (DbException e) {
                e.printStackTrace();
            }

        } else {
            for (CoreDataBean.DataBean dat : StaticObj.ld) {
                if (data.getAppId().equals(dat.getAppId())) {
                    baseAppInfoList.set(position, dat);
                }
            }

            if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
                // 这是在下载当中
                holder.imagecover.setVisibility(View.VISIBLE);
                holder.imagecover.setProgress(data.getDd().getDownSize());
                holder.imagebtn.setImageResource(R.mipmap.animation_play_press);
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
                holder.imagebtn.setImageResource(R.mipmap.animation_wait);
                holder.imagecover.setProgress(0);
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE) {
                holder.imagebtn.setImageResource(R.mipmap.animation_play_puase);
                holder.imagecover.setProgress(0);
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FAIL) {
                holder.imagebtn.setImageResource(R.mipmap.animation_play_faild);
                holder.imagecover.setProgress(0);
            } else {
                holder.imagebtn.setImageResource(R.mipmap.app_download);
                holder.imagecover.setVisibility(View.VISIBLE);
                holder.imagecover.setProgress(0);
            }
            holder.imagebtn.setVisibility(View.VISIBLE);
        }

        holder.tv_title.setText(data.getAppName());

        if ("1".equals(data.getIsNew())) {
            if (SPUtils.getNew(data.getAppId())) {
                holder.iv_isnew.setVisibility(View.VISIBLE);
            } else {
                holder.iv_isnew.setVisibility(View.GONE);
            }
        } else {
            holder.iv_isnew.setVisibility(View.GONE);
        }

        //添加点击事件
        data.getDd().setPosation(position);
        holder.rl_box.setTag(data);
        holder.rl_box.setOnClickListener(this);
//        holder.rl_box.setOnLongClickListener(this);
    }

    @Override
    public int getItemCount() {
        return baseAppInfoList == null ? 0 : baseAppInfoList.size();
    }

    @Override
    public void onClick(View v) {
        final CoreDataBean.DataBean data = (CoreDataBean.DataBean) v.getTag();

        SPUtils.putNew(data.getAppId());
        MusicUtils.getMusicUtils().playSound(R.raw.connect_15);

        // 判断是否已经存在，存在打开，不存在则打开dialog
        if (data.isExistDevice()) {
            // 如果已经存在本地，则直接打开
            mContext.startActivitySafely(data.getIntent());
            HttpUtil.getUtils().statistics(data.getAppId(),StaticObj.getCategory_id(), "0", "1", "0", 0);
            return;
        } else if (data.isFileExist()) {
            int result = PackageUtils.install(mContext, data.getSavePath());
            if (result == PackageUtils.INSTALL_SUCCEEDED) {
                notifyDataSetChanged();
                if (mContext.appInfoDialog != null)
                    mContext.appInfoDialog.dismiss();
            }
        } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE || data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FAIL) {
            DownManagerUtil.getInstance().downFile(data);
            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_ING);
            try {
                MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState");
            } catch (DbException e) {
                e.printStackTrace();
            }
            notifyItemChanged(data.getDd().getPosation());
        } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
            DownManagerUtil.getInstance().stopDownload(data);
            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_PUASE);
            try {
                MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState");
            } catch (DbException e) {
                e.printStackTrace();
            }
            notifyItemChanged(data.getDd().getPosation());
        } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
            DownManagerUtil.getInstance().stopDownload(data);
            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_NO);
            try {
                MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState");
            } catch (DbException e) {
                e.printStackTrace();
            }
            notifyItemChanged(data.getDd().getPosation());
        } else {
            mContext.mData = data;
            mContext.ShowInAppDialog();
        }

    }

    @Override
    public boolean onLongClick(View view) {
        final CoreDataBean.DataBean data = (CoreDataBean.DataBean) view.getTag();
        final File file = new File(DownManagerUtil.getInstance().savePath + data.getAppId());
        if (data.isExistDevice()) {
            AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
            adb.setTitle(mContext.getString(R.string.title_delete));
            adb.setNegativeButton(mContext.getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (DownManagerUtil.deleteAllFile(file)) {
//                        AppUtils.showToast(mContext.getString(R.string.del_ok));
                        MusicUtils.getMusicUtils().playSound(R.raw.removing_creatures_8);
                        try {
                            MyApplication.getInstance().getDbUtisl().delete(data.getDd());
                            MyApplication.getInstance().getDbUtisl().delete(data);


                            //注册应用程序广播接收者
//                            AppUninstallReceiver receiver = new AppUninstallReceiver();
//                            IntentFilter filter = new IntentFilter();
//                            filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
////                            filter.addDataScheme("package");
//                            mContext.registerReceiver(receiver, filter);

                            AppUtils.uninstallApplication(mContext, data.getPackageName());

                            DownManagerUtil.deleteAllFile(new File(data.getSavePath()));

                        } catch (DbException e) {
                            e.printStackTrace();
                        }
                        notifyDataSetChanged();
                    } else {
                        AppUtils.showToast(mContext.getString(R.string.del_no));
                    }


                }
            });
            adb.setPositiveButton(mContext.getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            adb.show();

        }
        return true;
    }


    private class AppinstallReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_PACKAGE_ADDED.equals(intent.getAction())) {
                notifyDataSetChanged();
            }
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageapp, iv_isnew;// 展示的appaIcon
        private ProgressBar imagecover;// 黑色遮挡物
        private ImageView imagebtn;// 下载按钮
        private RelativeLayout rl_box;
        private TextView tv_title;

        public ViewHolder(View convertView) {
            super(convertView);
            rl_box = (RelativeLayout) convertView.findViewById(R.id.rl_box);
            imageapp = (ImageView) convertView.findViewById(R.id.item_painting_image);
            imagecover = (ProgressBar) convertView.findViewById(R.id.item_painting_cover);
            imagebtn = (ImageView) convertView.findViewById(R.id.item_painting_btn);
            iv_isnew = (ImageView) convertView.findViewById(R.id.iv_isnew);
            tv_title = (TextView) convertView.findViewById(R.id.tv_title);
        }
    }
}
