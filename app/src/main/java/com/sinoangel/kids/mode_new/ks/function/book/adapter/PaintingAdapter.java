package com.sinoangel.kids.mode_new.ks.function.book.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;

import java.util.List;

/**
 */
public class PaintingAdapter extends RecyclerView.Adapter<PaintingAdapter.ViewHolder> {

    private List<CoreDataBean.DataBean> baseAppInfoList;

    public void setData(List<CoreDataBean.DataBean> baseAppInfoList) {
        this.baseAppInfoList = baseAppInfoList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = new View(viewGroup.getContext());
        int hei = viewGroup.getContext().getResources().getDisplayMetrics().heightPixels;
        view.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, hei / 4));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

    }

    @Override
    public int getItemCount() {

        if (baseAppInfoList == null || baseAppInfoList.size() <= 16) {
            return 4;
        }
        if (baseAppInfoList != null && baseAppInfoList.size() % 4 == 0) {
            return baseAppInfoList.size() / 4;
        } else {
            return baseAppInfoList.size() / 4 + 1;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setBackgroundResource(R.mipmap.list);
        }
    }
}

