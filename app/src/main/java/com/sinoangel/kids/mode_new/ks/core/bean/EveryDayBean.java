package com.sinoangel.kids.mode_new.ks.core.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/8/9 0009.
 */
public class EveryDayBean {


    /**
     * flag : 1
     * error : []
     * data : {"position":"1","title":"The Smurfs","keyword":"smurfs","data":[{"icon":"http://cn.img.store.sinoangel.cn/icons/lanjingling1.png","resourceType":4,"resource":"","appid":"54","category_id":"3","size":"2","albumAges":"4-12","lang":"1"},{"icon":"http://cn.img.store.sinoangel.cn/icons/lanjingling2.png","resourceType":4,"resource":"","appid":"54","category_id":"3","size":"1","albumAges":"4-12","lang":"1"},{"icon":"http://cn.img.store.sinoangel.cn/icons/lanjingling3.png","resourceType":4,"resource":"","appid":"54","category_id":"3","size":"1","albumAges":"4-12","lang":"1"}]}
     */

    private int flag;
    /**
     * position : 1
     * title : The Smurfs
     * keyword : smurfs
     * data : [{"icon":"http://cn.img.store.sinoangel.cn/icons/lanjingling1.png","resourceType":4,"resource":"","appid":"54","category_id":"3","size":"2","albumAges":"4-12","lang":"1"},{"icon":"http://cn.img.store.sinoangel.cn/icons/lanjingling2.png","resourceType":4,"resource":"","appid":"54","category_id":"3","size":"1","albumAges":"4-12","lang":"1"},{"icon":"http://cn.img.store.sinoangel.cn/icons/lanjingling3.png","resourceType":4,"resource":"","appid":"54","category_id":"3","size":"1","albumAges":"4-12","lang":"1"}]
     */

    private DataBean data;
    private List<?> error;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public List<?> getError() {
        return error;
    }

    public void setError(List<?> error) {
        this.error = error;
    }

    public static class DataBean {
        private String position;
        private String title;
        private String keyword;
        /**
         * icon : http://cn.img.store.sinoangel.cn/icons/lanjingling1.png
         * resourceType : 4
         * resource :
         * appid : 54
         * category_id : 3
         * size : 2
         * albumAges : 4-12
         * lang : 1
         */

        private List<DataCardBean> data;

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public List<DataCardBean> getData() {
            return data;
        }

        public void setData(List<DataCardBean> data) {
            this.data = data;
        }

        public static class DataCardBean {
            private String icon;
            private String resourceType;
            private String resource;
            private String appid;
            private String category_id;
            private String size;
            private String albumAges;
            private String lang;

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public String getResourceType() {
                return resourceType;
            }

            public void setResourceType(String resourceType) {
                this.resourceType = resourceType;
            }

            public String getResource() {
                return resource;
            }

            public void setResource(String resource) {
                this.resource = resource;
            }

            public String getAppid() {
                return appid;
            }

            public void setAppid(String appid) {
                this.appid = appid;
            }

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getSize() {
                return size;
            }

            public void setSize(String size) {
                this.size = size;
            }

            public String getAlbumAges() {
                return albumAges;
            }

            public void setAlbumAges(String albumAges) {
                this.albumAges = albumAges;
            }

            public String getLang() {
                return lang;
            }

            public void setLang(String lang) {
                this.lang = lang;
            }
        }
    }
}
