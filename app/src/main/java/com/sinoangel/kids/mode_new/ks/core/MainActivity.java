package com.sinoangel.kids.mode_new.ks.core;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.EveryDayBean;
import com.sinoangel.kids.mode_new.ks.core.bean.VisionBean;
import com.sinoangel.kids.mode_new.ks.function.cartoon.WEBViewActivity;
import com.sinoangel.kids.mode_new.ks.set.GuideActivity;
import com.sinoangel.kids.mode_new.ks.set.H5StoreActivity;
import com.sinoangel.kids.mode_new.ks.set.LoginActivity;
import com.sinoangel.kids.mode_new.ks.set.SetActivity;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.sinoangel.kids.mode_new.ks.widget.BelowDialog;
import com.sinoangel.kids.mode_new.ks.widget.YKPlayerActivity;
import com.umeng.analytics.MobclickAgent;
import com.youku.player.YoukuPlayerBaseConfiguration;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends BaseActivity implements OnClickListener {

    // 变量说明：一起玩，一起听，一起玩，讲故事,生日选择
    private ImageView iv_see, iv_listen, iv_play, iv_say, main_about, iv_store, iv_bulr;
    private VisionBean vb;
    private static final int[] everyLayoutId = {R.layout.item_everyday1};
    private static final int[] ivId = {R.id.iv1, R.id.iv2, R.id.iv3};
    private static final int[] iviconId = {R.id.iv_icon1, R.id.iv_icon2, R.id.iv_icon3};
    private long ltime;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            PackageManager pm = getPackageManager();
            PackageInfo pi = null;
            try {
                pi = pm.getPackageInfo(getPackageName(), PackageManager.GET_CONFIGURATIONS);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            int code = pi.versionCode;
            if (code < vb.getVisionCode()) {
                AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                adb.setNegativeButton(getString(R.string.update), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        update();
                    }
                });
                adb.setMessage(vb.getUpdateContent() + "\n\n" + getString(R.string.version) + pi.versionName);
                if (vb.getIsUpdate()) {
                    adb.setPositiveButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                }
                adb.setTitle(getString(R.string.newversion) + vb.getVisionName());
                adb.show();
            }
        }
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int ddpi = getResources().getDisplayMetrics().densityDpi;
        float dpi = getResources().getDisplayMetrics().density;
        float dim = getResources().getDimension(R.dimen.sw);
        AppUtils.outputLog(MyApplication.getInstance().getWei() + "*" + MyApplication.getInstance().getHei() + "ddpi:" + ddpi + "dpi:" + dpi + "dim:" + dim / dpi);

        init();

        try {
            //获取手机唯一标识
            TelephonyManager TelephonyMgr = (TelephonyManager) MyApplication.getInstance().getSystemService(MyApplication.getInstance().TELEPHONY_SERVICE);
            SPUtils.putPhoneId(TelephonyMgr.getDeviceId());
        } catch (Exception e) {

        }

        checkUpdazte();

        if (SPUtils.getMusicFlage()) {
            SPUtils.putMusicFlage(true);

            MusicUtils.getMusicUtils().nOFBGMusicService(true);
            MusicUtils.getMusicUtils().nOFSoundService(true);
        }

        if (StaticObj.getUid() != null) {
            String url = API.getUpdateuserLang();
            HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
                @Override
                public void onNetFail() {

                }

                @Override
                public void onNetSucceed(String json) {
                    int i = 0;
                }
            });
        }

        if (SPUtils.getIsExists(Constant.NEWUSE_NY1)) {
            Intent intent = new Intent(MainActivity.this, GuideActivity.class);
            intent.putExtra("index", 0);
            intent.putExtra("flage", Constant.NEWUSE_NY1);
            startActivity(intent);
        }


        screenLinerter();

//        openEverdayAdv();
//        DialogUtils.showShareDialog(this);

        ltime = new Date().getTime();

//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.sinoangel.kids.mode_new.ks",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                AppUtils.outputLog( Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

    }


    private void openEverdayAdv() {

        if (SPUtils.getIsExists(Constant.NEWUSE_NY1)) {
            return;
        }

        HttpUtil.getUtils().getJsonString(API.getEverdayShare(), new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                int i = 0;
            }

            @Override
            public void onNetSucceed(String json) {
                EveryDayBean edb = null;
                try {
                    edb = JSON.parseObject(json, EveryDayBean.class);
                } catch (Exception e) {
                    return;
                }
                final EveryDayBean.DataBean db = edb.getData();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final BelowDialog everyDayDialog = new BelowDialog(MainActivity.this);
                        everyDayDialog.setContentView(R.layout.dialog_everyday);
                        TextView tv = (TextView) everyDayDialog.findViewById(R.id.tv_title);
                        ImageView iv_booking = (ImageView) everyDayDialog.findViewById(R.id.iv_booking);

                        iv_booking.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                                Intent userIntent = null;
                                userIntent = new Intent(MainActivity.this, H5StoreActivity.class);
                                userIntent.putExtra(Constant.WEB_URL, API.getDAYRECOMMEND(db.getKeyword()));
                                startActivity(userIntent);
                                HttpUtil.getUtils().statistics("", "1029", "0", "0", "0", 0);
                            }
                        });
                        RelativeLayout rl_box = (RelativeLayout) everyDayDialog.findViewById(R.id.rl_box);
                        View v = getLayoutInflater().inflate(everyLayoutId[0], null);
                        int i = 0;
                        for (final EveryDayBean.DataBean.DataCardBean dcb : db.getData()) {
                            ImageView iv = (ImageView) v.findViewById(ivId[i]);
                            ImageView iv_icon = (ImageView) v.findViewById(iviconId[i]);
                            i++;
                            switch (dcb.getCategory_id()) {
                                case Constant.CATEGORY_CARTOON:
                                    iv_icon.setImageResource(R.mipmap.ev_icon_carton);
                                    break;
                                case Constant.CATEGORY_INITIATE:
                                    iv_icon.setImageResource(R.mipmap.ev_icon_game);
                                    break;
                                case Constant.CATEGORY_PICTURE:
                                    iv_icon.setImageResource(R.mipmap.ev_icon_book);
                                    break;
                                case Constant.CATEGORY_SONG:
                                    iv_icon.setImageResource(R.mipmap.ev_icon_song);
                                    break;
                            }

                            ImageUtils.showImgUrl(dcb.getIcon(), iv);

                            iv.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    MusicUtils.getMusicUtils().playSound(R.raw.connect_15);
                                    Intent i = null;
                                    switch (dcb.getCategory_id()) {
                                        case Constant.CATEGORY_CARTOON:
                                            switch (dcb.getResourceType()) {
                                                case "2":
                                                    i = new Intent(MainActivity.this, YKPlayerActivity.class);
                                                    i.putExtra("vid", dcb.getResource());
                                                    i.putExtra("appid", dcb.getAppid());
                                                    StaticObj.YKAgle = Constant.YK_FLAGE_BOOKING;
                                                    break;
                                                default:
                                                    i = new Intent(MainActivity.this, WEBViewActivity.class);
                                                    i.putExtra("url", dcb.getResource());

                                                    break;
                                            }
                                            break;
                                        case Constant.CATEGORY_SONG:
                                            i = new Intent(MainActivity.this, H5StoreActivity.class);
                                            i.putExtra(Constant.WEB_URL, API.NET_STOREDEILPAGE_MUSIC + dcb.getAppid());
                                            break;
                                        case Constant.CATEGORY_PICTURE:
                                            i = new Intent(MainActivity.this, H5StoreActivity.class);
                                            i.putExtra(Constant.WEB_URL, API.NET_STOREDEILPAGE_PIC + dcb.getAppid());
                                            break;
                                        default:
                                            i = new Intent(MainActivity.this, H5StoreActivity.class);
                                            i.putExtra(Constant.WEB_URL, API.NET_STOREDEILPAGE_GAME + dcb.getAppid());
                                            break;
                                    }
                                    startActivity(i);
                                    HttpUtil.getUtils().statistics("", "1030", "0", "0", "0", 0);
                                }
                            });
                        }
                        rl_box.addView(v);
                        tv.setText(db.getTitle());
                        tv.setSelected(true);
                        everyDayDialog.show();
                        SPUtils.putEveryFlage(new Date().getTime());
                    }
                });
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        MusicUtils.getMusicUtils().playOrPauseBgsc(true);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH");
        String[] after = sdf.format(SPUtils.getEveryFlage()).split("-");
        String[] now = sdf.format(new Date().getTime()).split("-");

        if (Integer.parseInt(now[0]) > Integer.parseInt(after[0])) {
            if (Integer.parseInt(now[3]) >= 5) {
                openEverdayAdv();
            }
        } else if (Integer.parseInt(now[0]) == Integer.parseInt(after[0])) {
            if (Integer.parseInt(now[1]) > Integer.parseInt(after[1])) {
                if (Integer.parseInt(now[3]) >= 5) {
                    openEverdayAdv();
                }
            } else if (Integer.parseInt(now[1]) == Integer.parseInt(after[1])) {
                if (Integer.parseInt(now[2]) > Integer.parseInt(after[2])) {
                    if (Integer.parseInt(now[3]) >= 5) {
                        openEverdayAdv();
                    }
                }
            }
        }

        if (!isAppOnForeground())
            HttpUtil.getUtils().statistics("", "1000", "0", "1", "0", 0);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpUtil.getUtils().statistics("", "1000", "0", "0", "0", new Date().getTime() - ltime);
        System.exit(0);    //系统离开
    }

    /**
     * 方法说明：初始化所有按钮
     */
    private void init() {
        // 初始化控件

        iv_see = (ImageView) findViewById(R.id.iv_see);
        iv_listen = (ImageView) findViewById(R.id.iv_listen);
        iv_play = (ImageView) findViewById(R.id.iv_play);
        iv_say = (ImageView) findViewById(R.id.iv_say);
        main_about = (ImageView) findViewById(R.id.main_about);
        iv_store = (ImageView) findViewById(R.id.iv_store);
//        iv_bulr = (ImageView) findViewById(R.id.iv_bulr);
        // 监听事件
        iv_see.setOnClickListener(this);
        iv_listen.setOnClickListener(this);
        iv_play.setOnClickListener(this);
        iv_say.setOnClickListener(this);
        main_about.setOnClickListener(this);
        iv_store.setOnClickListener(this);
    }

    /**
     * 方法说明：所有按钮的点击事件
     */
    public void onClick(View V) {
        Intent intent = new Intent(this, ChildActivity.class);

        switch (V.getId()) {
            // 点击一起玩
            case R.id.iv_play:
                MusicUtils.getMusicUtils().playSound(R.raw.objective_score);
                StaticObj.setCategory_id(Constant.CATEGORY_INITIATE);
                startActivity(intent);
                HttpUtil.getUtils().statistics("", "1002", "1", "0", "0", 0);
                break;
            // 点击讲故事
            case R.id.iv_say:
                MusicUtils.getMusicUtils().playSound(R.raw.objective_digging);
                StaticObj.setCategory_id(Constant.CATEGORY_PICTURE);
                startActivity(intent);
                HttpUtil.getUtils().statistics("", "1005", "1", "0", "0", 0);
                break;
            // 点击一起看
            case R.id.iv_see:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_refill1);
                StaticObj.setCategory_id(Constant.CATEGORY_CARTOON);
                startActivity(intent);
                HttpUtil.getUtils().statistics("", "1003", "1", "0", "0", 0);
                break;
            // 点击一起听
            case R.id.iv_listen:
                MusicUtils.getMusicUtils().playSound(R.raw.objective_item);
                StaticObj.setCategory_id(Constant.CATEGORY_SONG);
                startActivity(intent);
                HttpUtil.getUtils().statistics("", "1004", "1", "0", "0", 0);
                break;
            //点击设置
            case R.id.main_about:
                new DialogUtils().showULDialog(MainActivity.this, new DialogUtils.UnLockFinish() {
                    @Override
                    public void onULFinish() {
                        Intent set = new Intent(MainActivity.this, SetActivity.class);
                        startActivity(set);
                    }
                });

                HttpUtil.getUtils().statistics("", "1019", "1", "0", "0", 0);
                break;
            case R.id.iv_store:
                MusicUtils.getMusicUtils().playSound(R.raw.objective_jelly);
                Intent userIntent = null;
                if (StaticObj.getUid() != null) {
                    userIntent = new Intent(this, H5StoreActivity.class);
                } else {
                    userIntent = new Intent(this, LoginActivity.class);
                }
                startActivityForResult(userIntent, 0);
                HttpUtil.getUtils().statistics("", "1001", "1", "0", "0", 0);
                break;
        }

    }


    /**
     * 下载器
     */
    private void checkUpdazte() {
        HttpUtil.getUtils().getJsonString("http://cn.api.sinoangel.cn/general/visionupdate", new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {

            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    vb = JSON.parseObject(json, VisionBean.class);
                    handler.sendEmptyMessage(0);
                } catch (Exception e) {

                }
            }
        });
    }

    //下载
    private void update() {
        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

        Uri uri = Uri.parse("http://cn.api.sinoangel.cn/general/QR_Code");
        DownloadManager.Request request = new DownloadManager.Request(uri);

        //设置允许使用的网络类型，这里是移动网络和wifi都可以
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);

        //禁止发出通知，既后台下载，如果要使用这一句必须声明一个权限：android.permission.DOWNLOAD_WITHOUT_NOTIFICATION
        //request.setShowRunningNotification(false);

        //显示下载界面
        request.setVisibleInDownloadsUi(false);
        downloadManager.enqueue(request);
        // AppUtil.showToast(mContext, id + "");
        //把id保存好，在接收者里面要用，最好保存在Preferences里面
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /**
     * 应用退出时调用此方法
     */

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        YoukuPlayerBaseConfiguration.exit();        //退出应用时请调用此方法
        super.onBackPressed();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constant.LOGIN_SUSSCE:
                Intent userIntent = new Intent(this, H5StoreActivity.class);
                startActivity(userIntent);
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

//            Dialog dialog = new Dialog(this);
//            ImageView iv = new ImageView(this);
//            iv.setImageBitmap(ImageUtils.fastblur(this,myShot(this),20));
//            dialog.setContentView(iv);
//            dialog.show();

            new DialogUtils().showULDialog(this, new DialogUtils.UnLockFinish() {
                @Override
                public void onULFinish() {
                    Intent i = new Intent(Intent.ACTION_MAIN);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addCategory(Intent.CATEGORY_HOME);
                    startActivity(i);
                }
            });

        }
        return super.onKeyDown(keyCode, event);
    }

    private void screenLinerter() {
        final IntentFilter filter = new IntentFilter();
        // 屏幕灭屏广播
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        // 屏幕亮屏广播
        filter.addAction(Intent.ACTION_SCREEN_ON);
        // 屏幕解锁广播
        filter.addAction(Intent.ACTION_USER_PRESENT);
        // 当长按电源键弹出“关机”对话或者锁屏时系统会发出这个广播
        // example：有时候会用到系统对话框，权限可能很高，会覆盖在锁屏界面或者“关机”对话框之上，
        // 所以监听这个广播，当收到时就隐藏自己的对话，如点击pad右下角部分弹出的对话框
        filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);

        BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                String action = intent.getAction();

                if (Intent.ACTION_SCREEN_ON.equals(action)) {
//                    MyApplication.getInstance().playOrPauseBgsc(true);
                } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
                    MusicUtils.getMusicUtils().playOrPauseBgsc(false);
                    MyApplication.getInstance().stopLockScreen();
                } else if (Intent.ACTION_USER_PRESENT.equals(action)) {
                } else if (Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(intent.getAction())) {
                }
            }
        };
        registerReceiver(mBatInfoReceiver, filter);
    }

//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//        if (hasFocus) {
//            if (iv_bulr.getVisibility() == View.GONE) {
//                Bitmap bitmap = ImageUtils.getBulrBit(getWindow(), 20);
//                iv_bulr.setImageBitmap(bitmap);
//            }
//            iv_bulr.setVisibility(View.INVISIBLE);
//        } else {
//            iv_bulr.setVisibility(View.VISIBLE);
//        }
//    }
}
