package com.sinoangel.kids.mode_new.ks.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/8/24 0024.
 */
public class AppUtils {

    public static void showToast(String word) {
        Toast toast = Toast.makeText(MyApplication.getInstance(), word, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * 验证邮箱地址
     *
     * @param email 邮箱地址
     */
    public static boolean emailFormat(String email) {
        boolean tag = true;
        final String pattern1 = "^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$";
        //      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        final Pattern pattern = Pattern.compile(pattern1);
        final Matcher mat = pattern.matcher(email);
        if (!mat.find()) {
            tag = false;
        }
        return tag;
    }
//


    /**
     * 验证邮箱地址
     *
     * @param name 用户名
     */
    public static boolean userFormat(String name) {
        final String pattern1 = "^[a-zA-Z0-9_\\u4E00-\\u9FA5]{1,15}$";
        final Pattern pattern = Pattern.compile(pattern1);
        final Matcher mat = pattern.matcher(name);
        return mat.find();
    }

    /**
     * 卸载应用程序
     */
    public static void uninstallApplication(Context context, String packageName) {

        //隐式意图，设置动作、Category、数据
        Intent intent = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + packageName));
//        intent.setAction("android.intent.action.DELETE");
//        intent.addCategory("android.intent.category.DEFAULT");
//        //获取被点击对象的包名，并设置为意图的数据
//        intent.setData(Uri.parse("package:" + packageName));
        context.startActivity(intent);

    }

    public static void outputLog(String str) {
        if (str != null)
            Log.e("dd", str);
    }

}
