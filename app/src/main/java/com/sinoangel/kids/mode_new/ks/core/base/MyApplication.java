package com.sinoangel.kids.mode_new.ks.core.base;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.core.service.BGMusicService;
import com.sinoangel.kids.mode_new.ks.core.service.SoundService;
import com.sinoangel.kids.mode_new.ks.set.LockScreenActivity;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.squareup.picasso.Picasso;
import com.tencent.bugly.crashreport.CrashReport;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.youku.player.YoukuPlayerBaseConfiguration;

import java.io.File;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import cn.jpush.android.api.JPushInterface;

/**
 * 类说明：全局的appliction
 */
public class MyApplication extends Application {
    public static MyApplication instance;
    private DbUtils dbUtisl;

    private int hei, wei;
    public static YoukuPlayerBaseConfiguration configuration;


    //怪物课堂
    {
        //微信
        PlatformConfig.setWeixin("wxed6ff902b782a48e", "5b0278c86f101e6153c34a5c5ea14169");
        //新浪微博
        PlatformConfig.setSinaWeibo("1959369416", "154dee4d947e185c553014191ee8565a");
        //QQ
        PlatformConfig.setQQZone("1105605792", "69yKHib4yRLSLXCk");
        //Twitter
//        PlatformConfig.setTwitter("3aIN7fuF685MuZ7jtXkQxalyi", "MK6FEYG63eWcpDFgRYw4w9puJhzDl0tyuqWjZ3M7XJuuG7mMbO");
    }

    public int getHei() {
        return hei > wei ? hei : wei;
    }

    public int getWei() {
        return wei < hei ? wei : hei;
    }

    public void onCreate() {
        super.onCreate();
        instance = this;
        //优酷配置
        configuration = new YoukuPlayerBaseConfiguration(this) {


            /**
             * 通过覆写该方法，返回“正在缓存视频信息的界面”，
             * 则在状态栏点击下载信息时可以自动跳转到所设定的界面.
             * 用户需要定义自己的缓存界面
             */
            @Override
            public Class<? extends Activity> getCachingActivityClass() {
                // TODO Auto-generated method stub
                return BaseActivity.class;
            }

            /**
             * 通过覆写该方法，返回“已经缓存视频信息的界面”，
             * 则在状态栏点击下载信息时可以自动跳转到所设定的界面.
             * 用户需要定义自己的已缓存界面
             */

            @Override
            public Class<? extends Activity> getCachedActivityClass() {
                // TODO Auto-generated method stub
                return BaseActivity.class;
            }

            /**
             * 配置视频的缓存路径，格式举例： /appname/videocache/
             * 如果返回空，则视频默认缓存路径为： /应用程序包名/videocache/
             *
             */
            @Override
            public String configDownloadPath() {
                // TODO Auto-generated method stub
                return null;
            }
        };


        //注册腾讯bugly  怪物 900049878  怪物正式 900053592
        CrashReport.initCrashReport(getApplicationContext(), "900053592", false);

        //极光推送
        JPushInterface.init(this);

        //友盟统计
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);

        //友盟分享
        UMShareAPI.get(this);

        dbUtisl = DbUtils.create(this, "SINOANGEL_DATA");

        hei = getResources().getDisplayMetrics().heightPixels;
        wei = getDpi();

        MusicUtils.initMusic(this);

//        LeakCanary.install(this);

        //打开横竖屏开关
        //得到是否开启
//        int flag = Settings.System.getInt(getContentResolver(),
//                Settings.System.ACCELEROMETER_ROTATION, 0);
//        if (0 == flag) {
//            Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 1);
//        }
    }

    public DbUtils getDbUtisl() {
        return dbUtisl;
    }

    /**
     * 获取唯一的MarsApplication实例
     **/
    public static MyApplication getInstance() {
        return instance;
    }

    /**
     * 判断系统的语言
     */
    static public int systemLanguageType() {
        Locale l = Locale.getDefault();
        String language = l.getLanguage();
        String country = getCountry();
        if (TextUtils.isEmpty(language)) {
            return Constant.LANGUAGE_EN;
        } else if (language.trim().contains("en")) {//英文
            return Constant.LANGUAGE_EN;
        } else if (language.trim().contains("es")) {
            return Constant.LANGUAGE_ES;
        } else if (language.trim().contains("zh")) {//中文简体、繁体
            if (country.trim().contains("HK")) {
                //没有繁体数据，暂时用简体数据
                return Constant.LANGUAGE_ZH;
            } else {
                return Constant.LANGUAGE_ZH;
            }
        } else if (language.trim().contains("ar")) {
            return Constant.LANGUAGE_AR;
        } else if (language.trim().contains("ru")) {
            return Constant.LANGUAGE_RU;
        }
        return Constant.LANGUAGE_EN;
    }

    /**
     * HK：香港  CN 中国大陆  US 美国
     *
     * @return
     */
    public static String getCountry() {
        Locale l = Locale.getDefault();
        String country = l.getCountry();
        return country;
    }

    /**
     * 方法说明：获取渠道key
     *
     * @return
     */
    public static String getAppKey() {
        ApplicationInfo info;
        try {
            info = instance.getPackageManager().getApplicationInfo(
                    instance.getPackageName(), PackageManager.GET_META_DATA);
            String appKey = info.metaData.getString("UMENG_APPKEY");
            return appKey;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "123456";
    }

    /**
     * 方法说明：判断下载的apk的完整性
     *
     * @param apkPath
     * @return
     */
    public static boolean isFull(String apkPath) {
        if (apkPath == null) {
            return false;
        }
        PackageManager pm = getInstance().getPackageManager();
        PackageInfo pakinfo = pm.getPackageArchiveInfo(apkPath,
                PackageManager.GET_ACTIVITIES);
        if (pakinfo == null) {
            return false;
        }
        String versionName = null;
        String appName = null;
        String pakName = null;
        if (pakinfo != null) {
            ApplicationInfo appinfo = pakinfo.applicationInfo;
            versionName = pakinfo.versionName;
            appName = (String) pm.getApplicationLabel(appinfo);
            pakName = appinfo.packageName;

        }
        if (TextUtils.isEmpty(versionName) || TextUtils.isEmpty(appName)
                || TextUtils.isEmpty(pakName)) {
            return false;
        }
        return true;
    }




    public void startLockScreen() {
        long time = SPUtils.getTime();
        lockSrceenHandler.removeMessages(0);
        if (time != 0) {
            lockSrceenHandler.sendEmptyMessageDelayed(0, time * 60 * 1000);
            StaticObj.isCountDown = true;
        }
    }

    public void stopLockScreen() {
        lockSrceenHandler.removeMessages(0);
        StaticObj.isCountDown = false;
    }

    private Handler lockSrceenHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            startActivity(new Intent(MyApplication.getInstance(), LockScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    };


    public static int DayTime() {
        int age = SPUtils.getAge();
        switch (age) {
            case 1:
                return 1;
            case 2:
                return 6;
            case 3:
                return 12;
            default:
                return 15;
        }
    }

    /**
     * 异步缓存
     */
    public void saveItem(final List<CoreDataBean.DataBean> lcdb) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dbUtisl.saveOrUpdateAll(lcdb);
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void saveCardData(final List<Cate.DataBean> lcdb) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dbUtisl.saveOrUpdateAll(lcdb);
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void updateDD() {
        try {
            List<CoreDataBean.DownData> ldd = dbUtisl.findAll(CoreDataBean.DownData.class);
            dbUtisl.dropTable(CoreDataBean.DataBean.class);
            dbUtisl.dropTable(CoreDataBean.DownData.class);
            dbUtisl.saveOrUpdateAll(ldd);
        } catch (DbException e) {
            e.printStackTrace();
        }

    }

    //获取屏幕原始尺寸高度，包括虚拟功能键高度
    public int getDpi() {
        int dpi = 0;
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        @SuppressWarnings("rawtypes")
        Class c;
        try {
            c = Class.forName("android.view.Display");
            @SuppressWarnings("unchecked")
            Method method = c.getMethod("getRealMetrics", DisplayMetrics.class);
            method.invoke(display, displayMetrics);
            dpi = displayMetrics.widthPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dpi;
    }


}
