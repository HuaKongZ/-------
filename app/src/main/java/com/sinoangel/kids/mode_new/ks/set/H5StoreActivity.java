package com.sinoangel.kids.mode_new.ks.set;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.ChildActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class H5StoreActivity extends BaseActivity {
    private ImageView iv_back;
    private WebView wv;
    private String urlAdd;
    private int index = -1;//记录页面跳转次数

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h5_store);

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                if (wv.canGoBack()) {
                    wv.goBackOrForward(-1 * index);
                } else
                    finish();
                index = 0;
            }
        });


        wv = (WebView) findViewById(R.id.wv_store);

        WebSettings set = wv.getSettings();
        set.setJavaScriptEnabled(true);// 设置支持js 允许JavaScript执行
        set.setDefaultTextEncodingName("UTF-8");// 设置字符编码
        set.setCacheMode(WebSettings.LOAD_NO_CACHE);
        set.setJavaScriptEnabled(true);
        set.setBuiltInZoomControls(true);
        set.setDisplayZoomControls(false);
        set.setUseWideViewPort(true);
        wv.setInitialScale(1);
        set.setLoadWithOverviewMode(true);

        String url = getIntent().getStringExtra(Constant.WEB_URL);
        if (url == null) {
            wv.loadUrl(API.comeStore(StaticObj.getUid().getId()));
        }
        AppUtils.outputLog(url);
        wv.loadUrl(url);
        urlAdd = url;
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null) {
                    AppUtils.outputLog(url);
                    index++;
                    if (url.indexOf("kidos://") >= 0) {
                        String[] arr = url.split("/");

                        switch (arr[2]) {
                            case "video":
                                StaticObj.setCategory_id(Constant.CATEGORY_CARTOON);
                                break;
                            case "audio":
                                StaticObj.setCategory_id(Constant.CATEGORY_SONG);
                                break;
                            case "picbook":
                                StaticObj.setCategory_id(Constant.CATEGORY_PICTURE);
                                break;
                            case "app":
                                StaticObj.setCategory_id(Constant.CATEGORY_INITIATE);
                                HttpUtil.getUtils().getJsonString(API.getAppDownPath(arr[4]), new HttpUtil.OnNetResponseListener() {
                                    @Override
                                    public void onNetFail() {
                                    }

                                    @Override
                                    public void onNetSucceed(String json) {
                                        try {
                                            CoreDataBean db = JSON.parseObject(json, CoreDataBean.class);
                                            if (!db.getData().get(0).isExistDevice()) {
                                                DownManagerUtil.getInstance().downFile(db.getData().get(0));
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                });
                                break;
                        }
                        removeBlacklist(arr[4]);
                        Intent intent = new Intent(H5StoreActivity.this, ChildActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (url.indexOf("slogin://") >= 0) {
                        if (StaticObj.getUid() == null) {
                            AppUtils.outputLog("urlAdd:" + urlAdd);
                            startActivityForResult(new Intent(H5StoreActivity.this, LoginActivity.class), 0);
                        } else {
                            Map<String, String> mss = new HashMap<>();
                            mss.put("access_token", StaticObj.getUid().getId());
                            view.loadUrl(API.getURL(urlAdd, mss));

                        }
                    } else {
                        view.loadUrl(url);
                        AppUtils.outputLog("urlAdd:" + urlAdd);
                        urlAdd = url;
                    }
                }

                return true;// 返回true表示停留在本WebView(不跳转到系统的浏览器)
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constant.LOGIN_SUSSCE:

                if (urlAdd == null)
                    return;

                if (urlAdd.indexOf("?") > -1)
                    urlAdd = urlAdd.substring(0, urlAdd.indexOf("?"));

                Map<String, String> mss = new HashMap<>();
                mss.put("access_token", StaticObj.getUid().getId());
                String url = API.getURL(urlAdd, mss);
                wv.loadUrl(url);
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        clearCacheFolder(getFilesDir());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wv.destroy();
        System.exit(0);
    }

    private int clearCacheFolder(File dir) {
        int deletedFiles = 0;
        if (dir != null && dir.isDirectory()) {
            try {
                for (File child : dir.listFiles()) {
                    if (child.isDirectory()) {
                        deletedFiles += clearCacheFolder(child);
                    }
                    if (child.delete()) {
                        deletedFiles++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return deletedFiles;
    }

    private void removeBlacklist(String id) {

        String url = API.getRemoveBlacklist(id);
        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {

            }

            @Override
            public void onNetSucceed(String json) {
                int i = 0;
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        wv.pauseTimers();
    }

    @Override
    protected void onPostResume() {
        wv.resumeTimers();
        super.onPostResume();
    }
}
