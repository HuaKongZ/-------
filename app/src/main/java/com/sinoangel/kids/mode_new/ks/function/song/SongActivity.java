package com.sinoangel.kids.mode_new.ks.function.song;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.squareup.okhttp.Call;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * 儿歌馆
 *
 * @author XRB
 */
public class SongActivity extends BaseActivity
        implements OnClickListener {

    private RecyclerView rv_list;
    // 歌曲的适配器
    private SongRVAdapter sRVAdapter;
    private SeekBar sb_player;
    private MediaPlayer player;// 音乐播放器
    //    private RadioGroup rg_btn;//播放 暂停
    private ImageView child_more_back, iv_info;// 返回
    private TextView tv_time;//播放时间
    private List<CoreDataBean.DataBean> songInfosList;
    //    PowerManager pm;
//    PowerManager.WakeLock mWakeLock;
    //动画相关
    private GifImageView iv_anmi;
    private TextView tv_title;
    //标记用户当前是否正在控制seekbar
    private boolean isTSB;
    //记录用户调整的新时间
    private int newTime;
    private long userTime;//使用时长
    private GifImageView gib_bar;
    private Call call;
    private String idflage = "";

    private Handler upViewHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    super.handleMessage(msg);
                    sRVAdapter = new SongRVAdapter(SongActivity.this, songInfosList);
                    rv_list.setAdapter(sRVAdapter);

                    if (SPUtils.getIsExists(StaticObj.getCategory_id() + StaticObj.getCurrCata().getAppId())) {
                        SPUtils.putIsExists(StaticObj.getCategory_id() + StaticObj.getCurrCata().getAppId());

                        if (!"-1".equals(StaticObj.getCurrCata().getAppId())) {
                            DialogUtils.showDesDialog(SongActivity.this, getWindow(), R.mipmap.btn_listen, new DialogUtils.BtnStartListener() {
                                @Override
                                public void onDoSome() {
                                    sRVAdapter.fastItem();
                                }
                            });
                        }
                    }
                    iv_info.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    AppUtils.showToast(getString(R.string.net_warnnetwork));
                    break;
            }
            gib_bar.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 保持屏幕常亮
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_my_third);
        initView();

//        if (Constant.DO_REFSIH.equals(getIntent().getStringExtra(Constant.DO))) {
//            if (HttpUtil.isNetworkAvailable()) {
//                getNetData();
//            }
//        } else
        if ("0".equals(StaticObj.getCurrCata().getAppId())) {
            getNativeData(false);
        } else if (!HttpUtil.isNetworkAvailable()) {
            getNativeData(true);
        } else if (HttpUtil.isNetworkAvailable()) {
            getNetData();
            idflage = StaticObj.getCurrCata().getAppId();
        }


        if ("-1".equals(StaticObj.getCurrCata().getAppId())) {
            iv_info.setVisibility(View.INVISIBLE);
        }

        tv_title.setText(StaticObj.getCurrCata().getAppName());

        userTime = new Date().getTime();

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!"0".equals(StaticObj.getCurrCata().getAppId()))
            if (!idflage.equals(StaticObj.getCurrCata().getAppId())) {
                if (HttpUtil.isNetworkAvailable()) {
                    gib_bar.setVisibility(View.VISIBLE);
                    songInfosList.clear();
                    sRVAdapter.notifyDataSetChanged();
                    iv_info.setVisibility(View.GONE);
                    tv_title.setText(StaticObj.getCurrCata().getAppName());
                    getNetData();
                }
            }

        // 注册广播接收机制 : 更新应用列表状态
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(DownManagerUtil.DOWNLOAD_ACTION); // progress
        this.registerReceiver(AppReceiver, mIntentFilter);

        MusicUtils.getMusicUtils().playOrPauseBgsc(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(AppReceiver);
        if (player != null && player.isPlaying()) {
            player.pause();
        }
        if (call != null)
            call.cancel();
    }

    /**
     * 开始播放音乐
     */

    private void start() {
        try {
            player.reset(); // 重置多媒体
            player.setDataSource(StaticObj.dataIng.getSavePath());// 为多媒体对象设置播放路径
            player.prepare();// 准备播放
            player.start();// 开始播放
            startAnim();
            sRVAdapter.notifyDataSetChanged();
            sb_player.setEnabled(true);
            Listening();
        } catch (Exception e) {
        }
    }


    public void reStartPlay(CoreDataBean.DataBean data) {
        if (StaticObj.dataIng != null) {
            if (data.getAppId().equals(StaticObj.dataIng.getAppId())) {
                if (isPlaying()) {
                    pause();
                } else {
                    startPlay();
                }
            } else {
                StaticObj.dataIng = data;
                start();
                startPlay();
            }
        } else {
            StaticObj.dataIng = data;
            start();
            startPlay();
        }
        sRVAdapter.notifyItemChanged(StaticObj.dataIng.getDd().getPosation());
    }

    public void startPlay() {
        if (player != null) {
            if (StaticObj.dataIng != null) {
                player.start();
            } else {
                pause();
            }
            startAnim();
        }

    }

    // 暂停
    public void pause() {
        if (player != null) {
            player.pause();
            startAnim();
        }
    }

    // 停止
    public void stopPlay() {
        pause();
        if (player != null) {
            player.stop();
        }
        StaticObj.dataIng = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.stop();
        }
        player.release();
        handler.removeCallbacks(updateThread);
        player = null;
        StaticObj.dataIng = null;

        HttpUtil.getUtils().statistics(StaticObj.getCurrCata().getAppId(), StaticObj.getCategory_id(), "1", "1", "0", (new Date().getTime() - userTime));
    }

    /**
     * 方法说明：用来控制seekBar
     */
    protected Handler handler = new Handler();
    protected Runnable updateThread = new Runnable() {
        public void run() {
            // 获得歌曲现在播放位置并设置成播放进度条的值
            if (player != null && !isTSB && player.isPlaying()) {
                sb_player.setProgress(player.getCurrentPosition());
            }
            // 每次延迟500毫秒再启动线程
            handler.postDelayed(updateThread, 100);
        }
    };

    public void initView() {

        player = new MediaPlayer();

        rv_list = (RecyclerView) findViewById(R.id.rv_list);

//        sdv_list = (SlideAndDragListView) findViewById(R.id.sdv_list);

//        rg_btn = (RadioGroup) findViewById(R.id.rg_btn);

        child_more_back = (ImageView) findViewById(R.id.child_more_back);// 返回按钮和吉祥物
        iv_info = (ImageView) findViewById(R.id.iv_info);

        iv_anmi = (GifImageView) findViewById(R.id.iv_anmi);

        sb_player = (SeekBar) findViewById(R.id.sb_player);

        gib_bar = (GifImageView) findViewById(R.id.gib_bar);

        tv_title = (TextView) findViewById(R.id.tv_title);

        tv_time = (TextView) findViewById(R.id.tv_time);

        sb_player.setEnabled(false);

        child_more_back.setOnClickListener(this);
        iv_info.setOnClickListener(this);

//        rg_btn.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId) {
//                    case R.id.bt_bofang:
//                        startPlay();
//                        break;
//                    case R.id.bt_zanting:
//                        pause();
//                        break;
//                }
//                sRVAdapter.notifyDataSetChanged();
//            }
//        });

        sb_player.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser && player != null) {
                    newTime = progress;
                }
                tv_time.setText(toTime(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isTSB = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isTSB = false;
                player.seekTo(newTime);
            }
        });

        LinearLayoutManager rlm = new LinearLayoutManager(this);
        rlm.setOrientation(LinearLayoutManager.VERTICAL);
        rv_list.setLayoutManager(rlm);

    }


    private void getNativeData(boolean flage) {
        try {
            if (flage) {
                Selector selec = Selector.from(CoreDataBean.DataBean.class);
                selec.expr("cardID = '" + StaticObj.getCurrCata().getAppId() + "' AND categoryID = '" + Constant.CATEGORY_SONG + "'");
                songInfosList = MyApplication.getInstance().getDbUtisl().findAll(selec);

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_SONG + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (songInfosList != null)
                            for (CoreDataBean.DataBean db : songInfosList) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    break;
                                }
                            }
                    }
            } else {

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr(" categoryID =" + Constant.CATEGORY_SONG + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                Selector sel_db = Selector.from(CoreDataBean.DataBean.class);
                sel_db.expr(" categoryID =" + Constant.CATEGORY_SONG);
                List<CoreDataBean.DataBean> ldb = MyApplication.getInstance().getDbUtisl().findAll(sel_db);

                songInfosList = new ArrayList<>();
                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (ldb != null)
                            for (CoreDataBean.DataBean db : ldb) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    songInfosList.add(db);
                                    break;
                                }
                            }
                    }
            }
            if (songInfosList.size() > 0)
                upViewHandler.sendEmptyMessage(0);
            else
                upViewHandler.sendEmptyMessage(1);
        } catch (Exception e) {
            int i = 0;
        }
    }

    /**
     * 方法说明：对所有数据进行请求
     */
    public void getNetData() {
        String url = API.generalDetail();
        call = HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                upViewHandler.sendEmptyMessage(1);
            }

            @Override
            public void onNetSucceed(String json) {
                try {

                    CoreDataBean dat = JSON.parseObject(json, CoreDataBean.class);
                    songInfosList = dat.getData();

                    MyApplication.getInstance().saveItem(dat.getData());

                    Selector selec = Selector.from(CoreDataBean.DownData.class);
                    selec.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_SONG + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");

                    List<CoreDataBean.DownData> lcd = MyApplication.getInstance().getDbUtisl().findAll(selec);
                    if (lcd != null)
                        for (CoreDataBean.DownData dd : lcd) {
                            if (songInfosList != null)
                                for (CoreDataBean.DataBean cdb : songInfosList) {
                                    if (dd.getAppId().equals(cdb.getAppId())) {
                                        cdb.setDd(dd);
                                        break;
                                    }
                                }
                        }
                    if (songInfosList.size() > 0)
                        upViewHandler.sendEmptyMessageDelayed(0, 1500);
                    else
                        upViewHandler.sendEmptyMessage(1);

                } catch (DbException de) {
                    try {
                        MyApplication.getInstance().getDbUtisl().dropTable(CoreDataBean.DataBean.class);
                        getNetData();
                    } catch (DbException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    /**
     * 方法说明：监听播放的动作
     */
    private void Listening() {
        // 监听音乐播放中
        player.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                sb_player.setMax(player.getDuration());
                handler.post(updateThread);

            }
        });

        // 监听播放完毕后
        player.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                for (int i = StaticObj.dataIng.getDd().getPosation() + 1; i < songInfosList.size(); i++) {
                    if (songInfosList.get(i).getDd().getDownState() == DownManagerUtil.DOWNLOAD_FINISH) {
                        reStartPlay(songInfosList.get(i));
                        return;
                    }
                }
                sb_player.setProgress(0);
                pause();
                sb_player.setEnabled(false);
                int pos = StaticObj.dataIng.getDd().getPosation();
                StaticObj.dataIng = null;
                sRVAdapter.notifyItemChanged(pos);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }


    private void startAnim() {
        if (player.isPlaying()) {
            iv_anmi.setImageResource(R.mipmap.anmi_music);
        } else {
            iv_anmi.setImageResource(R.mipmap.anmi_music_s);
        }
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {

            case R.id.child_more_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_anmi:
                MusicUtils.getMusicUtils().playSound(R.raw.supergem_chainreaction_2);
                break;
            case R.id.iv_info:
                DialogUtils.showDesDialog(SongActivity.this, getWindow(), R.mipmap.btn_listen, new DialogUtils.BtnStartListener() {
                    @Override
                    public void onDoSome() {
                        sRVAdapter.fastItem();
                    }
                });
                break;
        }
    }

    /**
     * 方法说明：下载的服务
     */
    private BroadcastReceiver AppReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(), DownManagerUtil.DOWNLOAD_ACTION)) {
                if (sRVAdapter != null) {
                    CoreDataBean.DataBean data = (CoreDataBean.DataBean) intent.getSerializableExtra(DownManagerUtil.DOWNLOAD_OBJ);
                    sRVAdapter.notifyItemChanged(data.getDd().getPosation());
                }
            }
        }
    };


//    class MyPhoneStateListener extends PhoneStateListener {
//
//        @Override
//        public void onCallStateChanged(int state, String incomingNumber) {
//            switch (state) {
//                case TelephonyManager.CALL_STATE_IDLE: // 空闲
//                    Log.i("phone", "电话空闲");
//                    if (player != null && !player.isLooping()) {
//                        player.start();
//                        if (player.isPlaying()) {
////                            startAnimation();
//                        }
//                    }
//                    break;
//                case TelephonyManager.CALL_STATE_RINGING: // 来电
//                    Log.i("phone", "有来电");
//                    if (player != null && player.isPlaying()) {
//                        player.pause();
//                    }
//                    break;
//                case TelephonyManager.CALL_STATE_OFFHOOK: // 摘机（正在通话中）
//                    Log.i("phone", "通话中");
//                    break;
//            }
//        }
//    }

    /**
     * 时间转化处理
     */
    public String toTime(long time) {
        time /= 1000;
        long minute = time / 60;
        long second = time % 60;
        minute %= 60;
        return String.format(" %02d:%02d ", minute, second);
    }


    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent == null)
            return;
        super.onNewIntent(intent);

        switch (intent.getStringExtra(Constant.DO)) {
//            case Constant.DO_REFSIH:
//
//                break;
        }
    }

}