package com.sinoangel.kids.mode_new.ks.set;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.set.bean.UserInfo;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    private ImageView iv_back, iv_register;
    private EditText et_name, et_account, et_pwd, et_confirm;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    AppUtils.showToast(getString(R.string.reg_zcsb));
                    break;
                case 1:
                    AppUtils.showToast(getString(R.string.net_warnnetwork));
                    break;
                case 2:
                    AppUtils.showToast(getString(R.string.net_warnnetwork));
                    break;
                case 3:
                    AppUtils.showToast(getString(R.string.reg_zccg));
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_register = (ImageView) findViewById(R.id.iv_register);
        et_name = (EditText) findViewById(R.id.et_name);
        et_account = (EditText) findViewById(R.id.et_account);
        et_pwd = (EditText) findViewById(R.id.et_pwd);
        et_confirm = (EditText) findViewById(R.id.et_confirm);

        iv_back.setOnClickListener(this);
        iv_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_register:
                if (et_name.getText().toString().length() == 0) {
                    AppUtils.showToast(getString(R.string.qingshuruyonghuming));
                } else if (!AppUtils.userFormat(et_name.getText().toString())) {
                    AppUtils.showToast(getString(R.string.usernameyouwu));
                } else if (!AppUtils.emailFormat(et_account.getText().toString())) {
                    AppUtils.showToast(getString(R.string.youxianngyouwu));
                } else if (et_pwd.getText().toString().length() < 6) {
                    AppUtils.showToast(getString(R.string.qingshuruzhanghao) + "(6-18)");
                } else if (!et_confirm.getText().toString().equals(et_pwd.getText().toString())) {
                    AppUtils.showToast(getString(R.string.liangcibuyizhi));
                } else if (!HttpUtil.isNetworkAvailable()) {
                    AppUtils.showToast(getString(R.string.net_nonetwork));
                } else {
                    Map<String, String> mss = new HashMap<>();
                    mss.put("username", et_name.getText().toString());
                    mss.put("email", et_account.getText().toString());
                    mss.put("password", et_pwd.getText().toString());

                    HttpUtil.getUtils().getJsonString(API.userRegister(mss), new HttpUtil.OnNetResponseListener() {
                        @Override
                        public void onNetFail() {
                            handler.sendEmptyMessage(1);
                        }

                        @Override
                        public void onNetSucceed(String json) {
                            UserInfo ui = JSON.parseObject(json, UserInfo.class);
                            if (ui.getFlag() == 1) {
                                Map<String, String> mss = new HashMap<>();
                                mss.put("email", et_account.getText().toString());
                                mss.put("password", et_pwd.getText().toString());
                                goLogin(mss);
                            } else {
                                handler.sendEmptyMessage(0);
                            }
                        }
                    });
                }
                break;
        }
    }

    public void goLogin(Map<String, String> mss) {

        String url = API.userLogin(API.NET_USER_LOGIN, mss);
        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    UserInfo ui = JSON.parseObject(json, UserInfo.class);
                    if (ui.getFlag() == 1) {
                        StaticObj.setUid(ui.getData());

                        startActivity(new Intent(RegisterActivity.this, H5StoreActivity.class));
                        String url = API.getUpdateuserLang();
                        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
                            @Override
                            public void onNetFail() {
                            }

                            @Override
                            public void onNetSucceed(String json) {
                            }
                        });
                        finish();
                    }
                } catch (Exception e) {
                }
            }
        });
    }

}
