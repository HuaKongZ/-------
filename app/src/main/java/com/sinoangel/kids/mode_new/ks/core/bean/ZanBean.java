package com.sinoangel.kids.mode_new.ks.core.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/8/2 0002.
 */
public class ZanBean {

    /**
     * flag : 1
     * error : []
     * data : {"singleId":"3839","seriesId":0,"isLike":0}
     */

    private int flag;
    /**
     * singleId : 3839
     * seriesId : 0
     * isLike : 0
     */

    private DataBean data;
    private List<?> error;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public List<?> getError() {
        return error;
    }

    public void setError(List<?> error) {
        this.error = error;
    }

    public static class DataBean {
        private String singleId;
        private int seriesId;
        private int isLike;

        public String getSingleId() {
            return singleId;
        }

        public void setSingleId(String singleId) {
            this.singleId = singleId;
        }

        public int getSeriesId() {
            return seriesId;
        }

        public void setSeriesId(int seriesId) {
            this.seriesId = seriesId;
        }

        public int getIsLike() {
            return isLike;
        }

        public void setIsLike(int isLike) {
            this.isLike = isLike;
        }
    }
}
