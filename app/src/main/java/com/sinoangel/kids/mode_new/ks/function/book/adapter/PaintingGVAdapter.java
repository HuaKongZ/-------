package com.sinoangel.kids.mode_new.ks.function.book.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.function.book.PaintingToolActivity;
import com.sinoangel.kids.mode_new.ks.function.book.PaintingToolActivitys;
import com.sinoangel.kids.mode_new.ks.function.book.bean.BookBean;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.sinoangel.kids.mode_new.ks.util.ZipUnpackUtils;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Administrator on 2016/7/6.
 */
public class PaintingGVAdapter extends RecyclerView.Adapter<PaintingGVAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {
    private Context mContext;
    private List<CoreDataBean.DataBean> baseAppInfoList;

    private Handler mh = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Object[] obj = (Object[]) msg.obj;
            BookBean bb = (BookBean) obj[0];
            CoreDataBean.DataBean dd = (CoreDataBean.DataBean) obj[1];
            switch (msg.what) {
                case 0:
                    try {
                        Intent intent = new Intent();
                        intent.putExtra("bbean", bb);
                        intent.putExtra("cdbbean", dd);
                        if ("1".equals(bb.getVideo())) {
                            intent.setClass(mContext, PaintingToolActivity.class);
                        } else {
                            intent.setClass(mContext, PaintingToolActivitys.class);
                        }
                        mContext.startActivity(intent);
                        DialogUtils.dismissProgressDialog();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 1:

                    break;
            }
        }
    };

    public void setData(List<CoreDataBean.DataBean> baseAppInfoList) {
        this.baseAppInfoList = baseAppInfoList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_painting, null);
        int hei = parent.getContext().getResources().getDisplayMetrics().heightPixels;
        view.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, hei / 4));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        // 设置tag，作为唯一标示
        CoreDataBean.DataBean data = baseAppInfoList.get(position);

        ImageUtils.showImgUrl(data.getIcon(), viewHolder.imageapp);

        if (data.getDd().getDownState() != DownManagerUtil.DOWNLOAD_FINISH) {

            for (CoreDataBean.DataBean dating : StaticObj.ld) {
                if (dating.getAppId().equals(data.getAppId())) {
                    dating.getDd().setPosation(position);
                    baseAppInfoList.set(position, dating);
                    break;
                }
            }


            if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
                // 这是在下载当中
                viewHolder.imagecover.setProgress(data.getDd().getDownSize());
                viewHolder.imagebtn.setImageResource(R.mipmap.animation_play_press);
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
                viewHolder.imagebtn.setImageResource(R.mipmap.animation_wait);
                viewHolder.imagecover.setProgress(0);
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE) {
                viewHolder.imagebtn.setImageResource(R.mipmap.animation_play_puase);
                viewHolder.imagecover.setProgress(0);
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FAIL) {
                viewHolder.imagebtn.setImageResource(R.mipmap.animation_play_faild);
                viewHolder.imagecover.setProgress(0);
            } else {
                viewHolder.imagecover.setProgress(0);
                viewHolder.imagebtn.setImageResource(R.mipmap.app_download);
            }
            viewHolder.imagecover.setVisibility(View.VISIBLE);
            viewHolder.imagebtn.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imagebtn.setVisibility(View.INVISIBLE);
            viewHolder.imagecover.setVisibility(View.INVISIBLE);
        }

        if ("1".equals(data.getIsNew())) {
            if (SPUtils.getNew(data.getAppId())) {
                viewHolder.iv_isnew.setVisibility(View.VISIBLE);
            } else {
                viewHolder.iv_isnew.setVisibility(View.GONE);
            }
        } else {
            viewHolder.iv_isnew.setVisibility(View.GONE);
        }


        //添加点击事件
        data.getDd().setPosation(position);
        viewHolder.rl_box.setTag(data);

    }

    @Override
    public int getItemCount() {
        return baseAppInfoList == null ? 0 : baseAppInfoList.size();
    }

    @Override
    public void onClick(View v) {
        final CoreDataBean.DataBean _data = (CoreDataBean.DataBean) v.getTag();
        StaticObj.dataIng = _data;

        if (_data.getDd().getPosation() != baseAppInfoList.size() - 1) {
            StaticObj.isEnd = true;
        } else {
            StaticObj.isEnd = false;
        }

        SPUtils.putNew(_data.getAppId());
        MusicUtils.getMusicUtils().playSound(R.raw.connect_15);

        if (_data.isFileExist()) {
            DialogUtils.showProgressDialog(mContext, "");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ZipUnpackUtils.Unzip(_data.getSavePath(), _data.getUnZipPath());
                    String str = ZipUnpackUtils.ReadTxtFile(_data.getUnZipPath() + "1.txt");
                    try {
                        BookBean bb = JSON.parseObject(str, BookBean.class);
                        Message msg = new Message();

                        msg.obj = new Object[]{
                                bb, _data
                        };

                        msg.what = 0;
                        mh.sendMessage(msg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            return;
        } else if (_data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
            DownManagerUtil.getInstance().stopDownload(_data);
            _data.getDd().setDownState(DownManagerUtil.DOWNLOAD_PUASE);
            try {
                MyApplication.getInstance().getDbUtisl().update(_data.getDd(), "downState");
            } catch (DbException e) {
                e.printStackTrace();
            }
            notifyItemChanged(_data.getDd().getPosation());
        } else if (_data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
            DownManagerUtil.getInstance().stopDownload(_data);
            _data.getDd().setDownState(DownManagerUtil.DOWNLOAD_NO);
            try {
                MyApplication.getInstance().getDbUtisl().update(_data.getDd(), "downState");
            } catch (DbException e) {
                e.printStackTrace();
            }
            notifyItemChanged(_data.getDd().getPosation());
        } else {
            DownManagerUtil.getInstance().downFile(_data);

        }
    }

    @Override
    public boolean onLongClick(View view) {
        final CoreDataBean.DataBean data = (CoreDataBean.DataBean) view.getTag();
        final File file = new File(DownManagerUtil.getInstance().savePath + data.getAppId());
        if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FINISH || data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE || data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FAIL) {
            DialogUtils.YesOrNoDialog(mContext, R.mipmap.text_delete, new DialogUtils.YeOrOnListener() {

                @Override
                public void onOk() {
                    if (DownManagerUtil.deleteAllFile(file)) {
                        AppUtils.showToast(mContext.getString(R.string.del_ok));
                        MusicUtils.getMusicUtils().playSound(R.raw.removing_creatures_8);
                        try {
                            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_NO);
                            data.getDd().setDownSize(0);
                            MyApplication.getInstance().getDbUtisl().delete(data.getDd());
                            MyApplication.getInstance().getDbUtisl().delete(data);
                        } catch (DbException e) {
                            e.printStackTrace();
                        }
                        notifyItemChanged(data.getDd().getPosation());
                    } else {
                        AppUtils.showToast(mContext.getString(R.string.del_no));
                    }

                }

                @Override
                public void onNo() {

                }
            });
        }
        return true;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageapp, iv_isnew;// 展示的appaIcon
        private ProgressBar imagecover;// 黑色遮挡物
        private ImageView imagebtn;// 下载按钮
        private RelativeLayout rl_box;

        public ViewHolder(View convertView) {
            super(convertView);
            rl_box = (RelativeLayout) convertView.findViewById(R.id.rl_box);
            imageapp = (ImageView) convertView.findViewById(R.id.item_painting_image);
            imagecover = (ProgressBar) convertView.findViewById(R.id.item_painting_cover);
            imagebtn = (ImageView) convertView.findViewById(R.id.item_painting_btn);
            iv_isnew = (ImageView) convertView.findViewById(R.id.iv_isnew);

            rl_box.setOnClickListener(PaintingGVAdapter.this);
            rl_box.setOnLongClickListener(PaintingGVAdapter.this);
        }
    }

    public void nextItem() {
        if (StaticObj.dataIng.getDd().getPosation() + 1 < baseAppInfoList.size()) {

            CoreDataBean.DataBean nextD = baseAppInfoList.get(StaticObj.dataIng.getDd().getPosation() + 1);

            if (nextD.getDd().getDownState() != DownManagerUtil.DOWNLOAD_ING && nextD.getDd().getDownState() != DownManagerUtil.DOWNLOAD_WAIT) {
                View v = new View(mContext);
                v.setTag(nextD);
                onClick(v);
            }
        } else {
        }
    }

    public void fastItem() {
        CoreDataBean.DataBean nextD = baseAppInfoList.get(0);
        View v = new View(mContext);
        v.setTag(nextD);
        onClick(v);
    }

}
