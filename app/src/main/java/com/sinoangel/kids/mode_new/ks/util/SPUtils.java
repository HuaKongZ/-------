package com.sinoangel.kids.mode_new.ks.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;

public class SPUtils {

    private final static String SP_SINOANGEL = "spsinoangel";
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;


    public static void putUserPwd(String pwd) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putString("PASSWORD", pwd);
        editor.commit();
    }

    public static String getUserPWD(Context context) {
        preferences = context.getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getString("PASSWORD", "");
    }

    public static void putUserName(String name) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putString("NAME", name);
        editor.commit();
    }

    public static String getUserName() {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getString("NAME", "");
    }

    /**
     * 记录是否为新
     */
    public static void putNew(String appid) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putBoolean(appid, false);
        editor.commit();
    }

    /**
     * 获取是否为新
     */
    public static boolean getNew(String appid) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getBoolean(appid, true);
    }

    public static void putPhoneId(String name) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putString("PID", name);
        editor.commit();
    }

    public static String getPhoneId() {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getString("PID", "860846030000447");
    }

    public static void putAge(int name) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putInt("AGE", name);
        editor.commit();
    }

    public static int getAge() {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getInt("AGE", 2);
    }

    public static void putTime(int time) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putInt("TIME", time);
        editor.commit();
    }

    public static int getTime() {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getInt("TIME", 0);
    }

    public static void putEveryFlage(long time) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putLong("EVERTIME", time);
        editor.commit();
    }

    public static long getEveryFlage() {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getLong("EVERTIME", 0);
    }


    public static void putIsExists(String name) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putBoolean(name, false);
        editor.commit();
    }

    public static boolean getIsExists(String name) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getBoolean(name, true);
    }

    public static void putMusicFlage(boolean flage) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putBoolean("BK_MUSIC", flage);
        editor.commit();
    }

    public static boolean getMusicFlage() {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getBoolean("BK_MUSIC", true);
    }

    public static void putLocation(String loc) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putString("LOCATION", loc);
        editor.commit();
    }

    public static String getLocation() {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getString("LOCATION", "1");
    }

    public static String getStringVar(String name) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        return preferences.getString(name, null);
    }

    public static void putStringVar(String name, String var) {
        preferences = MyApplication.getInstance().getSharedPreferences(SP_SINOANGEL, 0);
        editor = preferences.edit();
        editor.putString(name, var);
        editor.commit();
    }
}
