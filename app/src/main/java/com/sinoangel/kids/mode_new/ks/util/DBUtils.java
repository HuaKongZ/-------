package com.sinoangel.kids.mode_new.ks.util;

import android.content.Context;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;

import java.util.List;

/**
 * Created by Z on 2016/12/1.
 */

public class DBUtils {
    private static DbUtils dbUtisl;

    public static DbUtils getDbUtisl() {
        return dbUtisl;
    }

    public static void initDB(Context context) {
        if (dbUtisl == null)
            dbUtisl = DbUtils.create(context, "SINOANGEL_DATA");
    }

    /**
     * 异步缓存
     */
    public static void saveItem(final List<CoreDataBean.DataBean> lcdb) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dbUtisl.saveOrUpdateAll(lcdb);
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void saveCardData(final List<Cate.DataBean> lcdb) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dbUtisl.saveOrUpdateAll(lcdb);
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void updateDD() {
        try {
            List<CoreDataBean.DownData> ldd = dbUtisl.findAll(CoreDataBean.DownData.class);
            dbUtisl.dropTable(CoreDataBean.DataBean.class);
            dbUtisl.dropTable(CoreDataBean.DownData.class);
            dbUtisl.saveOrUpdateAll(ldd);
        } catch (DbException e) {
            e.printStackTrace();
        }

    }
}
