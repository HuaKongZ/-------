package com.sinoangel.kids.mode_new.ks.set;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.squareup.okhttp.Call;

import java.util.HashMap;
import java.util.Map;

public class LostPWDActivity extends BaseActivity implements View.OnClickListener {
    private ImageView iv_ok, iv_back;
    private EditText et_email;
    private Call call;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    AppUtils.showToast(getString(R.string.zhaohuimima));
                    break;
                case 1:
                    DialogUtils.dismissProgressDialog();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lost_pwd);
        iv_ok = (ImageView) findViewById(R.id.iv_ok);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        et_email = (EditText) findViewById(R.id.et_email);
        iv_back.setOnClickListener(this);
        iv_ok.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_ok:
                if (!AppUtils.emailFormat(et_email.getText().toString())) {
                    AppUtils.showToast(getString(R.string.youxianngyouwu));
                } else {
                    DialogUtils.showProgressDialog(LostPWDActivity.this, "");
                    Map<String, String> mss = new HashMap<>();
                    mss.put("email", et_email.getText().toString());
                    String url = API.NET_FINDPWDBYEMAIL;
                    call = HttpUtil.getUtils().getJsonStringByPost(url, mss, new HttpUtil.OnNetResponseListener() {
                        @Override
                        public void onNetFail() {
                            handler.sendEmptyMessage(1);
                        }

                        @Override
                        public void onNetSucceed(String json) {

                            handler.sendEmptyMessage(0);
                            finish();
                        }
                    });
                }
                break;
            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (call != null)
            call.cancel();
    }
}
