package com.sinoangel.kids.mode_new.ks.util;

import android.view.View;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.set.bean.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * 类使用使用：所有常量都储存在此类中
 */
public class StaticObj {


    public static final int flags = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN;


    private static String category_id;//大类ID
    private static Cate.DataBean currCata;//当前选中的卡片

    private static UserInfo.DataBean uid;//用户
    public static CoreDataBean.DataBean dataIng;//正在使用的单品
    public static List<CoreDataBean.DataBean> ld = new ArrayList<>();//正在下载的集合


    public static int YKAgle;//优酷结束标记
    public static boolean isEnd;//标记产品是否是列表最后一个

    public static boolean isCountDown;//判断是否开启计时


    public static String getCategory_id() {
        if (category_id == null)
            category_id = SPUtils.getStringVar(Constant.CATEGORYID);
        return category_id;
    }

    public static void setCategory_id(String category_id) {
        StaticObj.category_id = category_id;
        SPUtils.putStringVar(Constant.CATEGORYID, category_id);

    }

    public static Cate.DataBean getCurrCata() {
        if (currCata == null) {
            String str = SPUtils.getStringVar(Constant.CARDOBJ);
            currCata = JSON.parseObject(str, Cate.DataBean.class);
        }
        return currCata;
    }

    public static void setCurrCata(Cate.DataBean currCata) {
        StaticObj.currCata = currCata;
        String str = JSON.toJSONString(currCata);
        SPUtils.putStringVar(Constant.CARDOBJ, str);
    }

    public static UserInfo.DataBean getUid() {
        if (uid == null) {
            String str = SPUtils.getStringVar(Constant.USERBEAN);
            if (str != null)
                uid = JSON.parseObject(str, UserInfo.DataBean.class);
        }
        return uid;
    }

    public static void setUid(UserInfo.DataBean uid) {
        StaticObj.uid = uid;
        if (uid != null) {
            String str = JSON.toJSONString(uid);
            SPUtils.putStringVar(Constant.USERBEAN, str);
        } else {
            SPUtils.putStringVar(Constant.USERBEAN, null);
        }
    }
}
