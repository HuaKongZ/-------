package com.sinoangel.kids.mode_new.ks.function.book.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/7/29 0029.
 */
public class BookBean implements Serializable {

    /**
     * Title : 恐龙宝贝四格漫画
     * FileTitle : DinosaurBabyFour
     * Screen : portrait
     * Video : 2
     * data : [{"position":1,"Background":"01 (1).jpg"},{"position":2,"Background":"01 (2).jpg"},{"position":3,"Background":"01 (3).jpg"},{"position":4,"Background":"01 (4).jpg"},{"position":5,"Background":"01 (5).jpg"},{"position":6,"Background":"01 (6).jpg"}]
     */

    private String Title;
    private String FileTitle;
    private String Screen;
    private String Video;
    /**
     * position : 1
     * Background : 01 (1).jpg
     */

    private List<DataBean> data;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getFileTitle() {
        return FileTitle;
    }

    public void setFileTitle(String FileTitle) {
        this.FileTitle = FileTitle;
    }

    public String getScreen() {
        return Screen;
    }

    public void setScreen(String Screen) {
        this.Screen = Screen;
    }

    public String getVideo() {
        return Video;
    }

    public void setVideo(String Video) {
        this.Video = Video;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        private int position;
        private String Background;
        private String mp3File;
        private String SubTitle1;
        private String SubTitle2;
        private String Second;

        public String getMp3File() {
            return mp3File;
        }

        public void setMp3File(String mp3File) {
            this.mp3File = mp3File;
        }

        public String getSubTitle1() {
            return SubTitle1;
        }

        public void setSubTitle1(String subTitle1) {
            SubTitle1 = subTitle1;
        }

        public String getSubTitle2() {
            return SubTitle2;
        }

        public void setSubTitle2(String subTitle2) {
            SubTitle2 = subTitle2;
        }

        public String getSecond() {
            return Second;
        }

        public void setSecond(String second) {
            Second = second;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getBackground() {
            return Background;
        }

        public void setBackground(String Background) {
            this.Background = Background;
        }
    }
}
