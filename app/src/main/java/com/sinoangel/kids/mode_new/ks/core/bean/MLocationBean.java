package com.sinoangel.kids.mode_new.ks.core.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/9/2 0002.
 */
public class MLocationBean {

    /**
     * status : 0
     * result : {"location":{"lng":39.90372100000009,"lat":74.00002218455094},"formatted_address":"","business":"","addressComponent":{"adcode":"0","city":"","country":"","direction":"","distance":"","district":"","province":"","street":"","street_number":"","country_code":0},"pois":[],"poiRegions":[],"sematic_description":"","cityCode":0}
     */

    private int status;
    /**
     * location : {"lng":39.90372100000009,"lat":74.00002218455094}
     * formatted_address :
     * business :
     * addressComponent : {"adcode":"0","city":"","country":"","direction":"","distance":"","district":"","province":"","street":"","street_number":"","country_code":0}
     * pois : []
     * poiRegions : []
     * sematic_description :
     * cityCode : 0
     */

    private ResultBean result;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * lng : 39.90372100000009
         * lat : 74.00002218455094
         */

        private LocationBean location;
        private String formatted_address;
        private String business;
        /**
         * adcode : 0
         * city :
         * country :
         * direction :
         * distance :
         * district :
         * province :
         * street :
         * street_number :
         * country_code : 0
         */

        private AddressComponentBean addressComponent;
        private String sematic_description;
        private int cityCode;
        private List<?> pois;
        private List<?> poiRegions;

        public LocationBean getLocation() {
            return location;
        }

        public void setLocation(LocationBean location) {
            this.location = location;
        }

        public String getFormatted_address() {
            return formatted_address;
        }

        public void setFormatted_address(String formatted_address) {
            this.formatted_address = formatted_address;
        }

        public String getBusiness() {
            return business;
        }

        public void setBusiness(String business) {
            this.business = business;
        }

        public AddressComponentBean getAddressComponent() {
            return addressComponent;
        }

        public void setAddressComponent(AddressComponentBean addressComponent) {
            this.addressComponent = addressComponent;
        }

        public String getSematic_description() {
            return sematic_description;
        }

        public void setSematic_description(String sematic_description) {
            this.sematic_description = sematic_description;
        }

        public int getCityCode() {
            return cityCode;
        }

        public void setCityCode(int cityCode) {
            this.cityCode = cityCode;
        }

        public List<?> getPois() {
            return pois;
        }

        public void setPois(List<?> pois) {
            this.pois = pois;
        }

        public List<?> getPoiRegions() {
            return poiRegions;
        }

        public void setPoiRegions(List<?> poiRegions) {
            this.poiRegions = poiRegions;
        }

        public static class LocationBean {
            private double lng;
            private double lat;

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }
        }

        public static class AddressComponentBean {
            private String adcode;
            private String city;
            private String country;
            private String direction;
            private String distance;
            private String district;
            private String province;
            private String street;
            private String street_number;
            private int country_code;

            public String getAdcode() {
                return adcode;
            }

            public void setAdcode(String adcode) {
                this.adcode = adcode;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getDirection() {
                return direction;
            }

            public void setDirection(String direction) {
                this.direction = direction;
            }

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public String getDistrict() {
                return district;
            }

            public void setDistrict(String district) {
                this.district = district;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getStreet() {
                return street;
            }

            public void setStreet(String street) {
                this.street = street;
            }

            public String getStreet_number() {
                return street_number;
            }

            public void setStreet_number(String street_number) {
                this.street_number = street_number;
            }

            public int getCountry_code() {
                return country_code;
            }

            public void setCountry_code(int country_code) {
                this.country_code = country_code;
            }
        }
    }
}
