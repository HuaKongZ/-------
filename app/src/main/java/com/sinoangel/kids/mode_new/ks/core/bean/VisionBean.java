package com.sinoangel.kids.mode_new.ks.core.bean;

/**
 * Created by Administrator on 2016/7/25 0025.
 */
public class VisionBean {

    /**
     * visionCode : 3
     * visionName : 1.0.4
     * updateContent : 有新版本了!
     * isUpdate : false
     */

    private int visionCode;
    private String visionName;
    private String updateContent;
    private boolean isUpdate;

    public int getVisionCode() {
        return visionCode;
    }

    public void setVisionCode(int visionCode) {
        this.visionCode = visionCode;
    }

    public String getVisionName() {
        return visionName;
    }

    public void setVisionName(String visionName) {
        this.visionName = visionName;
    }

    public String getUpdateContent() {
        return updateContent;
    }

    public void setUpdateContent(String updateContent) {
        this.updateContent = updateContent;
    }

    public boolean getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }
}
