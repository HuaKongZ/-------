package com.sinoangel.kids.mode_new.ks.set;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.MainActivity;
import com.sinoangel.kids.mode_new.ks.core.base.BaseFragmentActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.function.cartoon.WEBViewActivity;
import com.sinoangel.kids.mode_new.ks.set.bean.AdvBean;
import com.sinoangel.kids.mode_new.ks.set.bean.QQBean;
import com.sinoangel.kids.mode_new.ks.set.bean.UserInfo;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.GifUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.sinoangel.kids.mode_new.ks.widget.YKPlayerActivity;
import com.sinoangel.kids.mode_new.ks.wxapi.WeiXinBean;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class LoginActivity extends BaseFragmentActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    private UMShareAPI mShareAPI = null;
    private ImageView iv_back, iv_icon_sina, iv_icon_weixin, iv_icon_qq, iv_icon_facebook, iv_icon_guge, iv_icon_twwer, iv_login;
    private EditText et_account, et_pwd;
    private TextView tv_register, tv_lostpwd;
    private int[] giflist = {R.id.gv0, R.id.gv1, R.id.gv2, R.id.gv3, R.id.gv4, R.id.gv5, R.id.gv6, R.id.gv7, R.id.gv8, R.id.gv9, R.id.gv10, R.id.gv11,};
    private LinearLayout ll_EC;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private ProfileTracker profileTracker;


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    AppUtils.showToast(getString(R.string.dengluchenggong));
                    break;
                case 1:
                    AppUtils.showToast(getString(R.string.mingzimimacuowu));
                    DialogUtils.dismissProgressDialog();
                    break;
                case 2:
                    AppUtils.showToast(getString(R.string.net_warnnetwork));
                    DialogUtils.dismissProgressDialog();
                    break;
            }
            iv_icon_facebook.setClickable(true);
            iv_icon_guge.setClickable(true);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        mShareAPI = UMShareAPI.get(this);

        init();

        if (MyApplication.getInstance().systemLanguageType() == Constant.LANGUAGE_EN) {
            FacebookSdk.sdkInitialize(getApplication());
            ll_EC.setVisibility(View.GONE);
        } else {
            ll_EC.setVisibility(View.VISIBLE);
        }

        getADV();

        if (SPUtils.getIsExists(Constant.NEWUSE_NY2)) {
            Intent intent = new Intent(LoginActivity.this, GuideActivity.class);
            intent.putExtra("index", 0);
            intent.putExtra("flage", Constant.NEWUSE_NY2);
            startActivity(intent);
        }
//            isNewOpen();

    }

    private void getADV() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpUtil.getUtils().getJsonString(API.LoginImg(), new HttpUtil.OnNetResponseListener() {
                    @Override
                    public void onNetFail() {
                    }

                    @Override
                    public void onNetSucceed(String json) {
                        final AdvBean ab = JSON.parseObject(json, AdvBean.class);
                        if (ab.getFlag() == 1) {
                            final List<AdvBean.DataBean> lad = ab.getData();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (final AdvBean.DataBean adb : lad) {
                                        GifUtils.getGIFUtils().DownGif(adb.getIcon(), adb.getPosition() + ".gif", new GifUtils.GifListener() {
                                            @Override
                                            public void onGifFinish(String filePath) {
                                                try {
                                                    GifDrawable gifFromPath = new GifDrawable(filePath);
                                                    GifImageView gv = (GifImageView) findViewById(giflist[adb.getPosition()]);
                                                    gv.setImageDrawable(gifFromPath);
                                                    gv.setTag(adb);
                                                    gv.setOnClickListener(vocl);
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }).start();
    }

    private void init() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_icon_sina = (ImageView) findViewById(R.id.iv_icon_sina);
        iv_icon_weixin = (ImageView) findViewById(R.id.iv_icon_weixin);
        iv_icon_qq = (ImageView) findViewById(R.id.iv_icon_qq);
        iv_icon_facebook = (ImageView) findViewById(R.id.iv_icon_facebook);
        iv_icon_guge = (ImageView) findViewById(R.id.iv_icon_guge);
        iv_icon_twwer = (ImageView) findViewById(R.id.iv_icon_twwer);
        et_account = (EditText) findViewById(R.id.et_account);
        et_pwd = (EditText) findViewById(R.id.et_pwd);
        iv_login = (ImageView) findViewById(R.id.iv_login);
        tv_register = (TextView) findViewById(R.id.tv_register);
        tv_lostpwd = (TextView) findViewById(R.id.tv_lostpwd);
        ll_EC = (LinearLayout) findViewById(R.id.ll_ce_login);

        iv_back.setOnClickListener(this);
        iv_icon_sina.setOnClickListener(this);
        iv_icon_weixin.setOnClickListener(this);
        iv_icon_qq.setOnClickListener(this);
        iv_icon_facebook.setOnClickListener(this);
        iv_login.setOnClickListener(this);
        tv_register.setOnClickListener(this);
        tv_lostpwd.setOnClickListener(this);
        iv_icon_guge.setOnClickListener(this);
        iv_icon_twwer.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 93) {
            if (data != null) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            } else {
                iv_icon_guge.setClickable(true);
            }
        } else {
            mShareAPI.onActivityResult(requestCode, resultCode, data);
            if (callbackManager != null)
                callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private UMAuthListener umAuthListener = new UMAuthListener() {


        @Override
        public void onComplete(SHARE_MEDIA platform, int action, final Map<String, String> data) {
            final Map<String, String> mss = new HashMap<>();
            if (platform == SHARE_MEDIA.SINA) {
                mss.put("userName", data.get("com.sina.weibo.intent.extra.NICK_NAME"));
                mss.put("userId", data.get("uid"));
                mss.put("platformName", "weibo");
                mss.put("iconUrl", data.get("com.sina.weibo.intent.extra.USER_ICON"));
                DialogUtils.showProgressDialog(LoginActivity.this, "");
                goLogin(API.NET_USER_OTHERLOGIN, mss);
            } else if (platform == SHARE_MEDIA.QQ) {
                mss.put("userId", data.get("uid"));
                mss.put("openid", data.get("openid"));
                mss.put("oauth_consumer_key", "1105605792");
                mss.put("access_token", data.get("access_token"));
                DialogUtils.showProgressDialog(LoginActivity.this, "");
                HttpUtil.getUtils().getJsonString(API.QQ(mss), new HttpUtil.OnNetResponseListener() {
                    @Override
                    public void onNetFail() {

                    }

                    @Override
                    public void onNetSucceed(String json) {
                        QQBean qq = JSON.parseObject(json, QQBean.class);
                        mss.put("iconUrl", qq.getFigureurl_qq_2());
                        mss.put("userName", qq.getNickname());
                        mss.put("platformName", "qq");
                        goLogin(API.NET_USER_OTHERLOGIN, mss);
                    }
                });
            } else if (platform == SHARE_MEDIA.WEIXIN) {
                mss.put("platformName", "weixin");
                mss.put("openId", data.get("openid"));
                mss.put("userId", data.get("openid"));
                mss.put("access_token", data.get("access_token"));
                HttpUtil.getUtils().getJsonString(API.WeiXin(mss), new HttpUtil.OnNetResponseListener() {
                    @Override
                    public void onNetFail() {

                    }

                    @Override
                    public void onNetSucceed(String json) {
                        WeiXinBean wxb = JSON.parseObject(json, WeiXinBean.class);
                        mss.put("iconUrl", wxb.getHeadimgurl());
                        mss.put("userName", wxb.getNickname());
                        goLogin(API.NET_USER_OTHERLOGIN, mss);
                    }
                });
            }


        }

        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
        }
    };

    public void goLogin(String api, Map<String, String> mss) {
        iv_icon_facebook.setClickable(false);
        iv_icon_guge.setClickable(false);
        String url = API.userLogin(api, mss);
        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                handler.sendEmptyMessage(2);
            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    UserInfo ui = JSON.parseObject(json, UserInfo.class);
                    if (ui.getFlag() == 1) {
                        StaticObj.setUid(ui.getData());
                        setResult(Constant.LOGIN_SUSSCE);
                        handler.sendEmptyMessageDelayed(10, 1000);

                        String url = API.getUpdateuserLang();
                        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
                            @Override
                            public void onNetFail() {

                            }

                            @Override
                            public void onNetSucceed(String json) {
                            }
                        });

                        finish();
                    } else {
                        //用户名密码有误!
                        handler.sendEmptyMessage(1);
                        setResult(Constant.LOGIN_FAILE);
                    }
                } catch (Exception e) {
                    handler.sendEmptyMessage(2);
                    setResult(Constant.LOGIN_FAILE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_icon_sina:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                mShareAPI.doOauthVerify(LoginActivity.this, SHARE_MEDIA.SINA, umAuthListener);
                HttpUtil.getUtils().statistics("", "1011", "0", "0", "0", 0);
                break;
            case R.id.iv_icon_weixin:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                mShareAPI.doOauthVerify(LoginActivity.this, SHARE_MEDIA.WEIXIN, umAuthListener);
                HttpUtil.getUtils().statistics("", "1010", "0", "0", "0", 0);
                break;
            case R.id.iv_icon_qq:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                mShareAPI.doOauthVerify(LoginActivity.this, SHARE_MEDIA.QQ, umAuthListener);
                HttpUtil.getUtils().statistics("", "1012", "0", "0", "0", 0);
                break;
            case R.id.iv_icon_facebook:
                iv_icon_facebook.setClickable(false);
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);

                Profile profile = Profile.getCurrentProfile();
                if (profile == null) {

                    callbackManager = CallbackManager.Factory.create();

                    loginManager = LoginManager.getInstance();
                    loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            profileTracker = new ProfileTracker() {
                                @Override
                                protected void onCurrentProfileChanged(
                                        final Profile oldProfile,
                                        final Profile currentProfile) {
                                    if (currentProfile != null)
                                        fBLogin(currentProfile);
                                    else
                                        iv_icon_facebook.setClickable(true);
                                }
                            };
                        }

                        @Override
                        public void onCancel() {
                            iv_icon_facebook.setClickable(true);
                        }

                        @Override
                        public void onError(FacebookException error) {
                            iv_icon_facebook.setClickable(true);
                        }
                    });

                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));//, "publish_actions"
                } else {
                    fBLogin(profile);
                }
                HttpUtil.getUtils().statistics("", "1013", "0", "0", "0", 0);
                break;
            case R.id.iv_icon_guge:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);

                if (gso == null)
                    gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestEmail()
                            .build();
                if (mGoogleApiClient == null)
                    mGoogleApiClient = new GoogleApiClient.Builder(this)
                            .enableAutoManage(this, this)
                            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                            .build();

                signIn();
                HttpUtil.getUtils().statistics("", "1015", "0", "0", "0", 0);
                break;
            case R.id.iv_icon_twwer:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
//                mShareAPI.doOauthVerify(LoginActivity.this, SHARE_MEDIA.TWITTER, umAuthListener);
                HttpUtil.getUtils().statistics("", "1014", "0", "0", "0", 0);
                break;
            case R.id.iv_login:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                if (et_account.getText().toString().length() == 0) {
                    AppUtils.showToast(getString(R.string.qingshuruzhanghao));
                } else if (et_pwd.getText().toString().length() == 0) {
                    AppUtils.showToast(getString(R.string.qingshurumima));
                } else if (!HttpUtil.isNetworkAvailable()) {
                    AppUtils.showToast(getString(R.string.net_nonetwork));
                } else {
                    Map<String, String> mss = new HashMap<>();
                    mss.put("email", et_account.getText().toString());
                    mss.put("password", et_pwd.getText().toString());
                    goLogin(API.NET_USER_LOGIN, mss);
                    DialogUtils.showProgressDialog(this, "");
                }
                HttpUtil.getUtils().statistics("", "1016", "0", "0", "0", 0);
                break;
            case R.id.tv_register:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                HttpUtil.getUtils().statistics("", "1017", "0", "0", "0", 0);
                break;
            case R.id.tv_lostpwd:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                startActivity(new Intent(LoginActivity.this, LostPWDActivity.class));
                HttpUtil.getUtils().statistics("", "1018", "0", "0", "0", 0);
                break;
        }
    }

    private View.OnClickListener vocl = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AdvBean.DataBean adb = (AdvBean.DataBean) v.getTag();
            MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
            if (!HttpUtil.isNetworkAvailable()) {
                AppUtils.showToast(LoginActivity.this.getString(R.string.net_warnnetwork));
                return;
            }
            Intent i = null;
            switch (adb.getResourceType()) {
                case "2":
                    //优酷视频，调用优酷视频播放器，并需要传递vid
                    String vid = adb.getUrl();
                    i = new Intent(LoginActivity.this, YKPlayerActivity.class);
                    i.putExtra("vid", vid);
                    StaticObj.YKAgle = Constant.YK_FLAGE_MORE;
                    break;
                case "4":
                    i = new Intent(LoginActivity.this, WEBViewActivity.class);
                    i.putExtra("url", adb.getUrl());
                    break;
            }
            startActivity(i);
            HttpUtil.getUtils().statistics(adb.getPosition() + "", "1006", "0", "0", "0", 0);
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(this);
        if (profileTracker != null)
            profileTracker.stopTracking();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(LoginActivity.this);
            mGoogleApiClient.disconnect();
        }

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, 93);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Map<String, String> mss = new HashMap<>();
            mss.put("userName", acct.getDisplayName());
            mss.put("userId", acct.getId());
            mss.put("platformName", "google");
            if (acct.getPhotoUrl() != null)
                mss.put("iconUrl", acct.getPhotoUrl().toString());
            goLogin(API.NET_USER_OTHERLOGIN, mss);
        } else {
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (StaticObj.getUid() != null) {
            finish();
        }
    }

    private void fBLogin(Profile profile) {
        Map<String, String> mss = new HashMap<>();
        mss.put("userName", profile.getName());
        mss.put("userId", profile.getId());
        mss.put("platformName", "facebook");
//        try {
//            String imgurl = URLEncoder.encode(profile.getProfilePictureUri(100, 100).toString(), "UTF-8");
//            mss.put("iconUrl", imgurl);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

        goLogin(API.NET_USER_OTHERLOGIN, mss);
    }
}
