package com.sinoangel.kids.mode_new.ks.core;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.alibaba.fastjson.JSON;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.adapter.RecommeAdapter;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.core.bean.ZanBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ZanActivity extends BaseActivity implements View.OnClickListener {

    private CheckBox cb;
    private ImageView iv_f_share, iv_f_comments, iv_f_back;
    public RecyclerView rv_list;

    private CoreDataBean.DataBean dat;
    private List<Cate.DataBean> lcdb;
    private Handler mh = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    cb.setEnabled(false);
                    break;
                case 1:
                    rv_list.setAdapter(new RecommeAdapter(ZanActivity.this, lcdb));
                    break;
            }
        }
    };

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra(Constant.ZAN_VH, true)) {
            setContentView(R.layout.item_zan_v);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);//竖屏
        } else {
            setContentView(R.layout.item_zan_h);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);//横屏
        }

        dat = (CoreDataBean.DataBean) getIntent().getExtras().get(Constant.DATA);

        cb = (CheckBox) findViewById(R.id.cb_zan);
        iv_f_share = (ImageView) findViewById(R.id.iv_f_share);
        iv_f_comments = (ImageView) findViewById(R.id.iv_f_comments);
        iv_f_back = (ImageView) findViewById(R.id.iv_f_back);
        rv_list = (RecyclerView) findViewById(R.id.rv_f_list);

        rv_list.setLayoutManager(new GridLayoutManager(this, 3));

        cb.setOnClickListener(this);
        iv_f_share.setOnClickListener(this);
        iv_f_comments.setOnClickListener(this);
        iv_f_back.setOnClickListener(this);

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    cb.setEnabled(false);
                }
            }
        });

        getIspointLikeChat(dat.getAppId());

        getProductreCommend(dat.getAppId());

    }

    /**
     * 获取推荐
     */
    public void getProductreCommend(String id) {

        String url = API.getProductreCommend(id);
        HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    Cate cate = JSON.parseObject(json, Cate.class);
                    lcdb = cate.getData();
                    mh.sendEmptyMessage(1);
                } catch (Exception e) {
                }
            }
        });
    }


    public void getIspointLikeChat(String id) {
        Map<String, String> mss = new HashMap<>();
        if (StaticObj.getUid() != null)
            mss.put("userid", StaticObj.getUid().getUserId() + "");
        mss.put("deviceToken", SPUtils.getPhoneId());
        mss.put("singleId", id);
        mss.put("seriesId", StaticObj.getCurrCata().getAppId());
        mss.put("carrierId", StaticObj.getCategory_id());
        HttpUtil.getUtils().getJsonStringByPost(API.NET_USER_ISPOINTLIKECHAT, mss, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {

            }

            @Override
            public void onNetSucceed(String json) {
                try {
                    final ZanBean zb = JSON.parseObject(json, ZanBean.class);
                    if (zb.getFlag() == 1) {
                        if (zb.getData().getIsLike() == 1) {
                            mh.sendEmptyMessage(0);
                        }
                    }
                } catch (Exception e) {

                }

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_f_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_f_share:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);

                String name = "0".equals(StaticObj.getCurrCata().getAppId()) ? "" : "---" + StaticObj.getCurrCata().getAppName();

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.app_name) + name + " " + API.getShare(dat.getAppId()));
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "QQ"));

                break;
            case R.id.iv_f_comments:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                startActivity(new Intent(ZanActivity.this, CommentsActivity.class).putExtra("data", dat));
                break;
            case R.id.cb_zan:
                MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                Map<String, String> mss = new HashMap<>();
                if (StaticObj.getUid() != null)
                    mss.put("userId", StaticObj.getUid().getId());
                mss.put("deviceToken", SPUtils.getPhoneId());
                mss.put("singleId", dat.getAppId());
                mss.put("seriesId", StaticObj.getCurrCata().getAppId());
                mss.put("carrierId", StaticObj.getCategory_id());
                mss.put("isLike", "1");
                HttpUtil.getUtils().getJsonStringByPost(API.NET_USER_POINTLIKECHAT, mss, null);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mh.removeCallbacksAndMessages(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

    }
}
