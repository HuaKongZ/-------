package com.sinoangel.kids.mode_new.ks.function.song;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.io.File;
import java.util.List;

/**
 * 儿歌本音乐的适配器
 *
 * @author Administrator
 */
public class SongRVAdapter extends RecyclerView.Adapter<SongRVAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    private SongActivity activity;
    //歌曲信息对象的集合
    private List<CoreDataBean.DataBean> baseAppInfoList;
    //歌曲的保存路径
    // 变量说明：用于判断数据是否为新增的sp文件
//    public static SharedPreferences judgeNewSp;
//    public static SharedPreferences.Editor judgeNewEditor;

    //需要歌曲的名字、ic_launcher、时长
    public SongRVAdapter(SongActivity mContext, List<CoreDataBean.DataBean> dataList) {
        this.activity = mContext;
        this.baseAppInfoList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RelativeLayout rl = (RelativeLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_songlist, null);
        ViewHolder vh = new ViewHolder(rl);
        vh.iv_icon = (ImageView) rl.findViewById(R.id.iv_icon);
        vh.tv_title = (TextView) rl.findViewById(R.id.tv_title);
        vh.tv_time = (TextView) rl.findViewById(R.id.tv_time);
        vh.pb_music = (ProgressBar) rl.findViewById(R.id.pb_music);
        rl.setOnClickListener(this);
        rl.setOnLongClickListener(this);
        return vh;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        CoreDataBean.DataBean data = baseAppInfoList.get(position);
        data.getDd().setPosation(position);
        viewHolder.rl.setTag(data);


        if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FINISH) {
            //下载完成的
            if (StaticObj.dataIng != null && StaticObj.dataIng.getAppId() == data.getAppId()) {
                //当前正在播放
                viewHolder.tv_title.setTextColor(activity.getResources().getColor(R.color.font_white));
                viewHolder.tv_time.setTextColor(activity.getResources().getColor(R.color.font_white));
                if (activity.isPlaying()) {
                    viewHolder.iv_icon.setImageResource(R.mipmap.music_icon_puase_w);
                    viewHolder.tv_title.setSelected(true);
                } else {
                    viewHolder.iv_icon.setImageResource(R.mipmap.music_playing);
                    viewHolder.tv_title.setSelected(true);
                }
            } else {
                //存在没播放
                viewHolder.tv_title.setTextColor(activity.getResources().getColor(R.color.font_brown));
                viewHolder.tv_time.setTextColor(activity.getResources().getColor(R.color.font_brown));
                viewHolder.iv_icon.setImageResource(R.mipmap.music_icon_paly);
                viewHolder.tv_title.setSelected(false);
            }
            viewHolder.pb_music.setProgress(100);
        } else {

            for (CoreDataBean.DataBean dat : StaticObj.ld) {
                if (data.getAppId().equals(dat.getAppId())) {
                    dat.getDd().setPosation(position);//位置纠正
                    baseAppInfoList.set(position, dat);
                }
            }

            if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
                //下载中的
                viewHolder.iv_icon.setImageResource(R.mipmap.music_icon_dowing);
                viewHolder.pb_music.setProgress(data.getDd().getDownSize());
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
                viewHolder.iv_icon.setImageResource(R.mipmap.animation_wait_b);
                viewHolder.pb_music.setProgress(0);
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE) {
                viewHolder.iv_icon.setImageResource(R.mipmap.music_icon_puase);
                viewHolder.pb_music.setProgress(data.getDd().getDownSize());
            } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FAIL) {
                viewHolder.iv_icon.setImageResource(R.mipmap.animation_play_faild_b);
                viewHolder.pb_music.setProgress(data.getDd().getDownSize());
            } else {
                viewHolder.iv_icon.setImageResource(R.mipmap.music_icon_nodow);
                viewHolder.pb_music.setProgress(0);

            }
            viewHolder.tv_title.setTextColor(activity.getResources().getColor(R.color.font_brown));
            viewHolder.tv_time.setTextColor(activity.getResources().getColor(R.color.font_brown));

        }

        viewHolder.tv_title.setText(data.getAppName());
        viewHolder.tv_time.setText(data.getVersionCode());

    }

    @Override
    public int getItemCount() {
        return baseAppInfoList == null ? 0 : baseAppInfoList.size();
    }

    @Override
    public void onClick(View v) {
        CoreDataBean.DataBean data = (CoreDataBean.DataBean) v.getTag();

        MusicUtils.getMusicUtils().playSound(R.raw.connect_15);

        if (data.isFileExist()) {
            //存在就播放
            activity.reStartPlay(data);
            HttpUtil.getUtils().statistics(data.getAppId(),StaticObj.getCategory_id(), "0", "1", "0", 0);
        } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
            DownManagerUtil.getInstance().stopDownload(data);
            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_PUASE);
            try {
                MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState");
            } catch (DbException e) {
                e.printStackTrace();
            }

            notifyItemChanged(data.getDd().getPosation());
        } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
            DownManagerUtil.getInstance().stopDownload(data);
            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_NO);
            try {
                MyApplication.getInstance().getDbUtisl().update(data.getDd(), "downState");
            } catch (DbException e) {
                e.printStackTrace();
            }
            notifyItemChanged(data.getDd().getPosation());
        } else {
            //下载
            DownManagerUtil.getInstance().downFile(data);
            HttpUtil.getUtils().statistics(data.getAppId(),StaticObj.getCategory_id(), "0", "0", "1", 0);
        }

    }

    @Override
    public boolean onLongClick(View v) {
        final CoreDataBean.DataBean data = (CoreDataBean.DataBean) v.getTag();
        final File file = new File(DownManagerUtil.getInstance().getSavePath() + data.getAppId());
        if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FINISH || data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE) {

            DialogUtils.YesOrNoDialog(activity,  R.mipmap.text_delete, new DialogUtils.YeOrOnListener() {
                @Override
                public void onOk() {
                    if (DownManagerUtil.deleteAllFile(file)) {
                        try {
                            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_NO);
                            data.getDd().setDownSize(0);
                            MyApplication.getInstance().getDbUtisl().delete(data.getDd());
                            MyApplication.getInstance().getDbUtisl().delete(data);
                        } catch (DbException e) {
                            e.printStackTrace();
                        }

                        if (StaticObj.dataIng != null && StaticObj.dataIng.getAppId() == data.getAppId() && activity.isPlaying()) {
                            activity.stopPlay();
                        }
                        AppUtils.showToast(activity.getString(R.string.del_ok));
                        MusicUtils.getMusicUtils().playSound(R.raw.removing_creatures_8);
                        notifyItemChanged(data.getDd().getPosation());
                    } else {
                        AppUtils.showToast(activity.getString(R.string.del_no));
                    }
                }

                @Override
                public void onNo() {

                }
            });

        }
        return true;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rl;
        private ImageView iv_icon;
        private TextView tv_title, tv_time;
        private ProgressBar pb_music;

        public ViewHolder(View itemView) {
            super(itemView);
            rl = (RelativeLayout) itemView;
        }
    }

    public void fastItem() {
        CoreDataBean.DataBean nextD = baseAppInfoList.get(0);

        View v = new View(activity);
        v.setTag(nextD);
        onClick(v);
    }

}

