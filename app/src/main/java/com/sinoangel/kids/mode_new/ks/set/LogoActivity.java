package com.sinoangel.kids.mode_new.ks.set;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.MainActivity;
import com.sinoangel.kids.mode_new.ks.util.LocationUtil;

/**
 * logo页
 *
 * @author XRB
 */
public class LogoActivity extends BaseActivity implements OnCompletionListener {

    private SurfaceView sv;//播放视频的位置
    private MediaPlayer player;//播放器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        sv = (SurfaceView) findViewById(R.id.sv_media);

        try {
            player = new MediaPlayer();
//            读取raw中的视频文件
            AssetFileDescriptor afd = this.getResources().openRawResourceFd(R.raw.logodonghua);
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            player.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //添加surfaceview的回调
        sv.getHolder().addCallback(new SurfaceHolder.Callback() {
            //surfaceview销毁的时候
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }

            //surfaceview完成创建的时候
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                player.setDisplay(holder);
                player.start();
            }

            //surfaceview改变的时候
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                       int height) {

            }
        });
        player.setOnCompletionListener(this);

        LocationUtil.getLocation();
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (player.isPlaying()) {
            player.stop();
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (player.isPlaying()) {
            player.stop();
        }
        player.release();
        player = null;

    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
