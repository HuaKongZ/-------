package com.sinoangel.kids.mode_new.ks.function.book;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.ZanActivity;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.function.book.bean.BookBean;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

//import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * 类说明：绘本屋的播放器 六岁以上 编写日期: 2016-2-2 作者: 闻铭 </pre>
 */
public class PaintingToolActivitys extends BaseActivity implements
        OnClickListener {
    // 变量说明：返回按钮 和背景图 , 显示的图片
    private ImageView back;
    // 变量说明： 控制播放音频的自增长下标
    private int count;
    // 变量说明：储存json的集合
    private List<BookBean.DataBean> comics;
    // 变量说明：绘本名字，不带后缀
    private CoreDataBean.DataBean dat;
    // 变量说明：判断漫画是横屏还是竖屏
    private String screen;
    //gb
    // 变量说明：页数显示
    private TextView tv_datainfo;
    // 变量说明：滚动控件
    // 变量说明：横屏动画
    private PhotoView iv_landscape;

    private long userTime;//使用时长

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 保持屏幕常亮
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_painting_tools);
        Intent intent = getIntent();

        dat = (CoreDataBean.DataBean) intent.getExtras().get("cdbbean");

        init();

        addData((BookBean) intent.getExtras().get("bbean"));

        userTime = new Date().getTime();
    }

    /**
     * 根据横竖屏 选择放入数据
     */
    private void addData(BookBean bb) {
        screen = bb.getScreen();
        comics = bb.getData();


        if ("landscape".equals(screen)) {//单图
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);//横屏

            ImageUtils.showImgFile(dat.getUnZipPath() + comics.get(count).getBackground(), iv_landscape, false);
        } else {
            //一页或两页
            Configuration newConfig = getResources().getConfiguration();
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                Bitmap bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + comics.get(count).getBackground(), dat.getUnZipPath() + comics.get(++count).getBackground());
                iv_landscape.setImageBitmap(bitmap);
                //横屏
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                //竖屏
                ImageUtils.showImgFile(dat.getUnZipPath() + comics.get(count).getBackground(), iv_landscape, true);
            }

        }

        if (tv_datainfo != null && comics != null) {
            if (dat.getAppName() != null)
                tv_datainfo.setText(dat.getAppName() + "   " + (count + 1) + "/" + comics.size());
            else
                tv_datainfo.setText(dat.getCateEn() + "   " + (count + 1) + "/" + comics.size());
        }
    }

    /**
     * 方法说明：初始化所有控件
     */

    private void init() {
        comics = new ArrayList<>();
        back = (ImageView) findViewById(R.id.iv_back);
        iv_landscape = (PhotoView) findViewById(R.id.iv_landscape);
        tv_datainfo = (TextView) findViewById(R.id.tv_datainfo);

        back.setOnClickListener(this);

        iv_landscape.setOnSingleFlingListener(new SingleFlingListener());
        iv_landscape.setOnPhotoTapListener(new OnSingleFlingListener());
    }

    private class OnSingleFlingListener implements PhotoViewAttacher.OnPhotoTapListener {

        @Override
        public void onPhotoTap(View view, float x, float y) {
            if (back.getVisibility() == View.GONE) {
                back.setVisibility(View.VISIBLE);
            } else {
                back.setVisibility(View.GONE);
            }
        }

        @Override
        public void onOutsidePhotoTap() {

        }
    }

    private class SingleFlingListener implements PhotoViewAttacher.OnSingleFlingListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (velocityX < 0) {   //左滑
                if (Math.abs(velocityX) > Math.abs(velocityY)) {
                    //显示下一张照片
                    changePage(true);
                }

            } else if (velocityX > 0) {   //右滑
                if (Math.abs(velocityX) > Math.abs(velocityY)) {
                    //显示上一张照片
                    changePage(false);
                }
            }
            return true;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        MusicUtils.getMusicUtils().playOrPauseBgsc(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * 方法说明：按钮的点击事件
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_f_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
        }
        System.exit(0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DownManagerUtil.deleteAllFile(new File(dat.getUnZipPath()));
        HttpUtil.getUtils().statistics(dat.getAppId(), StaticObj.getCategory_id(), "1", "1", "0", (new Date().getTime() - userTime));
        System.exit(0);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!"landscape".equals(screen)) {
            if (newConfig.orientation == this.getResources().getConfiguration().ORIENTATION_PORTRAIT) {
                //切换为竖屏
                if (count > 0 && count % 2 == 1)
                    count--;
                ImageUtils.showImgFile(dat.getUnZipPath() + comics.get(count).getBackground(), iv_landscape, true);
            } else if (newConfig.orientation == this.getResources().getConfiguration().ORIENTATION_LANDSCAPE) {
                //切换为横屏
                if (count % 2 == 1)
                    count--;
                Bitmap bitmap;
                if (count < comics.size() - 2) {
                    bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + comics.get(count).getBackground(), dat.getUnZipPath() + comics.get(++count).getBackground());
                } else {
                    bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + comics.get(count).getBackground());
                }
                iv_landscape.setImageBitmap(bitmap);
            }
        }
        if (tv_datainfo != null && comics != null) {
            if (dat.getAppName().length() != 0)
                tv_datainfo.setText(dat.getAppName() + "   " + (count + 1) + "/" + comics.size());
            else
                tv_datainfo.setText(dat.getCateEn() + "   " + (count + 1) + "/" + comics.size());
        }

    }

    /**
     * flage true 下一页 false 上一页
     */

    private void changePage(boolean flage) {

        if (flage) {
            //下一页
            if (count >= comics.size() - 1) {

                if (getResources().getConfiguration().orientation == getResources().getConfiguration().ORIENTATION_PORTRAIT) {
                    startActivity(new Intent(PaintingToolActivitys.this, ZanActivity.class).putExtra(Constant.ZAN_VH, true).putExtra(Constant.DATA, dat));
                } else {
                    startActivity(new Intent(PaintingToolActivitys.this, ZanActivity.class).putExtra(Constant.ZAN_VH, false).putExtra(Constant.DATA, dat));
                }
                finish();
                System.exit(0);
                return;
            }

            if (!"landscape".equals(screen)) {//单双页模式
                if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    Bitmap bitmap;
                    if (count < comics.size() - 2) {
                        bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + comics.get(++count).getBackground(), dat.getUnZipPath() + comics.get(++count).getBackground());
                        iv_landscape.setImageBitmap(bitmap);
                    } else {
                        bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + comics.get(++count).getBackground());
                    }
                    iv_landscape.setImageBitmap(bitmap);
                } else {
                    ImageUtils.showImgFile(dat.getUnZipPath() + comics.get(++count).getBackground(), iv_landscape, true);
                }
            } else {
                ImageUtils.showImgFile(dat.getUnZipPath() + comics.get(++count).getBackground(), iv_landscape, false);
            }
        } else {
            //上一页
            if (count <= 0)
                return;
            if (!"landscape".equals(screen)) {//单双页模式
                if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    if (count >= 2) {
                        if (count % 2 == 0)
                            count++;
                        count -= 2;
                        Bitmap bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + comics.get(count - 1).getBackground(), dat.getUnZipPath() + comics.get(count).getBackground());
                        iv_landscape.setImageBitmap(bitmap);
                    }
                } else {
                    ImageUtils.showImgFile(dat.getUnZipPath() + comics.get(--count).getBackground(), iv_landscape, true);
                }
            } else {
                ImageUtils.showImgFile(dat.getUnZipPath() + comics.get(--count).getBackground(), iv_landscape, flage);
            }
        }
        String str;
        if (dat.getAppName() != null)
            str = dat.getAppName() + "   " + (count + 1) + "/" + comics.size();
        else
            str = dat.getCateEn() + "   " + (count + 1) + "/" + comics.size();
        tv_datainfo.setText(str);
    }


}
