package com.sinoangel.kids.mode_new.ks.function.book;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.ZanActivity;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.function.book.bean.BookBean;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class PaintingToolActivity extends BaseActivity implements
        OnClickListener {

    // 变量说明 上一页，播放，下一页
    private Button up, play, down;
    // 变量说明：返回按钮 字幕和背景图
    private ImageView back, iv_text;
    private PhotoView pv_img;
    // 变量说明： 播放控制布局
    private LinearLayout playrl;
    // 音乐播放类
    private MediaPlayer mp;
    // 变量说明：储存json的集合
    private List<BookBean.DataBean> lbb;
    // 变量说明： 控制播放音频的自增长下标
    private int count;
    // 变量说明： 显示布局的控制开关
    private boolean show = true;
    // 变量说明： 控制播放进度的开关
    private Boolean isPause = false;//true是按了，false是没按
    // 变量说明：判断漫画是横屏还是竖屏
    private String screen;

    private TextView tv_datainfo;
    private CoreDataBean.DataBean dat;

    private long userTime;//使用时长

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case 0:
                        ImageUtils.showImgFile(dat.getUnZipPath()
                                + lbb.get(count).getSubTitle2(), iv_text);
                        break;
                    case 1:
                        // 设置要播放的文件
                        Play_BorF(true);
                        break;
                }
            } catch (Exception e) {

            }
        }
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 保持屏幕常亮
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_painting_tool);

        dat = (CoreDataBean.DataBean) getIntent().getExtras().get("cdbbean");
        init();

        getJson((BookBean) getIntent().getExtras().get("bbean"));

        playbgMusic();

        tv_datainfo.setText(dat.getAppName() + " " + (count + 1) + "/" + lbb.size());
        userTime = new Date().getTime();

    }

    protected void onStart() {
        super.onStart();
        if (!isPause && mp != null) {
            mp.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MusicUtils.getMusicUtils().playOrPauseBgsc(false);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    protected void onStop() {
        super.onStop();
        if (mp != null) {
            mp.stop();
            handler.removeCallbacksAndMessages(this);
        }

    }

    protected void onDestroy() {
        super.onDestroy();
        if (mp != null) {
            mp.release();
            mp = null;
        }
        DownManagerUtil.deleteAllFile(new File(dat.getUnZipPath()));
        HttpUtil.getUtils().statistics(dat.getAppId(), StaticObj.getCategory_id(), "1", "1", "0", (new Date().getTime() - userTime));
        System.exit(0);
    }

    /**
     * 方法说明：初始化所有控件
     */
    private void init() {
        up = (Button) findViewById(R.id.painting_tool_top);
        down = (Button) findViewById(R.id.painting_tool_bottom);
        play = (Button) findViewById(R.id.painting_tool_play);
        pv_img = (PhotoView) findViewById(R.id.pv_img);//背景
        back = (ImageView) findViewById(R.id.iv_back);
        playrl = (LinearLayout) findViewById(R.id.painting_tool_ll);
        iv_text = (ImageView) findViewById(R.id.iv_text);//字幕
        tv_datainfo = (TextView) findViewById(R.id.tv_datainfo);


        back.setOnClickListener(this);
        down.setOnClickListener(this);
        play.setOnClickListener(this);

        up.setOnClickListener(this);
        pv_img.setOnClickListener(this);

        playrl.setVisibility(View.GONE);

        mp = new MediaPlayer();

        pv_img.setOnSingleFlingListener(new PhotoViewAttacher.OnSingleFlingListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                if (velocityX < 0) {
                    if (Math.abs(velocityX) > Math.abs(velocityY)) {
                        //显示下一张照片
                        if (!"landscape".equals(screen))
                            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                                if (count + 2 <= lbb.size() - 1 && count % 2 == 0) {
                                    count++;
                                }
                        Play_BorF(true);
                    }

                } else if (velocityX > 0) {
                    if (Math.abs(velocityX) > Math.abs(velocityY)) {
                        //显示上一张照片
                        if (!"landscape".equals(screen))
                            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                                if (count > 1 && count % 2 == 1) {
                                    count -= 2;
                                } else {
                                    count--;
                                }
                        Play_BorF(false);
                    }
                }
                return true;
            }
        });

        pv_img.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float x, float y) {
                if (show == true) {
                    playrl.setVisibility(View.VISIBLE);
                    back.setVisibility(View.VISIBLE);
                    show = false;
                } else {
                    playrl.setVisibility(View.GONE);
                    back.setVisibility(View.GONE);
                    show = true;
                }
            }

            @Override
            public void onOutsidePhotoTap() {

            }
        });

        pv_img.setOnScaleChangeListener(new PhotoViewAttacher.OnScaleChangeListener() {
            @Override
            public void onScaleChange(float scaleFactor, float focusX, float focusY) {

            }
        });

    }

    /**
     * 方法说明：所有按钮的点击事件
     */
    public void onClick(View v) {
        switch (v.getId()) {
            // 点击返回
            case R.id.iv_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            // 点击上一页
            case R.id.painting_tool_top:
                if (!"landscape".equals(screen))
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                        if (count > 1 && count % 2 == 1) {
                            count -= 2;
                        } else {
                            count--;
                        }
                Play_BorF(false);
                break;
            // 点击下一页
            case R.id.painting_tool_bottom:
                if (!"landscape".equals(screen))
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                        if (count + 2 <= lbb.size() - 1 && count % 2 == 0) {
                            count++;
                        }
                Play_BorF(true);
                break;
            // 点击播放
            case R.id.painting_tool_play:
                //添加if-else
                if (mp != null && mp.isPlaying()) {
                    mp.pause();
                    isPause = true;
                    play.setBackgroundResource(R.mipmap.painting_tool_play);
                } else {
                    if (mp != null) {
                        mp.start();
                        isPause = false;
                        play.setBackgroundResource(R.mipmap.painting_tool_puase);
                    }
                }
                break;
        }
    }

    /**
     * 方法说明：播放路径下的音频
     */
    private void playbgMusic() {

        onConfigurationChanged(getResources().getConfiguration());

        File file = new File(dat.getUnZipPath() + lbb.get(count).getMp3File());
        try {
            // 文件是否存在
            if (file.exists()) {
                // 设置播放文件的路径
                mp.reset();
                mp.setDataSource(file.getAbsolutePath());
                mp.prepare();
                mp.start();
                play.setBackgroundResource(R.mipmap.painting_tool_puase);

                long time = Long.parseLong(lbb.get(count).getSecond());
                //后加
                handler.sendEmptyMessageDelayed(0, time);
            }
        } catch (Exception e) {
            finish();
        }

        // 对播放进行监听
        mp.setOnCompletionListener(new OnCompletionListener() {

            public void onCompletion(MediaPlayer mp) {

                if (count >= lbb.size() - 1) {

                    if (getResources().getConfiguration().orientation == getResources().getConfiguration().ORIENTATION_PORTRAIT) {
                        startActivity(new Intent(PaintingToolActivity.this, ZanActivity.class).putExtra(Constant.ZAN_VH, true).putExtra(Constant.DATA, dat));
                    } else {
                        startActivity(new Intent(PaintingToolActivity.this, ZanActivity.class).putExtra(Constant.ZAN_VH, false).putExtra(Constant.DATA, dat));
                    }
                    finish();
                } else {
                    int dur = mp.getDuration() / 1000;
                    if (dur >= 5)
                        handler.sendEmptyMessageDelayed(1, 0);
                    else
                        handler.sendEmptyMessageDelayed(1, (5 - dur) * 1000);
                }
            }
        });
        tv_datainfo.setText(dat.getAppName() + " " + (count + 1) + "/" + lbb.size());
    }

    /**
     * 方法说明：播放到上一个或者下一个
     */
    private void Play_BorF(boolean flage) {
        if (flage) {
            if (count >= lbb.size() - 1) {
                if (getResources().getConfiguration().orientation == getResources().getConfiguration().ORIENTATION_PORTRAIT) {
                    startActivity(new Intent(PaintingToolActivity.this, ZanActivity.class).putExtra(Constant.ZAN_VH, true).putExtra(Constant.DATA, dat));
                } else {
                    startActivity(new Intent(PaintingToolActivity.this, ZanActivity.class).putExtra(Constant.ZAN_VH, false).putExtra(Constant.DATA, dat));
                }
                finish();
            } else {
                count++;
                playbgMusic();
            }
        } else {
            if (count <= 0) {
            } else {
                count--;
                playbgMusic();
            }
        }
    }

    /**
     * 方法说明：获取到路径下的图片
     */
    /**
     * 方法说明：解析得到txt里面的内容
     */
    public void getJson(BookBean bb) {
        lbb = bb.getData();
        screen = bb.getScreen();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (!"landscape".equals(screen)) {
            if (newConfig.orientation == this.getResources().getConfiguration().ORIENTATION_PORTRAIT) {
                //切换为竖屏
                ImageUtils.showImgFile(dat.getUnZipPath() + lbb.get(count).getBackground(), pv_img, true);
            } else if (newConfig.orientation == this.getResources().getConfiguration().ORIENTATION_LANDSCAPE) {
                //切换为横屏
                Bitmap bitmap;
                if (count % 2 == 0) {
                    if (count < lbb.size() - 1) {
                        bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + lbb.get(count).getBackground(), dat.getUnZipPath() + lbb.get(count + 1).getBackground());
                    } else {
                        bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + lbb.get(count).getBackground());
                    }
                } else {
                    if (count < lbb.size()) {
                        bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + lbb.get(count - 1).getBackground(), dat.getUnZipPath() + lbb.get(count).getBackground());
                    } else {
                        bitmap = new ImageUtils().toConformBitmap(dat.getUnZipPath() + lbb.get(count).getBackground());
                    }
                }
                pv_img.setImageBitmap(bitmap);
            }
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);//横屏
            ImageUtils.showImgFile(dat.getUnZipPath() + lbb.get(count).getBackground(), pv_img, false);
        }
        if (tv_datainfo != null && lbb != null) {
            tv_datainfo.setText(dat.getAppName() + "   " + (count + 1) + "/" + lbb.size());
        }

    }

}
