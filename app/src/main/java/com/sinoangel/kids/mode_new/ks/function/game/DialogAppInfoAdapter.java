package com.sinoangel.kids.mode_new.ks.function.game;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;

import java.util.List;

/**
 * Created by Administrator on 2016/6/22.
 */
public class DialogAppInfoAdapter extends RecyclerView.Adapter<DialogAppInfoAdapter.ViewHolder> {


    // 数据集
    List<CoreDataBean.ImagesBean> ldii;

    String picShowType;

    public DialogAppInfoAdapter(List<CoreDataBean.ImagesBean> imgs, String picShowType) {
        super();
        ldii = imgs;
        this.picShowType = picShowType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ImageView view = new ImageView(viewGroup.getContext());
        view.setScaleType(ImageView.ScaleType.FIT_XY);
        RecyclerView.LayoutParams lp;
        int imgW = MyApplication.getInstance().getWei() / 3;
        int imgH = MyApplication.getInstance().getHei() / 3;
        if ("1".equals(picShowType)) {
            // 横图展示
            lp = new RecyclerView.LayoutParams(imgH, ViewGroup.LayoutParams.MATCH_PARENT);
        } else {
            // 纵图展示
            lp = new RecyclerView.LayoutParams(imgW, imgH);
        }
//        else {
//            imgParams = new LinearLayout.LayoutParams(imgW, imgH);
//        }
        lp.setMargins(20, 0, 0, 0);
        view.setLayoutParams(lp);
        // 创建一个ViewHolder
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        ImageUtils.showImgUrl(ldii.get(i).getUrl(), viewHolder.mIv);
    }

    @Override
    public int getItemCount() {
        return ldii == null ? 0 : ldii.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView mIv;

        public ViewHolder(View itemView) {
            super(itemView);
            mIv = (ImageView) itemView;
        }
    }

}
