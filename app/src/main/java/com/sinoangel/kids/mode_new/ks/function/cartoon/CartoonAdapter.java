package com.sinoangel.kids.mode_new.ks.function.cartoon;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.sinoangel.kids.mode_new.ks.widget.YKPlayerActivity;

import java.io.File;
import java.util.List;

public class CartoonAdapter extends RecyclerView.Adapter<CartoonAdapter.ViewItem> implements View.OnClickListener, View.OnLongClickListener {

    private CartoonActivity mContext;
    private List<CoreDataBean.DataBean> baseAppInfoList;

    public CartoonAdapter(CartoonActivity mContext, List<CoreDataBean.DataBean> baseAppInfoList) {
        super();
        this.mContext = mContext;
        this.baseAppInfoList = baseAppInfoList;
    }

    @Override
    public ViewItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View fl = LayoutInflater.from(mContext).inflate(R.layout.item_cartoon, null);
        View card = fl.findViewById(R.id.cartoon_item_kuang);
        int wei = (MyApplication.getInstance().getHei() / 4);
        int hei = (int) (wei * 25d / 44d);
        card.setLayoutParams(new RelativeLayout.LayoutParams(wei, hei));

        ViewItem item = new ViewItem(fl);

        return item;
    }

    @Override
    public void onBindViewHolder(ViewItem item, int position) {
        if (baseAppInfoList.get(position).getAppName() != null && baseAppInfoList.get(position).getAppName().length() > 0) {
            item.tv_title.setText(baseAppInfoList.get(position).getAppName());
        }

        CoreDataBean.DataBean data = baseAppInfoList.get(position);

        if (!"1".equals(data.getSourceId())) {
            item.btn.setImageResource(R.mipmap.onlineplay);
        } else {

            if (!data.isFileExist()) {
                for (CoreDataBean.DataBean dat : StaticObj.ld) {
                    if (data.getAppId().equals(dat.getAppId())) {
                        baseAppInfoList.set(position, dat);
                    }
                }
                item.iv_black_bg.setBackgroundResource(R.drawable.bk_round);
                if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
                    // 这是在下载当中
                    item.imagecover.setProgress(data.getDd().getDownSize());
                    item.btn.setImageResource(R.mipmap.animation_play_press);
                } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
                    item.btn.setImageResource(R.mipmap.animation_wait);
                    item.imagecover.setProgress(0);
                } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE) {
                    item.btn.setImageResource(R.mipmap.animation_play_puase);
                    item.imagecover.setProgress(0);
                } else if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FAIL) {
                    item.btn.setImageResource(R.mipmap.animation_play_faild);
                    item.imagecover.setProgress(0);
                } else {
                    item.btn.setImageResource(R.mipmap.app_download);
                    item.imagecover.setProgress(0);
                }
                item.imagecover.setVisibility(View.VISIBLE);//可见
                item.btn.setVisibility(View.VISIBLE);
            } else {
                item.btn.setImageDrawable(null);
                item.iv_black_bg.setBackground(null);
                item.imagecover.setVisibility(View.INVISIBLE);
            }

        }

        if (data.getDd().iseed()) {
            item.item_isee.setVisibility(View.VISIBLE);
            item.btn.setImageDrawable(null);
            item.iv_black_bg.setBackgroundResource(R.drawable.bk_round_wr);
        } else {
            item.item_isee.setVisibility(View.GONE);
            item.iv_black_bg.setBackground(null);
        }

        ImageUtils.showImgUrl(data.getIcon(), item.imageapp);
        if ("1".equals(data.getIsNew())) {
            if (SPUtils.getNew(data.getAppId())) {
                item.iv_isnew.setVisibility(View.VISIBLE);
            } else {
                item.iv_isnew.setVisibility(View.GONE);
            }
        } else {
            item.iv_isnew.setVisibility(View.GONE);
        }
        data.getDd().setPosation(position);
        item.rl_box.setTag(data);
        item.rl_box.setOnClickListener(this);
        item.rl_box.setOnLongClickListener(this);

    }

    @Override
    public int getItemCount() {
        return baseAppInfoList == null ? 0 : baseAppInfoList.size();
    }

    @Override
    public void onClick(View v) {

        final CoreDataBean.DataBean mData = (CoreDataBean.DataBean) v.getTag();

        SPUtils.putNew(mData.getAppId());
        MusicUtils.getMusicUtils().playSound(R.raw.connect_15);

        if (mData.getDd().getPosation() != baseAppInfoList.size() - 1) {
            StaticObj.isEnd = true;
        } else {
            StaticObj.isEnd = false;
        }


        StaticObj.dataIng = mData;
        if ("1".equals(mData.getSourceId())) {
            //原视频
            if (mData.isFileExist()) {

                Intent intent = new Intent(mContext, AnimationActivity.class);
                mContext.startActivity(intent);

                mData.getDd().setIseed(true);

                try {
                    MyApplication.getInstance().getDbUtisl().saveOrUpdate(mData.getDd());
                } catch (DbException e) {
                    e.printStackTrace();
                    MyApplication.getInstance().updateDD();
                }

                return;
            } else if (mData.getDd().getDownState() == DownManagerUtil.DOWNLOAD_ING) {
                DownManagerUtil.getInstance().stopDownload(mData);
                mData.getDd().setDownState(DownManagerUtil.DOWNLOAD_PUASE);
                try {
                    MyApplication.getInstance().getDbUtisl().update(mData.getDd(), "downState");
                } catch (DbException e) {
                    e.printStackTrace();
                }
                notifyItemChanged(mData.getDd().getPosation());
            } else if (mData.getDd().getDownState() == DownManagerUtil.DOWNLOAD_WAIT) {
                DownManagerUtil.getInstance().stopDownload(mData);
                mData.getDd().setDownState(DownManagerUtil.DOWNLOAD_NO);
                try {
                    MyApplication.getInstance().getDbUtisl().update(mData.getDd(), "downState");
                } catch (DbException e) {
                    e.printStackTrace();
                }
                notifyItemChanged(mData.getDd().getPosation());
            } else {
                DownManagerUtil.getInstance().downFile(mData);
            }

        } else if ("2".equals(mData.getSourceId())) {

            if (!HttpUtil.isNetworkAvailable()) {
                AppUtils.showToast(mContext.getString(R.string.net_warnnetwork));
                return;
            }
            //优酷视频，调用优酷视频播放器，并需要传递vid
            String vid = mData.getDownUrl();
            Intent i = new Intent(mContext, YKPlayerActivity.class);
            i.putExtra("vid", vid);
            i.putExtra("appid", mData.getAppId());
            mContext.startActivity(i);
            StaticObj.YKAgle = Constant.YK_FLAGE_ZAN;
            Log.e("cartoon", "sourceId:" + mData.getDownUrl());


            mData.getDd().setIseed(true);

            try {
                MyApplication.getInstance().getDbUtisl().saveOrUpdate(mData.getDd());
            } catch (DbException e) {
                e.printStackTrace();
                MyApplication.getInstance().updateDD();
            }

        }
//        else if ("3".equals(mData.getSourceId())) {
//            //爱奇艺视频
//            if (!HttpUtil.isNetworkAvailable()) {
//                AppUtils.showToast(mContext.getString(R.string.net_warnnetwork));
//                return;
//            }
//
//            HttpUtil.getUtils().getJsonString(mData.getDownUrl(), new HttpUtil.OnNetResponseListener() {
//                @Override
//                public void onNetFail() {
//
//                }
//
//                @Override
//                public void onNetSucceed(String json) {
//                    // 将json解析的数据赋值给集合
//                    getAiQiYi(json);
//                    Intent i = new Intent(mContext, WEBViewActivity.class);
//                    i.putExtra("url", aqyData.getVideoUrl());
//                    mContext.startActivity(i);
////                    mContext.goWebView(aqyData.getVideoUrl());
//
//                    mData.getDd().setIseed(true);
//
//                    try {
//                        MyApplication.getInstance().getDbUtisl().saveOrUpdate(mData.getDd());
//                    } catch (DbException e) {
//                        e.printStackTrace();
//                        MyApplication.getInstance().updateDD();
//                    }
//                }
//            });
//
//        }
        else if ("4".equals(mData.getSourceId())) {//YouTube视频
            if (!HttpUtil.isNetworkAvailable()) {
                AppUtils.showToast(mContext.getString(R.string.net_warnnetwork));
                return;
            }
            Intent i = new Intent(mContext, WEBViewActivity.class);
            i.putExtra("data", mData);
            mContext.startActivity(i);
            mData.getDd().setIseed(true);

            try {
                MyApplication.getInstance().getDbUtisl().saveOrUpdate(mData.getDd());
            } catch (DbException e) {
                e.printStackTrace();
                MyApplication.getInstance().updateDD();
            }
        }

    }

    @Override
    public boolean onLongClick(View view) {
        final CoreDataBean.DataBean data = (CoreDataBean.DataBean) view.getTag();
        final File tempFile = new File(DownManagerUtil.getInstance().savePath + data.getAppId());
        if (data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_FINISH || data.getDd().getDownState() == DownManagerUtil.DOWNLOAD_PUASE) {

            DialogUtils.YesOrNoDialog(mContext, R.mipmap.text_delete, new DialogUtils.YeOrOnListener() {
                @Override
                public void onOk() {
                    if (DownManagerUtil.deleteAllFile(tempFile)) {
                        AppUtils.showToast(mContext.getString(R.string.del_ok));
                        MusicUtils.getMusicUtils().playSound(R.raw.removing_creatures_8);
                        try {
                            data.getDd().setDownState(DownManagerUtil.DOWNLOAD_NO);
                            data.getDd().setDownSize(0);
                            data.getDd().setIseed(false);
                            MyApplication.getInstance().getDbUtisl().delete(data.getDd());
                            MyApplication.getInstance().getDbUtisl().delete(data);
                        } catch (DbException e) {
                            e.printStackTrace();
                        }
                        notifyItemChanged(data.getDd().getPosation());
                    } else {
                        AppUtils.showToast(mContext.getString(R.string.del_no));
                    }
                }

                @Override
                public void onNo() {

                }
            });
        }
        return true;
    }

    public class ViewItem extends RecyclerView.ViewHolder {
        private ImageView imageapp, iv_isnew, item_isee;// 展示的appaIcon
        private ProgressBar imagecover;// 黑色遮挡物
        private ImageView btn, iv_black_bg;// 下载按钮 黑色遮罩
        private TextView tv_title;//标题
        private RelativeLayout rl_box;

        public ViewItem(View convertView) {
            super(convertView);

            imageapp = (ImageView) convertView.findViewById(R.id.cartoon_item_app);
            item_isee = (ImageView) convertView.findViewById(R.id.item_isee);
            imagecover = (ProgressBar) convertView.findViewById(R.id.cartoon_item_lock);
            btn = (ImageView) convertView.findViewById(R.id.item_painting_btn);
            iv_black_bg = (ImageView) convertView.findViewById(R.id.iv_black_bg);
            iv_isnew = (ImageView) convertView.findViewById(R.id.iv_isnew);
            tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            rl_box = (RelativeLayout) convertView.findViewById(R.id.cartoon_item_kuang);

        }
    }

    public void nextItem() {
        if (StaticObj.dataIng.getDd().getPosation() + 1 < baseAppInfoList.size()) {
            CoreDataBean.DataBean nextD = baseAppInfoList.get(StaticObj.dataIng.getDd().getPosation() + 1);
            if (nextD.getDd().getDownState() != DownManagerUtil.DOWNLOAD_ING && nextD.getDd().getDownState() != DownManagerUtil.DOWNLOAD_WAIT) {
                View v = new View(mContext);
                v.setTag(nextD);
                onClick(v);
            }
        } else {
        }
    }

    public void fastItem() {
        CoreDataBean.DataBean nextD = baseAppInfoList.get(0);

        View v = new View(mContext);
        v.setTag(nextD);
        onClick(v);
    }

}