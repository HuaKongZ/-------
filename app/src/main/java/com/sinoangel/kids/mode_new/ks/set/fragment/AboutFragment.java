package com.sinoangel.kids.mode_new.ks.set.fragment;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sinoangel.kids.mode_new.ks.R;

/**
 * Created by Administrator on 2016/8/17 0017.
 */
public class AboutFragment extends Fragment {
    private TextView version;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, null);
        version = (TextView) view.findViewById(R.id.version);
        PackageManager pm = getActivity().getPackageManager();
        PackageInfo pi;
        try {
            pi = pm.getPackageInfo(getActivity().getPackageName(), 0);
            version.setText(getString(R.string.versionlab) + pi.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        return view;
    }
}
