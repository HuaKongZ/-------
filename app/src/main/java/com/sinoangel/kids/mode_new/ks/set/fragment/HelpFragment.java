package com.sinoangel.kids.mode_new.ks.set.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.sinoangel.kids.mode_new.ks.R;

/**
 * Created by Administrator on 2016/8/17 0017.
 */
public class HelpFragment extends Fragment {
    private ViewPager viewPager;
    private int[] iv;
    private RadioGroup rg_dian;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, null);
        viewPager = (ViewPager) view.findViewById(R.id.vp_list);
        rg_dian = (RadioGroup) view.findViewById(R.id.rg_dian);
        iv = new int[7];
        iv[0] = R.mipmap.news_main1;
        iv[1] = R.mipmap.news_main2;
        iv[2] = R.mipmap.news_login_tu;
        iv[3] = R.mipmap.news_store;
        iv[4] = R.mipmap.news_set;
        iv[5] = R.mipmap.news_context;
        iv[6] = R.mipmap.news_context1;
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return iv.length;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                ImageView ivimg = new ImageView(container.getContext());
                ivimg.setScaleType(ImageView.ScaleType.FIT_XY);
                ivimg.setImageResource(iv[position]);
                container.addView(ivimg);
                return ivimg;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((RadioButton) rg_dian.getChildAt(position)).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }
}
