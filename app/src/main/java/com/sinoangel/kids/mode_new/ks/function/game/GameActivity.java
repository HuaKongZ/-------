package com.sinoangel.kids.mode_new.ks.function.game;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.squareup.okhttp.Call;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * 一起玩
 * 类说明：启蒙应用类别（5中小分类）
 * 编写日期: 2016-2-1
 * 作者: 闻铭 </pre>
 */
public class GameActivity extends BaseActivity implements OnClickListener {

    // 变量说明： 返回按钮
    private ImageView back;
    private RecyclerView gridView;
    // 变量说明：标题
    private TextView tvtitle;
    // 适配器
    private GameGVAdapter showAppAdpter;
    private List<CoreDataBean.DataBean> dataList;
    private GifImageView gib_bar;
    private Call call;
    private long userTime;//使用时长

    private Handler animHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    // 展示集合
                    showAppAdpter = new GameGVAdapter(GameActivity.this, dataList);
                    gridView.setAdapter(showAppAdpter);

                    break;
                case 1:
                    AppUtils.showToast(getString(R.string.net_warnnetwork));
                    break;
            }
            gib_bar.setVisibility(View.GONE);
        }
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showcategory);
        init();

        if ("0".equals(StaticObj.getCurrCata().getAppId())) {
            getNativeData(false);
        } else if (!HttpUtil.isNetworkAvailable()) {
            getNativeData(true);
        }
        tvtitle.setText(StaticObj.getCurrCata().getAppName());

        userTime = new Date().getTime();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpUtil.getUtils().statistics(StaticObj.getCurrCata().getAppId(), StaticObj.getCategory_id(), "1", "1", "0", (new Date().getTime() - userTime));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (HttpUtil.isNetworkAvailable()) {
            getNetData();
        }

        // 注册广播接收机制 : 更新应用列表状态
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(DownManagerUtil.DOWNLOAD_ACTION);
        this.registerReceiver(AppReceiver, mIntentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (call != null)
            call.cancel();
        unregisterReceiver(AppReceiver);
        // 删除已经下载的并安装了的apk包
        if (dataList != null && dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                String path = dataList.get(i).getPackageName();
                if (DownManagerUtil.isAvilible(this, dataList.get(i).getPackageName())) {
                    // 已经安装了，那么就删除apk包
                    // 如果成功安装，就删除安装包
                    // apk安装成功，删除apk包
                    File apkFile = new File(DownManagerUtil.getInstance().savePath + path);
                    Log.i("uu", "uu:" + dataList.get(i).getPackageName() + "-------" + apkFile.exists());
                    if (apkFile.exists()) {
                        apkFile.delete();
                    }
                }
            }
        }
    }

    /**
     * 方法说明：初始化所有控件
     */
    private void init() {
        back = (ImageView) findViewById(R.id.show_back);
        gridView = (RecyclerView) findViewById(R.id.show_girdview);
        tvtitle = (TextView) findViewById(R.id.show_title);// 标题
        gib_bar = (GifImageView) findViewById(R.id.gib_bar);
        back.setOnClickListener(this);
        GridLayoutManager flm = new GridLayoutManager(this, 4);
        gridView.setLayoutManager(flm);

    }

    private void getNativeData(boolean flage) {
        try {

            if (flage) {
                Selector selec = Selector.from(CoreDataBean.DataBean.class);
                selec.expr("cardID = '" + StaticObj.getCurrCata().getAppId() + "' AND categoryID = '" + Constant.CATEGORY_INITIATE + "'");
                dataList = MyApplication.getInstance().getDbUtisl().findAll(selec);

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_INITIATE + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (dataList != null)
                            for (CoreDataBean.DataBean db : dataList) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    break;
                                }
                            }
                    }

            } else {

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr(" categoryID =" + Constant.CATEGORY_INITIATE + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                Selector sel_db = Selector.from(CoreDataBean.DataBean.class);
                sel_db.expr(" categoryID =" + Constant.CATEGORY_INITIATE);
                List<CoreDataBean.DataBean> ldb = MyApplication.getInstance().getDbUtisl().findAll(sel_db);

                dataList = new ArrayList<>();
                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (ldb != null)
                            for (CoreDataBean.DataBean db : ldb) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    dataList.add(db);
                                    break;
                                }
                            }
                    }
            }

            animHandler.sendEmptyMessage(0);
        } catch (Exception e) {
        }
    }

    /**
     * 方法说明：从服务器获取数据
     */
    private void getNetData() {
        String url = API.generalDetail();

        call = HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
            @Override
            public void onNetFail() {
                animHandler.sendEmptyMessage(1);
            }

            @Override
            public void onNetSucceed(String json) {
                try {

                    CoreDataBean dat = JSON.parseObject(json, CoreDataBean.class);
                    dataList = dat.getData();

                    MyApplication.getInstance().saveItem(dat.getData());

                    Selector selec = Selector.from(CoreDataBean.DownData.class);
                    selec.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_INITIATE + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");

                    List<CoreDataBean.DownData> lcd = MyApplication.getInstance().getDbUtisl().findAll(selec);
                    if (lcd != null)
                        for (CoreDataBean.DownData dd : lcd) {
                            if (dataList != null)
                                for (CoreDataBean.DataBean cdb : dataList) {
                                    if (dd.getAppId().equals(cdb.getAppId())) {
                                        cdb.setDd(dd);
                                        break;
                                    }
                                }
                        }

                    animHandler.sendEmptyMessageDelayed(0, 1500);
                } catch (DbException de) {
                    try {
                        MyApplication.getInstance().getDbUtisl().dropTable(CoreDataBean.DataBean.class);
                        gib_bar.setVisibility(View.VISIBLE);
                        getNetData();
                    } catch (DbException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    animHandler.sendEmptyMessage(1);
                }
            }
        });

    }

    /**
     * 方法说明：所有按钮的点击事件
     */
    public void onClick(View v) {
        switch (v.getId()) {
            // 点击返回按钮
            case R.id.show_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_anmi:
                MusicUtils.getMusicUtils().playSound(R.raw.supergem_chainreaction_6);
                break;
        }
    }

    private View dialogView; // dialog的view
    public Dialog appInfoDialog; // dialof控件
    public CoreDataBean.DataBean mData; // 数据的实体类

    /**
     * 方法说明：Showdialog
     */
    public void ShowInAppDialog() {

        // 绑定所有控件
        initDialogView();
        // 如果为空就创建新的
        if (appInfoDialog == null) {
            appInfoDialog = new Dialog(GameActivity.this, R.style.customDialog);
        }
        // 获取标题
        holder.dialogTitle.setText(mData.getAppName());
        // 获取描述
        if (mData.getAppDesc() != null) {
            holder.dialogDesc.setText(mData.getAppDesc().replace("<br/>", "").replace("<br />", ""));
        } else {
            holder.dialogDesc.setText(mData.getAppDesc() + "");
        }

        appInfoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
            }
        });

        DialogAppInfoAdapter daia = new DialogAppInfoAdapter(mData.getImagesList(), mData.getPicShowType() + "");
        holder.mGallery.setAdapter(daia);

        // 控制dialog的宽高
        int imgW = MyApplication.getInstance().getHei() / 3;
        int imgH = MyApplication.getInstance().getWei() / 3;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, imgH);

        if ("1".equals(mData.getPicShowType())) {
            // 横图展示
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, imgH);
        } else if ("2".equals(mData.getPicShowType())) {
            // 纵图展示
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, imgW);
        }
        holder.mGallery.setLayoutParams(lp);
        holder.dialogCancel.setOnClickListener(dialogClick);
        holder.dialodSure.setOnClickListener(dialogClick);
        holder.dialodSure.setTag(mData);
        appInfoDialog.setContentView(dialogView);

        // 设置dialog的宽高
        int W = MyApplication.getInstance().getHei() * 7 / 8;
        int H = MyApplication.getInstance().getWei() * 7 / 8;
        WindowManager.LayoutParams params = appInfoDialog.getWindow().getAttributes();
        params.width = W;
        params.height = H;
        appInfoDialog.getWindow().setAttributes(params);

        appInfoDialog.show();
    }

    /**
     * 方法说明：初始化dialog
     */
    private DialogHolder holder;

    private void initDialogView() {
        if (dialogView == null) {
            dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_app_info, null);
            holder = new DialogHolder();
            holder.dialogTitle = (TextView) dialogView.findViewById(R.id.dialog_info_title);
            holder.dialogDesc = (TextView) dialogView.findViewById(R.id.dialog_info_desc);
            holder.dialogDesc.setMovementMethod(ScrollingMovementMethod.getInstance());
            holder.dialodSure = (Button) dialogView.findViewById(R.id.dialog_info_sure);
            holder.dialogCancel = (Button) dialogView.findViewById(R.id.dialog_info_cancel);
            holder.mGallery = (RecyclerView) dialogView.findViewById(R.id.rv_dialog_info_gallery);
            dialogView.setTag(holder);


            LinearLayoutManager llm = new LinearLayoutManager(GameActivity.this);
            llm.setOrientation(LinearLayoutManager.HORIZONTAL);
            holder.mGallery.setLayoutManager(llm);
        } else {
            holder = (DialogHolder) dialogView.getTag();
        }
    }

    /* 私有的变量内部类 */
    private class DialogHolder {
        private TextView dialogTitle;// 标题
        private TextView dialogDesc;// 描述
        private Button dialodSure;// 下载
        private Button dialogCancel;// 取消
        private RecyclerView mGallery;// 图片控件
    }

    /**
     * 方法说明:dialog的点击事件
     */
    private OnClickListener dialogClick = new OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                // 取消按钮
                case R.id.dialog_info_cancel:
                    appInfoDialog.dismiss();
                    break;

                // 点击确定，开始下载，下载过程中显示下载进度
                case R.id.dialog_info_sure:
                    MusicUtils.getMusicUtils().playSound(R.raw.menu_button_click);
                    DownManagerUtil.getInstance().downFile(mData);
                    appInfoDialog.dismiss();
                    HttpUtil.getUtils().statistics(mData.getAppId(), StaticObj.getCategory_id(), "0", "0", "1", 0);
                    break;
            }
        }
    };

    /**
     * 方法说明：下载的服务
     */
    private BroadcastReceiver AppReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(), DownManagerUtil.DOWNLOAD_ACTION)) {
                if (showAppAdpter != null) {
                    showAppAdpter.notifyDataSetChanged();
                }
            }
        }
    };

}