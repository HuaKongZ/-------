package com.sinoangel.kids.mode_new.ks.set;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.MainActivity;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;

/**
 * Created by Z on 2016/11/24.
 */


public class GuideActivity extends BaseActivity implements View.OnClickListener {

    private int index;
    private String flage;
    private int[] maingu = {R.layout.dialog_ny1, R.layout.dialog_ny2, R.layout.dialog_ny3};
    private int[] logingu = {R.layout.dialog_ny4_1, R.layout.dialog_ny4_2, R.layout.dialog_ny4_3, R.layout.dialog_ny4_4, R.layout.dialog_ny4, R.layout.dialog_ny5};
    private int[] setgu = {R.layout.dialog_ny6, R.layout.dialog_ny7, R.layout.dialog_ny8};
    private int[] list;

    private ImageView iv_next, iv_skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);

        index = getIntent().getIntExtra("index", 0);
        flage = getIntent().getStringExtra("flage");

        if (Constant.NEWUSE_NY1.equals(flage)) {
            list = maingu;
        } else if (Constant.NEWUSE_NY2.equals(flage)) {
            list = logingu;
        } else if (Constant.NEWUSE_NY3.equals(flage)) {
            list = setgu;
        }

        setContentView(list[index]);
        initView();
    }

    private void initView() {
        iv_next = (ImageView) findViewById(R.id.iv_next);

        if (list.length == index + 1)
            iv_next.setImageResource(R.mipmap.news_know);

        iv_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_next:
                finish();
                if (index < list.length - 1) {
                    Intent intent = new Intent(GuideActivity.this, GuideActivity.class);
                    intent.putExtra("index", index + 1);
                    intent.putExtra("flage", flage);
                    startActivity(intent);
                } else {
                    SPUtils.putIsExists(flage);
                }
                break;
        }
    }
}
