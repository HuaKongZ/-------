package com.sinoangel.kids.mode_new.ks.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.sinoangel.kids.mode_new.ks.core.aidl.IMusicAidlInterface;
import com.sinoangel.kids.mode_new.ks.core.aidl.ISondAidlInterface;
import com.sinoangel.kids.mode_new.ks.core.service.BGMusicService;
import com.sinoangel.kids.mode_new.ks.core.service.SoundService;

/**
 * Created by Z on 2016/12/1.
 */

public class MusicUtils {
    private Context mContext;
    private ISondAidlInterface ssc;//音效控制器
    private IMusicAidlInterface bgsc;//背景音乐控制
    private ServiceConnection connS, connM;
    private static MusicUtils mu;

    private MusicUtils() {
    }

    public static MusicUtils getMusicUtils() {
        return mu;

    }

    public static void initMusic(Context context) {
        if (mu == null)
            mu = new MusicUtils();
        mu.mContext = context;

    }

    public void playSound(int id) {
        if (ssc != null) {
            try {
                ssc.playSound(id);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void setSsc(ISondAidlInterface ssc) {
        this.ssc = ssc;
    }

    public void playOrPauseBgsc(boolean flage) {
        if (bgsc != null) {
            try {
                bgsc.palyOrPause(flage);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }


    public void setBgsc(IMusicAidlInterface bgsc) {
        this.bgsc = bgsc;
    }


    public void nOFBGMusicService(boolean flage) {
        Intent soundService = new Intent(mContext, BGMusicService.class);
        if (connM == null)
            connM = new ServiceConnection() {
                public void onServiceDisconnected(ComponentName name) {
                }

                public void onServiceConnected(ComponentName name, IBinder service) {
                    setBgsc(IMusicAidlInterface.Stub.asInterface(service));
                }
            };
        if (flage) {
            mContext.bindService(soundService, connM, mContext.BIND_AUTO_CREATE);
        } else {
            mContext.unbindService(connM);
            bgsc = null;
            connM = null;
        }
    }

    public void nOFSoundService(boolean flage) {
        Intent soundService = new Intent(mContext, SoundService.class);
        if (connS == null)
            connS = new ServiceConnection() {
                public void onServiceDisconnected(ComponentName name) {
                    setSsc(null);
                }

                public void onServiceConnected(ComponentName name, IBinder service) {
                    setSsc(ISondAidlInterface.Stub.asInterface(service));
                }
            };
        if (flage) {
            mContext.bindService(soundService, connS, mContext.BIND_AUTO_CREATE);
        } else {
            mContext.unbindService(connS);
            ssc = null;
            connS = null;
        }
    }
}
