package com.sinoangel.kids.mode_new.ks.function.cartoon;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.db.sqlite.WhereBuilder;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.BaseActivity;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.MusicUtils;
import com.sinoangel.kids.mode_new.ks.util.SPUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;
import com.squareup.okhttp.Call;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * 类说明：一起看类
 * 功能说明：通过向服务器请求货到需要展示的mp4文件的图片，点击后实现下载功能 编写日期: 2016-2-18 作者: 闻铭 </pre>
 * <p/>
 * 播放视频说明：视频分为1：原视频、2：优酷视频、3：爱奇艺视频、4：YouTube视频
 * downUrl，存放1的下载地址，2的vid，4的网页播放地址
 */
public class CartoonActivity extends BaseActivity implements OnClickListener {

    // 变量说明：返回按钮,播放图片
    private ImageView back, iv_info;
    // 变量说明：页面的描述
    private TextView tv_title;
    // 页面的gridview
    private RecyclerView rView;
    // gridview的适配器
    private CartoonAdapter cartoonAdapter;
    private List<CoreDataBean.DataBean> dataList;
    private GifImageView gib_bar;
    private Call call;
    private String idflage = "";

    private long userTime;//使用时长

    private Handler animHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    // 展示集合
                    cartoonAdapter = new CartoonAdapter(CartoonActivity.this, dataList);
                    rView.setAdapter(cartoonAdapter);

                    if (SPUtils.getIsExists(StaticObj.getCategory_id() + StaticObj.getCurrCata().getAppId())) {
                        SPUtils.putIsExists(StaticObj.getCategory_id() + StaticObj.getCurrCata().getAppId());
                        DialogUtils.showDesDialog(CartoonActivity.this, getWindow(), R.mipmap.btn_play, new DialogUtils.BtnStartListener() {
                            @Override
                            public void onDoSome() {
                                cartoonAdapter.fastItem();
                            }
                        });
                    }
                    iv_info.setVisibility(View.VISIBLE);

                    break;
                case 1:
                    AppUtils.showToast(getString(R.string.net_warnnetwork));
                    break;
            }
            gib_bar.setVisibility(View.GONE);
        }
    };


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cartoon_my);

        init();
//        if (Constant.DO_REFSIH.equals(getIntent().getStringExtra(Constant.DO))) {
//            if (HttpUtil.isNetworkAvailable()) {
//                getNetData();
//            }
//        }
        if ("0".equals(StaticObj.getCurrCata().getAppId())) {
            getNativeData(false);
        } else if (!HttpUtil.isNetworkAvailable()) {
            getNativeData(true);
        } else if (HttpUtil.isNetworkAvailable()) {
            getNetData();
            idflage = StaticObj.getCurrCata().getAppId();
        }


        //处理头信息
//        MyApplication.getInstance().showImgUrl(StaticObj.getCurrCata().getImages(), Imange);
        tv_title.setText(StaticObj.getCurrCata().getAppName());


        userTime = new Date().getTime();

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!"0".equals(StaticObj.getCurrCata().getAppId()))
            if (!idflage.equals(StaticObj.getCurrCata().getAppId())) {
                if (HttpUtil.isNetworkAvailable()) {
                    gib_bar.setVisibility(View.VISIBLE);
                    dataList.clear();
                    iv_info.setVisibility(View.GONE);
                    cartoonAdapter.notifyDataSetChanged();
                    tv_title.setText(StaticObj.getCurrCata().getAppName());
                    getNetData();
                }
            }

        // 注册广播接收机制 : 更新应用列表状态
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(DownManagerUtil.DOWNLOAD_ACTION); // progress
        this.registerReceiver(AppReceiver, mIntentFilter);

    }


    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(AppReceiver);
        if (call != null)
            call.cancel();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        if (cartoonAdapter != null) {
            cartoonAdapter.notifyDataSetChanged();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpUtil.getUtils().statistics(StaticObj.getCurrCata().getAppId(), StaticObj.getCategory_id(), "1", "1", "0", (new Date().getTime() - userTime));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    /**
     * 方法说明：初始化所有变量，以及对所有按钮的点击监听
     */
    private void init() {
        back = (ImageView) findViewById(R.id.cartoon_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        rView = (RecyclerView) findViewById(R.id.cartoon_gridview);
        gib_bar = (GifImageView) findViewById(R.id.gib_bar);
        iv_info = (ImageView) findViewById(R.id.iv_info);

        back.setOnClickListener(this);
        iv_info.setOnClickListener(this);

        GridLayoutManager gm = new GridLayoutManager(CartoonActivity.this, 3);
        rView.setLayoutManager(gm);
    }


    private void getNativeData(boolean flage) {
        try {

            if (flage) {
                Selector selec = Selector.from(CoreDataBean.DataBean.class);
                selec.expr("cardID = '" + StaticObj.getCurrCata().getAppId() + "' AND categoryID = '" + Constant.CATEGORY_CARTOON + "'");
                dataList = MyApplication.getInstance().getDbUtisl().findAll(selec);

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_CARTOON + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (dataList != null)
                            for (CoreDataBean.DataBean db : dataList) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    break;
                                }
                            }
                    }

            } else {

                Selector sel_dd = Selector.from(CoreDataBean.DownData.class);
                sel_dd.expr(" categoryID =" + Constant.CATEGORY_CARTOON + " AND (downState = 4 OR downState = 2 OR downState = 3 OR downState = 5 OR downState = 6)");
                List<CoreDataBean.DownData> ldd = MyApplication.getInstance().getDbUtisl().findAll(sel_dd);

                Selector sel_db = Selector.from(CoreDataBean.DataBean.class);
                sel_db.expr(" categoryID =" + Constant.CATEGORY_CARTOON);
                List<CoreDataBean.DataBean> ldb = MyApplication.getInstance().getDbUtisl().findAll(sel_db);

                dataList = new ArrayList<>();
                if (ldd != null)
                    for (CoreDataBean.DownData dd : ldd) {
                        if (ldb != null)
                            for (CoreDataBean.DataBean db : ldb) {
                                if (dd.getAppId().equals(db.getAppId())) {
                                    db.setDd(dd);
                                    dataList.add(db);
                                    break;
                                }
                            }
                    }
            }
            if (dataList.size() > 0)
                animHandler.sendEmptyMessage(0);
            else
                animHandler.sendEmptyMessage(1);
        } catch (Exception e) {
        }
    }

    /**
     * 方法说明：对所有数据进行请求
     */
    private void getNetData() {

        String url = API.generalDetail();


        call = HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
                    @Override
                    public void onNetFail() {
                        animHandler.sendEmptyMessage(1);
                    }

                    @Override
                    public void onNetSucceed(String json) {
                        try {
                            CoreDataBean dat = JSON.parseObject(json, CoreDataBean.class);
                            dataList = dat.getData();

                            Selector seld = Selector.from(CoreDataBean.DataBean.class);
                            seld.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_CARTOON);
                            MyApplication.getInstance().getDbUtisl().delete(CoreDataBean.DataBean.class, WhereBuilder.b("categoryID", "=", Constant.CATEGORY_CARTOON));

                            MyApplication.getInstance().saveItem(dat.getData());

                            Selector selec = Selector.from(CoreDataBean.DownData.class);
                            selec.expr("cardID = " + StaticObj.getCurrCata().getAppId() + " AND categoryID =" + Constant.CATEGORY_CARTOON);

                            List<CoreDataBean.DownData> lcd = MyApplication.getInstance().getDbUtisl().findAll(selec);
                            if (lcd != null)
                                for (CoreDataBean.DownData dd : lcd) {
                                    if (dataList != null)
                                        for (CoreDataBean.DataBean cdb : dataList) {
                                            if (dd.getAppId().equals(cdb.getAppId())) {
                                                cdb.setDd(dd);
                                                break;
                                            }
                                        }
                                }


                            if (dataList.size() > 0)
                                animHandler.sendEmptyMessageDelayed(0, 1500);
                            else
                                animHandler.sendEmptyMessage(1);
                        } catch (DbException de) {
                            MyApplication.getInstance().updateDD();
                            getNetData();
                        } catch (Exception e) {

                        }
                    }
                }

        );
    }

    /**
     * 方法说明：所有按钮的点击事件
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cartoon_back:
                MusicUtils.getMusicUtils().playSound(R.raw.connect_1);
                finish();
                break;
            case R.id.iv_anmi:
                MusicUtils.getMusicUtils().playSound(R.raw.supergem_chainreaction_3);
                break;
            case R.id.iv_info:
                DialogUtils.showDesDialog(CartoonActivity.this, getWindow(), R.mipmap.btn_play, new DialogUtils.BtnStartListener() {
                    @Override
                    public void onDoSome() {
                        cartoonAdapter.fastItem();
                    }
                });
                break;
        }
    }

    /**
     * 方法说明：下载的服务
     */
    private BroadcastReceiver AppReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (TextUtils.equals(intent.getAction(), DownManagerUtil.DOWNLOAD_ACTION)) {
                if (cartoonAdapter != null) {
                    CoreDataBean.DataBean dat = (CoreDataBean.DataBean) intent.getExtras().get(DownManagerUtil.DOWNLOAD_OBJ);
                    cartoonAdapter.notifyItemChanged(dat.getDd().getPosation());
                }
            }
        }
    };

    public void nextItem() {
        if (cartoonAdapter != null)
            cartoonAdapter.nextItem();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent == null)
            return;
        switch (intent.getStringExtra(Constant.DO)) {
//            case Constant.DO_REFSIH:

//                break;
            case Constant.DO_NEXT:
                nextItem();
                break;
        }
    }

}
