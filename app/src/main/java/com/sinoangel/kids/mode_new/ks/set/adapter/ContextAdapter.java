package com.sinoangel.kids.mode_new.ks.set.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;
import com.sinoangel.kids.mode_new.ks.R;
import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.sinoangel.kids.mode_new.ks.core.bean.Cate;
import com.sinoangel.kids.mode_new.ks.core.bean.CoreDataBean;
import com.sinoangel.kids.mode_new.ks.util.API;
import com.sinoangel.kids.mode_new.ks.util.AppUtils;
import com.sinoangel.kids.mode_new.ks.util.Constant;
import com.sinoangel.kids.mode_new.ks.util.DialogUtils;
import com.sinoangel.kids.mode_new.ks.util.DownManagerUtil;
import com.sinoangel.kids.mode_new.ks.util.HttpUtil;
import com.sinoangel.kids.mode_new.ks.util.ImageUtils;
import com.sinoangel.kids.mode_new.ks.util.StaticObj;

import java.io.File;
import java.util.List;

/**
 * 关联类：讲故事的适配器 适配器说明：用于展示启蒙应用中的icon ,黑色遮盖物，下载按钮 编写日期: 2016-2-2 作者: 闻铭
 * </pre>
 */
public class ContextAdapter extends RecyclerView.Adapter<ContextAdapter.ViewHolder> implements View.OnLongClickListener, View.OnClickListener {

    private List<Cate.DataBean> CataList;
    private Context mContext;
    private Window win;
    private boolean isEdit;

    public ContextAdapter(Context mContext, Window win, List<Cate.DataBean> baseAppInfoList) {
        super();
        this.mContext = mContext;
        this.CataList = baseAppInfoList;
        this.win = win;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = null;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card, null);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.iv.setOnLongClickListener(this);
        viewHolder.iv.setOnClickListener(this);
        view.setOnClickListener(this);
        viewHolder.iv_delete.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if (isEdit) {
            viewHolder.iv_delete.setVisibility(View.VISIBLE);
        } else {
            viewHolder.iv_delete.setVisibility(View.GONE);
        }
        viewHolder.iv_delete.setTag(CataList.get(position));
        ImageUtils.showImgUrl(CataList.get(position).getIcon(), viewHolder.iv);


    }

    @Override
    public int getItemCount() {
        return CataList == null ? 0 : CataList.size();
    }

    @Override
    public boolean onLongClick(final View view) {
        isEdit = true;
        notifyDataSetChanged();

        return true;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.iv_delete:

                AppUtils.outputLog("iv_delete");

                if (StaticObj.getUid() != null) {
                    DialogUtils.YesOrNoDialog(mContext,  R.mipmap.text_delete, new DialogUtils.YeOrOnListener() {
                        @Override
                        public void onOk() {
                            final Cate.DataBean cdb = (Cate.DataBean) view.getTag();
                            String url = API.getAddBlacklist(cdb.getAppId());
                            HttpUtil.getUtils().getJsonString(url, new HttpUtil.OnNetResponseListener() {
                                @Override
                                public void onNetFail() {
                                }

                                @Override
                                public void onNetSucceed(String json) {

                                    CataList.remove(cdb);
                                    new Handler(mContext.getMainLooper()) {
                                        @Override
                                        public void handleMessage(Message msg) {
                                            super.handleMessage(msg);
                                            notifyDataSetChanged();
                                        }
                                    }.sendEmptyMessage(0);

                                    if (Constant.CATEGORY_PICTURE.equals(StaticObj.getCategory_id()) || Constant.CATEGORY_SONG.equals(StaticObj.getCategory_id())) {
                                        Selector selec = Selector.from(CoreDataBean.DataBean.class);
                                        selec.expr("cardID = '" + cdb.getAppId() + "' AND categoryID = '" + StaticObj.getCategory_id() + "'");
                                        try {
                                            List<CoreDataBean.DataBean> dataList = MyApplication.getInstance().getDbUtisl().findAll(selec);
                                            if (dataList != null)
                                                for (CoreDataBean.DataBean cdbd : dataList) {
                                                    if (DownManagerUtil.deleteAllFile(new File(cdbd.getSavePath()))) {
                                                        MyApplication.getInstance().getDbUtisl().deleteById(CoreDataBean.DownData.class, cdbd.getAppId());
                                                    }
                                                }
                                        } catch (DbException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            });
                        }

                        @Override
                        public void onNo() {

                        }
                    });
                } else {
                    AppUtils.showToast(mContext.getString(R.string.xiandenglu));
                }
                break;
            default:
                if (isEdit) {
                    isEdit = false;
                    notifyDataSetChanged();
                }
                break;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv;
        public ImageView iv_delete;

        public ViewHolder(View itemView) {
            super(itemView);
            iv = (ImageView) itemView.findViewById(R.id.iv_core);
            iv_delete = (ImageView) itemView.findViewById(R.id.iv_delete);
        }
    }

    public void refish() {
        if (isEdit) {
            isEdit = false;
            notifyDataSetChanged();
        }
    }
}

