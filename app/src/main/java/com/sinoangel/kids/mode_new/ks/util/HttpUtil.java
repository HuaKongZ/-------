package com.sinoangel.kids.mode_new.ks.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.sinoangel.kids.mode_new.ks.core.base.MyApplication;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 功能说明: 请求网络，获取json的字符串
 * 编写日期:	2016-1-28
 */
public class HttpUtil {
    private static HttpUtil utils;

    //创建okHttpClient对象
    OkHttpClient mOkHttpClient = new OkHttpClient();

    public final static int CONNECT_TIMEOUT = 10;
    public final static int READ_TIMEOUT = 30;
    public final static int WRITE_TIMEOUT = 30;

    private HttpUtil() {
    }

    public static HttpUtil getUtils() {
        if (utils == null) {
            utils = new HttpUtil();
            utils.mOkHttpClient.setConnectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
            utils.mOkHttpClient.setReadTimeout(READ_TIMEOUT, TimeUnit.SECONDS);//设置读取超时时间
            utils.mOkHttpClient.setWriteTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);//设置写的超时时间
        }
        return utils;
    }

    public Call getJsonString(String url, final OnNetResponseListener onResponseListener) {


        final Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                if (onResponseListener != null)
                    onResponseListener.onNetFail();
                if (e != null)
                    AppUtils.outputLog(e.getMessage());
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                if (onResponseListener != null)
                    onResponseListener.onNetSucceed(response.body().string());
            }


        });
        return call;
    }

    public Call getJsonStringByPost(String url, Map<String, String> mss, final OnNetResponseListener onResponseListener) {
        FormEncodingBuilder builder = new FormEncodingBuilder();
        for (String key : mss.keySet()) {
            builder.add(key, mss.get(key));
        }

        final Request request = new Request.Builder()
                .url(url)
                .post(builder.build())
                .build();
        Call call = mOkHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                if (onResponseListener != null)
                    onResponseListener.onNetFail();
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                if (onResponseListener != null)
                    onResponseListener.onNetSucceed(response.body().string());
            }
        });
        return call;
    }

    private Call statictiscCall;

    //统计模块运行时间
    public void statistics(String appId, String cateID, String isAlbum, String isStart, String isDown, long useSecond
    ) {

        if (statictiscCall != null && statictiscCall.isCanceled()) {
            statictiscCall.cancel();
        }

        Map<String, String> mss = new HashMap<>();
        mss.put("tokenId", SPUtils.getPhoneId());
        mss.put("channel_id", MyApplication.getAppKey());
        mss.put("appId", appId);
        mss.put("isAlbum", isAlbum);
        mss.put("isStart", isStart);
        mss.put("isDown", isDown);
        mss.put("runTime", useSecond / 1000 + "");
        mss.put("age", MyApplication.DayTime() + "");
        mss.put("lang", MyApplication.systemLanguageType() + "");
        mss.put("category_id", cateID);
        mss.put("device", "self");
//        mss.put("device", "tecno");

        String url = API.statistics(mss);

        final Request request = new Request.Builder()
                .url(url)
                .build();
        statictiscCall = mOkHttpClient.newCall(request);
        statictiscCall.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
            }
        });


    }


    /**
     * 判断是否开启网络
     *
     * @return
     */
    public static boolean isNetworkAvailable() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) MyApplication.getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkInfo = mConnectivityManager
                .getActiveNetworkInfo();
        if (mNetworkInfo != null) {
            return mNetworkInfo.isAvailable();
        }
        return false;
    }


    public interface OnNetResponseListener {
        void onNetFail();

        void onNetSucceed(String json);
    }

}
