package com.sinoangel.kids.mode_new.ks.core.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/8/2 0002.
 */
public class CommentsBean {


    /**
     * flag : 1
     * error : []
     * data : [{"userName":"匿名","content":"有些是否真的爱哦","starLevel":"5","createTime":"2016-08-02 17:06:22","userIcon":null}]
     */

    private int flag;
    private List<?> error;
    /**
     * userName : 匿名
     * content : 有些是否真的爱哦
     * starLevel : 5
     * createTime : 2016-08-02 17:06:22
     * userIcon : null
     */

    private List<DataBean> data;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public List<?> getError() {
        return error;
    }

    public void setError(List<?> error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String userName;
        private String content;
        private String starLevel;
        private String createTime;
        private String userIcon;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getStarLevel() {
            return starLevel;
        }

        public void setStarLevel(String starLevel) {
            this.starLevel = starLevel;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUserIcon() {
            return userIcon;
        }

        public void setUserIcon(String userIcon) {
            this.userIcon = userIcon;
        }
    }
}
