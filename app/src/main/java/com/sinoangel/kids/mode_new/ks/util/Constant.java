package com.sinoangel.kids.mode_new.ks.util;

/**
 * 类使用使用：所有常量都储存在此类中
 */
public class Constant {

    /**
     * =====语言======
     */
    public static int LANGUAGE_ZH = 1;// 中简体
    public static int LANGUAGE_EN = 2;//英
    //    public static int LANGUAGE_HK = 9;//香港繁体
    public static int LANGUAGE_ES = 5;//西班牙
    public static int LANGUAGE_AR = 6;
    public static int LANGUAGE_RU = 7;//俄语

    /**
     * ===大类别===
     */
    public static final String CATEGORY_INITIATE = "49"; // 一起玩
    public static final String CATEGORY_CARTOON = "50"; // 一起看
    public static final String CATEGORY_SONG = "51"; // 一起听
    public static final String CATEGORY_PICTURE = "62";// 讲故事


    public static final String ZAN_VH = "vh";// 横竖屏
    public static final String DATA = "data";// 单品
    public static final String DO = "do";// 重新打开activity
//    public static final String DO_REFSIH = "do_refsih";// 刷新
    public static final String DO_NEXT = "do";// 下一个
    public static final int YK_FLAGE_ZAN = 1;// 跳转点赞
    public static final int YK_FLAGE_MORE = 2;// 跳转下载更多
    public static final int YK_FLAGE_BOOKING = 3;// 跳转下载更多
    public static final String COMFLAGE = "comflage";// 跳转下载更多

    public static final String WEB_URL = "web_url";// 网址

    public static final int LOGIN_SUSSCE = 100;// 登录成功
    public static final int LOGIN_FAILE = 300;// 登录失败

    public static final String CATEGORYID = "categoryid";//大类id
    public static final String CARDOBJ = "cardobj";//当前选中的卡片
    public static final String USERBEAN = "userbean";//用户

    public static final String NEWUSE_NY1 = "newuse_ny1";//新手引导1
    public static final String NEWUSE_NY2 = "newuse_ny2";//新手引导2
    public static final String NEWUSE_NY3 = "newuse_ny3";//新手引导2

}
